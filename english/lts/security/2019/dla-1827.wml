<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Simon McVittie discovered a flaw in gvfs, the Gnome Virtual File
System. The gvfsd daemon opened a private D-Bus server socket without
configuring an authorization rule. A local attacker could connect to
this server socket and issue D-Bus method calls.</p>

<p>(Note that the server socket only accepts a single connection, so the
attacker would have to discover the server and connect to the socket
before its owner does.)</p>

<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
1.22.2-1+deb8u1.</p>

<p>We recommend that you upgrade your gvfs packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1827.data"
# $Id: $
