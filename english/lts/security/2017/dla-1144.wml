<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>git-annex before 6.20170818 allows remote attackers to execute arbitrary
commands via an ssh URL with an initial dash character in the hostname,
as demonstrated by an ssh://-eProxyCommand= URL, a related issue to
<a href="https://security-tracker.debian.org/tracker/CVE-2017-9800">CVE-2017-9800</a>, 
<a href="https://security-tracker.debian.org/tracker/CVE-2017-12836">CVE-2017-12836</a>, 
<a href="https://security-tracker.debian.org/tracker/CVE-2017-1000116">CVE-2017-1000116</a>, and 
<a href="https://security-tracker.debian.org/tracker/CVE-2017-1000117">CVE-2017-1000117</a>.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
3.20120629+deb7u1.</p>

<p>We recommend that you upgrade your git-annex packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-1144.data"
# $Id: $
