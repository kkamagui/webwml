<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>It was found that a flaw in Apache Groovy, a dynamic language for the
Java Virtual Machine, allows remote code execution wherever
deserialization occurs in the application. It is possible for an
attacker to craft a special serialized object that will execute code
directly when deserialized. All applications which rely on
serialization and do not isolate the code which deserializes objects
are subject to this vulnerability.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
1.8.6-1+deb7u2.</p>

<p>We recommend that you upgrade your groovy packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-794.data"
# $Id: $
