#use wml::debian::template MAINPAGE="true" 
#use wml::debian::recent_list
#use wml::debian::translation-check translation="9964ecf186f13ed156d7ef0745a70952a6f478f9"

<a id=community></a>
<h1>Debian är en gemenskap av folk</h1>
<p>Tusentals frivilliga över hela världen arbetar tillsammans och
  prioriterar fri mjukvara och användares behov.</p>


<ul>
  <li>
    <a href="../intro/about">Folk:</a>
    Vilka vi är, vad vi gör
  </li>
  <li>
    <a href="../intro/">Filosofi:</a>
    Varför vi gör det och hur vi gör det
  </li>
  <li>
    <a href="../devel/join/">Engagera dig:</a>
    Du kan vara en del av detta!
  </li>
  <p>
  <li>
    <a href="../social_contract">Socialt kontrakt:</a>
    Våran moraliska agenda
  </li>
  <li>
    <a href="../code_of_coduct">Uppförandekod</a>
  </li>
  <li>
    <a href="../partners/">Partner:</a>
    Företag och organisationer som tillhandahåller pågående assistans till
    Debianprojektet
  </li>
  <li>
    <a href="../donations">Donationer</a>
  </li>
  <li>
    <a href="../legal/">Rättsliga frågor</a>
  </li>
  <li>
    <a href="../legal/privacy">Dataintegritet</a>
  </li>
  <li>
    <a href="../contact">Kontakta oss</a>
  </li>
</ul>

<hr>

<a id=software></a>
<h1>Debian är ett fritt operativsystem</h1>
<p>Vi börjar med Linux och lägger till många tusentals applikationer för att
  möta våra användares behov.</p>

<ul>
  <li>
    <a href="../distrib">Hämtningar:</a>
    Fler varianter av Debianavbildningar
  </li>
  <li>
    <a href="../support">Support:</a>
    Få hjälp
  </li>
  <li>
    <a href="../security">Säkerhet:</a>
    Senast uppdaterad
    <:= get_recent_list ('security/2w', '1', '$(ENGLISHDIR)', 'bydate', '(2000\d+\w+|dsa-\d+)' ) :>
  </li>
  <p>
  <li>
    <a href="../distrib/packages"> Mjukvarupaket:</a>
    Sök och bläddra igenom den långa listan på vår mjukvara
  </li>
  <li>
    <a href="../doc"> Dokumentation</a>
  </li>
  <li>
    <a href="https://wiki.debian.org"> Debianwikin</a>
  </li>
  <li>
    <a href="../Bugs"> Felrapporter</a>
  </li>
  <li>
    <a href="https://lists.debian.org/">
    Sändlistor</a>
  </li>
  <li>
    <a href="../blends"> Pure Blends:</a>
    Metapaket för specifika behov
  </li>
  <li>
    <a href="../devel"> Utvecklarhörnan:</a>
    Information primärt av intresse för Debianutvecklare
  </li>
  <li>
    <a href="../ports"> Anpassningar/Arkitekturer:</a>
    CPU-arkitekturer som vi ger stöd för
  </li>
</ul>
