#use wml::debian::template title="Vores filosofi: Hvorfor og hvordan gør vi det"
#include "$(ENGLISHDIR)/releases/info"
#use wml::debian::translation-check translation="f51e6aa024def7d85259682733914f77e90a92e1"

# translators: some text is taken from /intro/about.wml

<ul class="toc">
<li><a href="#what">HVAD er Debian?</a>
<li><a href="#free">Er det hele frit tilgængeligt?</a>
<li><a href="#how">Hvordan arbejder fællesskabet som et projekt?</a>
<li><a href="#history">Hvordan begyndte det hele?</a>
</ul>


<h2><a name="what">HVAD er Debian?</a></h2>

<p><a href="$(HOME)/">Debian-projektet</a> er en sammenslutning af individer, 
der har det fælles mål, at fremstille et <a href="free">frit</a>, styresystem.  
Det styresystem kalder vi <strong>Debian</strong>.</p>

<p>Et styresystem er de basale programmer og værktøjer, der får din computer til 
at fungere.  Hjertet i et styresystem er kernen. Kernen er det mest fundamentale 
program på computeren; den tager sig af alle de basale gøremål og giver dig 
mulighed for at køre andre programmer.</p>

<p>Debian-systemer anvender i øjeblikket <a href="https://www.kernel.org/">\
Linux</a>-kernen eller <a href="https://www.freebsd.org/">FreeBSD</a>-kernen. 
Linux er et program startet af <a href="https://en.wikipedia.org/wiki/Linus_Torvalds">\
Linus Torvalds</a>, og støttet af tusindvis af programmører over hele verden.  
FreeBSD er et styresystem, herunder en kerne og anden software.</p>

<p>Men der arbejdes også på, at Debian kan køre med andre kerner, primært 
<a href="https://www.gnu.org/software/hurd/hurd.html">Hurd</a>.  Hurd er en 
samling servere, der kører oven på en mikrokerne (som for eksempel Mach), for at 
implementere forskellige funktioner.  Hurd er fri software, produceret af 
<a href="https://www.gnu.org/">GNU-projektet</a>.</p>

<p>En stor del af de basale værktøjer, der udgør styresystemet, kommer fra 
<a href="https://www.gnu.org/">GNU-projektet</a>; deraf navnene:  GNU/Linux, 
GNU/kFreeBSD og GNU/Hurd.  Disse værktøjer er også frit tilgængelige.</p>

<p>Selvfølgelig er det applikationsprogrammer, folk vil have: programmer der 
kan hjælpe dem med at udføre forskellige opgaver, lige fra redigering af 
dokumenter til at drive en virksomhed, computerspil, eller programudvikling.
Debian indeholder mere end <packages_in_stable> <a href="$(DISTRIB)/packages">\
pakker</a> (prækompilerede programmer i en pæn indpakning, lige til at 
installere på din maskine), et pakkehåndteringsprogram (APT), samt andre 
værktøjer, som gør det muligt at håndtere tusindvis af pakker på tusindvis af 
computere, så let som at installere en enkelt applikation.  Det hele er 
<a href="free">frit tilgængeligt</a>.</p>

<p>Det er lidt som et tårn.  Nederst er kernen.  Ovenpå den er der alle de 
grundlæggende værktøjer.  Dernæst al den software, som du kører på 
computeren.  Øverst i tårnet er Debian &ndash; som omhyggeligt organiserer 
og tilpasser alt, så det fungerer sammen.</p>


<h2>Det hele er <a href="free" name="free">frit tilgængeligt?</a></h2>

<p>Når vi bruger ordet <q>fri</q>, refererer vi til 
software<strong>frihed</strong>.  Du kan læse mere om 
<a href="free">hvad vi mener med <q>fri software</q></a> og 
<a href="https://www.gnu.org/philosophy/free-sw">hvad Free Software
Foundation</a> har at sige om emnet.</p>

<p>Du undrer dig måske over, hvorfor folk bruger timevis af deres fritid på at 
skrive programmer, omhyggeligt pakke dem sammen, for blot at <em>forære</em> 
det hele væk?  Svarene er lige så forskellige som de personer, der bidrager. 
Nogle kan lide at hjælpe andre.  Mange skriver programmer for at lære mere 
om computere.  Flere og flere søger efter muligheder for at undgå de opskruede 
priser på programmer.  En voksende gruppe bidrager som tak for al den gode, frie 
software, de har modtaget fra andre.  Mange i den akademiske verden udvikler fri 
software for at gøre resultatet af deres forskning tilgængelig for flere. 
Virksomheder hjælper til ved vedligeholdelsen af fri software, så de kan få 
indflydelse på hvordan den udvikler sig - der er ingen hurtigere måde at få 
lavet en ny funktion på, end ved selv at gøre det!  Selvfølgelig synes mange af 
os, at det bare er sjovt at være med.</p>

<p>Debian er så engageret i fri software, at vi mente det kunne være nyttigt, 
hvis engagementet var formaliseret i en eller anden form for dokument.  Sådan 
blev vores <a href="$(HOME)/social_contract">sociale kontrakt</a> født.</p>

<p>Selv om Debian går ind for fri software, er der tilfælde hvor man ønsker,
eller kan være nødt til, at installere ikke-frie programmer på sine maskiner.  
Så vidt det overhovedet er muligt, støtter Debian dette.  Der er endda et 
stigende antal pakker hvis eneste formål er at installere ikke-frie programmer 
på et Debian-system.</p>


<h2><a name="how">Hvordan arbejder fællesskabet som et projekt?</a></h2>

<p>Debian fremstilles af næsten ettusinde aktive udviklere spredt
<a href="$(DEVEL)/developers.loc">over hele verden</a>, som arbejder på 
projektet i deres fritid.  Få af udviklerne har mødt hinanden personligt. 
Kommunikation foregår primært ved hjælp af mail (postlister på 
lists.debian.org) og IRC (kanalen #debian på irc.debian.org).</p>

<p>Debian-projektet har en omhyggelig <a href="organization">organiseret 
struktur</a>.  For flere oplysninger om hvordan Debian ser ud indefra, 
kan du gennemse <a href="$(DEVEL)/">udviklerhjørnet</a>.</p>

<p>De primære dokumenter, som forklarer hvor fællesskabet arbejder, er 
følgende:</p>

<ul>
<li><a href="$(DEVEL)/constitution">Debians vedtægter</li>
<li><a href="../social_contract">Den sociale kontrakt og fri software-retningslinjer</li>
<li><a href="diversity">Mangfoldighedserklæring</li>
<li><a href="../code_of_conduct">Debians etiske regler</li>
<li><a href="../doc/developers-reference/">Debians opslagsbog for udviklere</li>
<li><a href="../doc/debian-policy/">Debians fremgangsmådehåndbog</li>
</ul>


<h2><a name="history">Hvordan begyndte det hele?</a></h2>

<p>I august 1993 blev Debian påbegyndt af Ian Murdock, som en ny distribution, 
der skulle fremstilles i fuld offentlighed, i Linux' og GNU's ånd. Debian skulle 
sammensættes omhyggeligt og samvittighedsfuldt, og den skulle vedligeholdes 
og der skulle ydes brugerhjælp, med samme omhu.  Det begyndte som en lille, 
sammentømret gruppe af fri software-folk, der gradvist voksede til et stort, 
velorganiseret fællesskab af udviklere og brugere.  Læs 
<a href="$(DOC)/manuals/project-history/">den detaljerede historie</a>.</p>

<p>Da mange har spurgt - Debian udtales: &#712;de.bi.&#601;n/ (eller 
"deb ii n").  Det kommer af navnet på Debians grundlægger, Ian Murdock, og hans 
hustru, Debra.</p>
