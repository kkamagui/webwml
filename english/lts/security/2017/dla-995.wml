<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-8400">CVE-2017-8400</a>

      <p>In SWFTools 0.9.2, an out-of-bounds write of heap data can occur in
      the function png_load() in lib/png.c:755. This issue can be triggered
      by a malformed PNG file that is mishandled by png2swf.
      Attackers could exploit this issue for DoS; it might cause arbitrary
      code execution.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-8401">CVE-2017-8401</a>

      <p>In SWFTools 0.9.2, an out-of-bounds read of heap data can occur in
      the function png_load() in lib/png.c:724. This issue can be triggered
      by a malformed PNG file that is mishandled by png2swf.
      Attackers could exploit this issue for DoS.</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
0.9.2+ds1-3+deb7u1.</p>

<p>We recommend that you upgrade your swftools packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-995.data"
# $Id: $
