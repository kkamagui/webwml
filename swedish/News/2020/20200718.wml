<define-tag pagetitle>Uppdaterad Debian 9; 9.13 utgiven</define-tag>
<define-tag release_date>2020-07-18</define-tag>
#use wml::debian::news
#use wml::debian::translation-check translation="b51002b9cd5b7dbca0e39ddb2d17bcc495ead21a"

<define-tag release>9</define-tag>
<define-tag codename>stretch</define-tag>
<define-tag revision>9.13</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>Debianprojektet presenterar stolt sin trettonde (och slutliga) uppdatering till dess
gamla stabila utgåva Debian <release> (med kodnamnet <q><codename></q>). 
Denna punktutgåva lägger huvudsakligen till rättningar för säkerhetsproblem,
tillsammans med ytterligare rättningar för allvarliga problem. Säkerhetsbulletiner
har redan publicerats separat och refereras när de finns tillgängliga.</p>

<p>Efter denna punktutgåva kommer Debians säkerhetsgrupp och utgåvegruppen inte
längre ge ut uppdateringar av Debian 9. Användare som vill fortsätta få
säkerhetsstöd bör uppdatera till Debian 10, eller se <url "https://wiki.debian.org/LTS">
för detaljer om underuppsättningen av arkitekturer och paket som stöds av
projektet för långtidsstöd.</p>

<p>Vänligen notera att punktutgåvan inte innebär en ny version av Debian 
<release> utan endast uppdaterar några av de inkluderade paketen. Det behövs
inte kastas bort gamla media av <q><codename></q>. Efter installationen
kan paket uppgraderas till de aktuella versionerna genom att använda en uppdaterad
Debianspegling..</p>

<p>Dom som frekvent installerar uppdateringar från security.debian.org kommer inte att behöva
uppdatera många paket, och de flesta av sådana uppdateringar finns
inkluderade i punktutgåvan.</p>

<p>Nya installationsavbildningar kommer snart att finnas tillgängliga på dom vanliga platserna.</p>

<p>En uppgradering av en existerande installation till denna revision kan utföras genom att
peka pakethanteringssystemet på en av Debians många HTTP-speglingar.
En utförlig lista på speglingar finns på:</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>




<h2>Blandade felrättningar</h2>

<p>Denna uppdatering av den gamla stabila utgåvan lägger till några viktiga felrättningar till följande paket:</p>

<table border=0>
<tr><th>Paket</th>               <th>Orsak</th></tr>
<correction acmetool "Rebuild against recent golang to pick up security fixes">
<correction atril "dvi: Mitigate command injection attacks by quoting filename [CVE-2017-1000159]; fix overflow checks in tiff backend [CVE-2019-1010006]; tiff: Handle failure from TIFFReadRGBAImageOriented [CVE-2019-11459]">
<correction bacula "Add transitional package bacula-director-common, avoiding loss of /etc/bacula/bacula-dir.conf when purged; make PID files owned by root">
<correction base-files "Update /etc/debian_version for the point release">
<correction batik "Fix server-side request forgery via xlink:href attributes [CVE-2019-17566]">
<correction c-icap-modules "Support ClamAV 0.102">
<correction ca-certificates "Update Mozilla CA bundle to 2.40, blacklist distrusted Symantec roots and expired <q>AddTrust External Root</q>; remove e-mail only certificates">
<correction chasquid "Rebuild against recent golang to pick up security fixes">
<correction checkstyle "Fix XML External Entity injection issue [CVE-2019-9658 CVE-2019-10782]">
<correction clamav "New upstream release [CVE-2020-3123]; security fixes [CVE-2020-3327 CVE-2020-3341]">
<correction compactheader "New upstream version, compatible with newer Thunderbird versions">
<correction cram "Ignore test failures to fix build issues">
<correction csync2 "Fail HELLO command when SSL is required">
<correction cups "Fix heap buffer overflow [CVE-2020-3898] and <q>the `ippReadIO` function may under-read an extension field</q> [CVE-2019-8842]">
<correction dbus "New upstream stable release; prevent a denial of service issue [CVE-2020-12049]; prevent use-after-free if two usernames share a uid">
<correction debian-installer "Update for the 4.9.0-13 Linux kernel ABI">
<correction debian-installer-netboot-images "Rebuild against stretch-proposed-updates">
<correction debian-security-support "Update support status of several packages">
<correction erlang "Fix use of weak TLS ciphers [CVE-2020-12872]">
<correction exiv2 "Fix denial of service issue [CVE-2018-16336]; fix over-restrictive fix for CVE-2018-10958 and CVE-2018-10999">
<correction fex "Security update">
<correction file-roller "Security fix [CVE-2020-11736]">
<correction fwupd "New upstream release; use a CNAME to redirect to the correct CDN for metadata; do not abort startup if the XML metadata file is invalid; add the Linux Foundation public GPG keys for firmware and metadata; raise the metadata limit to 10MB">
<correction glib-networking "Return bad identity error if identity is unset [CVE-2020-13645]">
<correction gnutls28 "Fix memory corruption issue [CVE-2019-3829]; fix memory leak; add support for zero length session tickets, fix connection errors on TLS1.2 sessions to some hosting providers">
<correction gosa "Tighten check on LDAP success/failure [CVE-2019-11187]; fix compatibility with newer PHP versions; backport several other patches; replace (un)serialize with json_encode/json_decode to mitigate PHP object injection [CVE-2019-14466]">
<correction heartbleeder "Rebuild against recent golang to pick up security fixes">
<correction intel-microcode "Downgrade some microcodes to previously released revisions, working around hangs on boot on Skylake-U/Y and Skylake Xeon E3">
<correction iptables-persistent "Don't fail if modprobe does">
<correction jackson-databind "Fix multiple security issues affecting BeanDeserializerFactory [CVE-2020-9548 CVE-2020-9547 CVE-2020-9546 CVE-2020-8840 CVE-2020-14195 CVE-2020-14062 CVE-2020-14061 CVE-2020-14060 CVE-2020-11620 CVE-2020-11619 CVE-2020-11113 CVE-2020-11112 CVE-2020-11111 CVE-2020-10969 CVE-2020-10968 CVE-2020-10673 CVE-2020-10672 CVE-2019-20330 CVE-2019-17531 and CVE-2019-17267]">
<correction libbusiness-hours-perl "Use explicit 4 digit years, fixing build and usage issues">
<correction libclamunrar "New upstream stable release; add an unversioned meta-package">
<correction libdbi "Comment out _error_handler() call again, fixing issues with consumers">
<correction libembperl-perl "Handle error pages from Apache &gt;= 2.4.40">
<correction libexif "Security fixes [CVE-2016-6328 CVE-2017-7544 CVE-2018-20030 CVE-2020-12767 CVE-2020-0093]; security fixes [CVE-2020-13112 CVE-2020-13113 CVE-2020-13114]; fix a buffer read overflow [CVE-2020-0182] and an unsigned integer overflow [CVE-2020-0198]">
<correction libvncserver "Fix heap overflow [CVE-2019-15690]">
<correction linux "New upstream stable release; update ABI to 4.9.0-13">
<correction linux-latest "Update for 4.9.0-13 kernel ABI">
<correction mariadb-10.1 "New upstream stable release; security fixes [CVE-2020-2752 CVE-2020-2812 CVE-2020-2814]">
<correction megatools "Add support for the new format of mega.nz links">
<correction mod-gnutls "Avoid deprecated ciphersuites in test suite; fix test failures when combined with Apache's fix for CVE-2019-10092">
<correction mongo-tools "Rebuild against recent golang to pick up security fixes">
<correction neon27 "Treat OpenSSL-related test failures as non-fatal">
<correction nfs-utils "Fix potential file overwrite vulnerability [CVE-2019-3689]; don't make all of /var/lib/nfs owned by the statd user">
<correction nginx "Fix error page request smuggling vulnerability [CVE-2019-20372]">
<correction node-url-parse "Sanitize paths and hosts before parsing [CVE-2018-3774]">
<correction nvidia-graphics-drivers "New upstream stable release; new upstream stable release; security fixes [CVE-2020-5963 CVE-2020-5967]">
<correction pcl "Fix missing dependency on libvtk6-qt-dev">
<correction perl "Fix multiple regular expression related security issues [CVE-2020-10543 CVE-2020-10878 CVE-2020-12723]">
<correction php-horde "Fix cross-site scripting vulnerability [CVE-2020-8035]">
<correction php-horde-data "Fix authenticated remote code execution vulnerability [CVE-2020-8518]">
<correction php-horde-form "Fix authenticated remote code execution vulnerability [CVE-2020-8866]">
<correction php-horde-gollem "Fix cross-site scripting vulnerability in breadcrumb output [CVE-2020-8034]">
<correction php-horde-trean "Fix authenticated remote code execution vulnerability [CVE-2020-8865]">
<correction phpmyadmin "Several security fixes [CVE-2018-19968 CVE-2018-19970 CVE-2018-7260 CVE-2019-11768 CVE-2019-12616 CVE-2019-6798 CVE-2019-6799 CVE-2020-10802 CVE-2020-10803 CVE-2020-10804 CVE-2020-5504]">
<correction postfix "New upstream stable release">
<correction proftpd-dfsg "Fix handling SSH_MSG_IGNORE packets">
<correction python-icalendar "Fix Python3 dependencies">
<correction rails "Fix possible cross-site scripting via Javascript escape helper [CVE-2020-5267]">
<correction rake "Fix command injection vulnerability [CVE-2020-8130]">
<correction roundcube "Fix cross-site scripting issue via HTML messages with malicious svg/namespace [CVE-2020-15562]">
<correction ruby-json "Fix unsafe object creation vulnerability [CVE-2020-10663]">
<correction ruby2.3 "Fix unsafe object creation vulnerability [CVE-2020-10663]">
<correction sendmail "Fix finding the queue runner control process in <q>split daemon</q> mode, <q>NOQUEUE: connect from (null)</q>, removal failure when using BTRFS">
<correction sogo-connector "New upstream version, compatible with newer Thunderbird versions">
<correction ssvnc "Fix out-of-bounds write [CVE-2018-20020], infinite loop [CVE-2018-20021], improper initialisation [CVE-2018-20022], potential denial-of-service [CVE-2018-20024]">
<correction storebackup "Fix possible privilege escalation vulnerability [CVE-2020-7040]">
<correction swt-gtk "Fix missing dependency on libwebkitgtk-1.0-0">
<correction tinyproxy "Create PID file before dropping privileges to non-root account [CVE-2017-11747]">
<correction tzdata "New upstream stable release">
<correction websockify "Fix missing dependency on python{3,}-pkg-resources">
<correction wpa "Fix AP mode PMF disconnection protection bypass [CVE-2019-16275]; fix MAC randomisation issues with some cards">
<correction xdg-utils "Sanitise window name before sending it over D-Bus; correctly handle directories with names containing spaces; create the <q>applications</q> directory if needed">
<correction xml-security-c "Fix length calculation in the concat method">
<correction xtrlock "Fix blocking of (some) multitouch devices while locked [CVE-2016-10894]">
</table>


<h2>Säkerhetsuppdateringar</h2>


<p>Denna revision lägger till följande säkerhetsuppdateringar till den gamla stabila utgåvan.
Säkerhetsgruppen har redan släppt bulletiner för alla dessa
uppdateringar:</p>

<table border=0>
<tr><th>Bulletin-ID</th>  <th>Paket</th></tr>
<dsa 2017 4005 openjfx>
<dsa 2018 4255 ant>
<dsa 2018 4352 chromium-browser>
<dsa 2019 4379 golang-1.7>
<dsa 2019 4380 golang-1.8>
<dsa 2019 4395 chromium>
<dsa 2019 4421 chromium>
<dsa 2020 4616 qemu>
<dsa 2020 4617 qtbase-opensource-src>
<dsa 2020 4618 libexif>
<dsa 2020 4619 libxmlrpc3-java>
<dsa 2020 4620 firefox-esr>
<dsa 2020 4621 openjdk-8>
<dsa 2020 4622 postgresql-9.6>
<dsa 2020 4624 evince>
<dsa 2020 4625 thunderbird>
<dsa 2020 4628 php7.0>
<dsa 2020 4629 python-django>
<dsa 2020 4630 python-pysaml2>
<dsa 2020 4631 pillow>
<dsa 2020 4632 ppp>
<dsa 2020 4633 curl>
<dsa 2020 4634 opensmtpd>
<dsa 2020 4635 proftpd-dfsg>
<dsa 2020 4637 network-manager-ssh>
<dsa 2020 4639 firefox-esr>
<dsa 2020 4640 graphicsmagick>
<dsa 2020 4642 thunderbird>
<dsa 2020 4646 icu>
<dsa 2020 4647 bluez>
<dsa 2020 4648 libpam-krb5>
<dsa 2020 4650 qbittorrent>
<dsa 2020 4653 firefox-esr>
<dsa 2020 4655 firefox-esr>
<dsa 2020 4656 thunderbird>
<dsa 2020 4657 git>
<dsa 2020 4659 git>
<dsa 2020 4660 awl>
<dsa 2020 4663 python-reportlab>
<dsa 2020 4664 mailman>
<dsa 2020 4666 openldap>
<dsa 2020 4668 openjdk-8>
<dsa 2020 4670 tiff>
<dsa 2020 4671 vlc>
<dsa 2020 4673 tomcat8>
<dsa 2020 4674 roundcube>
<dsa 2020 4675 graphicsmagick>
<dsa 2020 4676 salt>
<dsa 2020 4677 wordpress>
<dsa 2020 4678 firefox-esr>
<dsa 2020 4683 thunderbird>
<dsa 2020 4685 apt>
<dsa 2020 4686 apache-log4j1.2>
<dsa 2020 4687 exim4>
<dsa 2020 4688 dpdk>
<dsa 2020 4689 bind9>
<dsa 2020 4692 netqmail>
<dsa 2020 4693 drupal7>
<dsa 2020 4695 firefox-esr>
<dsa 2020 4698 linux>
<dsa 2020 4700 roundcube>
<dsa 2020 4701 intel-microcode>
<dsa 2020 4702 thunderbird>
<dsa 2020 4703 mysql-connector-java>
<dsa 2020 4704 vlc>
<dsa 2020 4705 python-django>
<dsa 2020 4706 drupal7>
<dsa 2020 4707 mutt>
<dsa 2020 4711 coturn>
<dsa 2020 4713 firefox-esr>
<dsa 2020 4715 imagemagick>
<dsa 2020 4717 php7.0>
<dsa 2020 4718 thunderbird>
</table>


<h2>Borttagna paket</h2>

<p>Följande paket har tagits bort på grund av omständigheter utom vår kontroll:</p>

<table border=0>
<tr><th>Paket</th>               <th>Orsak</th></tr>
<correction certificatepatrol "Incompatible with newer Firefox ESR versions">
<correction colorediffs-extension "Incompatible with newer Thunderbird versions">
<correction dynalogin "Depends on to-be-removed simpleid">
<correction enigmail "Incompatible with newer Thunderbird versions">
<correction firefox-esr "[armel] No longer supported (requires nodejs)">
<correction firefox-esr "[mips mipsel mips64el] No longer supported (needs newer rustc)">
<correction getlive "Broken due to Hotmail changes">
<correction gplaycli "Broken by Google API changes">
<correction kerneloops "Upstream service no longer available">
<correction libmicrodns "Security issues">
<correction libperlspeak-perl "Security issues; unmaintained">
<correction mathematica-fonts "Relies on unavailable download location">
<correction pdns-recursor "Security issues; unsupported">
<correction predictprotein "Depends on to-be-removed profphd">
<correction profphd "Unusable">
<correction quotecolors "Incompatible with newer Thunderbird versions">
<correction selenium-firefoxdriver "Incompatible with newer Firefox ESR versions">
<correction simpleid "Does not work with PHP7">
<correction simpleid-ldap "Depends on to-be-removed simpleid">
<correction torbirdy "Incompatible with newer Thunderbird versions">
<correction weboob "Unmaintained; already removed from later releases">
<correction yahoo2mbox "Broken for several years">

</table>

<h2>Debianinstalleraren</h2>
<p>Installeraren har uppdaterats för att inkludera rättningarna som har inkluderats i den
gamla stabila utgåvan med denna punktutgåva.</p>

<h2>URLer</h2>

<p>Den fullständiga listan på paket som har förändrats i denna revision:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>Den aktuella gamla stabila utgåvan:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/oldstable/">
</div>

<p>Föreslagna uppdateringar till den gamla stabila utgåvan:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/oldstable-proposed-updates">
</div>

<p>Information om den gamla stabila utgåvan (versionsfakta, kända problem osv.):</p>

<div class="center">
  <a
  href="$(HOME)/releases/oldstable/">https://www.debian.org/releases/oldstable/</a>
</div>

<p>Säkerhetsbulletiner och information:</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>

<h2>Om Debian</h2>

<p>Debianprojektet är en grupp utvecklare av Fri mjukvara som
donerar sin tid och kraft för att producera det helt
fria operativsystemet Debian.</p>

<h2>Kontaktinformation</h2>

<p>För ytterligare information, vänligen besök Debians webbplats på
<a href="$(HOME)/">https://www.debian.org/</a>, skicka e-post till
&lt;press@debian.org&gt;, eller kontakta gruppen för stabila utgåvor på
&lt;debian-release@lists.debian.org&gt;.</p>


