<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Multiple vulnerabilities were discovered in Python2.7, an interactive
high-level object-oriented language.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-20852">CVE-2018-20852</a>

     <p>By using a malicious server an attacker might steal cookies that are
     meant for other domains.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5010">CVE-2019-5010</a>

     <p>NULL pointer dereference using a specially crafted X509 certificate.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-9636">CVE-2019-9636</a>

     <p>Improper Handling of Unicode Encoding (with an incorrect netloc)
     during NFKC normalization resulting in information disclosure
     (credentials, cookies, etc. that are cached against a given
     hostname).  A specially crafted URL could be incorrectly parsed to
     locate cookies or authentication data and send that information to
     a different host than when parsed correctly.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-9740">CVE-2019-9740</a>

     <p>An issue was discovered in urllib2 where CRLF injection is possible
     if the attacker controls a url parameter, as demonstrated by the
     first argument to urllib.request.urlopen with \r\n (specifically in
     the query string after a ? character) followed by an HTTP header or
     a Redis command.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-9947">CVE-2019-9947</a>

     <p>An issue was discovered in urllib2 where CRLF injection is possible
     if the attacker controls a url parameter, as demonstrated by the
     first argument to urllib.request.urlopen with \r\n (specifically in
     the path component of a URL that lacks a ? character) followed by an
     HTTP header or a Redis command. This is similar to the <a href="https://security-tracker.debian.org/tracker/CVE-2019-9740">CVE-2019-9740</a>
     query string issue.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-9948">CVE-2019-9948</a>

     <p>urllib supports the local_file: scheme, which makes it easier for
     remote attackers to bypass protection mechanisms that blacklist
     file: URIs, as demonstrated by triggering a
     urllib.urlopen('local_file:///etc/passwd') call.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-10160">CVE-2019-10160</a>

     <p>A security regression of <a href="https://security-tracker.debian.org/tracker/CVE-2019-9636">CVE-2019-9636</a> was discovered which still
     allows an attacker to exploit <a href="https://security-tracker.debian.org/tracker/CVE-2019-9636">CVE-2019-9636</a> by abusing the user and
     password parts of a URL. When an application parses user-supplied
     URLs to store cookies, authentication credentials, or other kind of
     information, it is possible for an attacker to provide specially
     crafted URLs to make the application locate host-related information
     (e.g. cookies, authentication data) and send them to a different
     host than where it should, unlike if the URLs had been correctly
     parsed. The result of an attack may vary based on the application.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-16056">CVE-2019-16056</a>

     <p>The email module wrongly parses email addresses that contain
     multiple @ characters. An application that uses the email module and
     implements some kind of checks on the From/To headers of a message
     could be tricked into accepting an email address that should be
     denied.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-20907">CVE-2019-20907</a>

     <p>Opening a crafted tar file could result in an infinite loop due to
     missing header validation.</p>


<p>For Debian 9 stretch, these problems have been fixed in version
2.7.13-2+deb9u4.</p>

<p>We recommend that you upgrade your python2.7 packages.</p>

<p>For the detailed security status of python2.7 please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/python2.7">https://security-tracker.debian.org/tracker/python2.7</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p></li>

</ul>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2337.data"
# $Id: $
