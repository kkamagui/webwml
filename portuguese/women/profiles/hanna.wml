<define-tag pagetitle>Hanna Wallach</define-tag>
#use wml::debian::profiles
#include "$(ENGLISHDIR)/women/profiles/profiles.def"
#use wml::debian::translation-check translation="08da87bf88f0132c810bd37c7312b5d0c4675a51"

<profile name="Hanna Wallach" picture="hanna.jpg">
    <URL>https://dirichlet.net</URL>
    <email>hanna@join-the-dots.org</email>

    <question1>
    <answer><p>
    Desde abril de 1999
    </p></answer>

    <question2>
    <answer><p>
    Ainda não; no entanto, pretendo entrar no processo para me tornar
    nova mantenedora nos próximos meses.
    </p></answer>

    <question3>
    <answer><p>
    Minha principal contribuição para o projeto Debian é empacotar.
    Atualmente, co-mantenho um pacote e acabei de registrar um ITP (intenção
    de empacotar) para outro. <br /> Eu também tenho um papel ativo em
    encorajar outras pessoas a usar o Debian. Eu acredito que uma das
    maneiras mais importantes de apoiar o crescimento de um projeto como o
    Debian é incentivar as pessoas a descobrirem mais e, se interessadas,
    se envolverem. Nos últimos anos, iniciei discussões formais e informais
    sobre o Debian com uma variedade de indivíduos e ajudei a realizar
    vários festivais de instalação do Debian.
    </p></answer>

    <question4>
    <answer><p>
    O Debian tem uma das comunidades de desenvolvedores(as)
    mais ativas e interessantes de qualquer distribuição Linux - foi isso
    que originalmente me atraiu para o projeto.
    </p></answer>

    <question5>
    <answer><p>
    Pense no que você gostaria de contribuir para o Debian e no que você
    espera obter com o seu envolvimento. Em um nível mais prático, ajude
    a corrigir bugs, adote pacotes órfãos, confira a página <a
    href="https://www.debian.org/devel/wnpp/">WNPP</a> (pacotes que precisam
    de trabalho e futuros pacotespacotes) regularmente e leia as
    <a href="https://lists.debian.org/">listas de discussão do Debian</a>
    - elas são uma ótima maneira de aprender mais sobre a comunidade e o
    espírito do Debian.
    </p></answer>

    <question6>
    <answer><p>
    Faço parte do projeto <a href="http://www.cl.cam.ac.uk/women/"
    >Women@CL</a>, uma iniciativa baseada em Cambridge que fornece
    atividades locais, nacionais e internacionais para mulheres
    envolvidas com pesquisa em computação e liderança acadêmica. Também
    faço parte de <a href="http://www.seas.upenn.edu/~cisters/"
    >CISters</a>, um grupo informal de mulheres graduadas no Departamento
    de Computação e Ciência da Informação da Universidade da Pensilvânia.
    </p></answer>

    <question7>
    <answer><p>
    Eu me formei no Laboratório de Ciência da Computação da Universidade de
    Cambridge depois de uma transferência da engenharia para a ciência da
    computação durante meu segundo ano. Desde então, obtive um mestrado na
    Universidade de Edimburgo, onde me especializei em programação neural e
    aprendizado de dados. Comecei o doutorado no grupo Inference da Cambridge.
    Quando não estou trabalhando na minha tese, passo muito tempo tomando café,
    mexendo com computadores, contribuindo para o projeto Debian e atualizando
    meu <a href="http://join-the-dots.org/">blog</a>.
    </p></answer>
</profile>
