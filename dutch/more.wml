#use wml::debian::template MAINPAGE="true"
#use wml::debian::recent_list
#use wml::debian::translation-check translation="9964ecf186f13ed156d7ef0745a70952a6f478f9"

<a id=community></a>
<h1>Debian is een samenwerkingsverband van mensen</h1>
<p>Duizenden mensen van over de hele wereld werken samen en zetten daarbij Vrije Software en de noden van gebruikers op de eerste plaats.</p>


<ul>
  <li>
    <a href="../intro/about">Mensen:</a>
    Wie we zijn, wat we doen
  </li>
  <li>
    <a href="../intro/">Filosofie:</a>
    Waarom we het doen en hoe we het doen
  </li>
  <li>
    <a href="../devel/join/">Raak betrokken:</a>
    U kunt hier deel van uitmaken!
  </li>
  <p>
  <li>
    <a href="../social_contract">Sociaal contract:</a>
    Onze morele agenda
  </li>
  <li>
    <a href="../code_of_coduct">Gedragscode</a>
  </li>
  <li>
    <a href="../partners/">Partners:</a>
    Bedrijven en organisaties die het Debian project op lange termijn assistentie verlenen
  </li>
  <li>
    <a href="../donations">Giften</a>
  </li>
  <li>
    <a href="../legal/">Juridische info </a>
  </li>
  <li>
    <a href="../legal/privacy">Gegevensbescherming</a>
  </li>
  <li>
    <a href="../contact">Ons contacteren</a>
  </li>
</ul>

<hr>

<a id=software></a>
<h1>Debian is een vrij besturingssysteem</h1>
<p>We starten met Linux en voegen er verschillende duizenden toepassingen aan toe om tegemoet te komen aan de behoeften van de gebruikers.</p>

<ul>
  <li>
    <a href="../distrib">Downloaden:</a>
    Andere varianten van Debian images
  </li>
  <li>
    <a href="../support">Ondersteuning:</a>
    Hulp krijgen
  </li>
  <li>
    <a href="../security">Veiligheid:</a>
    Laatste update
    <:= get_recent_list ('security/2w', '1', '$(ENGLISHDIR)', 'bydate', '(2000\d+\w+|dsa-\d+)' ) :>
  </li>
  <p>
  <li>
    <a href="../distrib/packages"> Softwarepakketten:</a>
    Zoeken en bladeren in de lange lijst met onze software
  </li>
  <li>
    <a href="../doc"> Documentatie</a>
  </li>
  <li>
    <a href="https://wiki.debian.org"> Debian wiki</a>
  </li>
  <li>
    <a href="../Bugs"> Bugrapporten</a>
  </li>
  <li>
    <a href="https://lists.debian.org/">
    Mailinglijsten</a>
  </li>
  <li>
    <a href="../blends"> Specifieke uitgaven (pure blends):</a>
    Metapakketten die aan specifieke noden beantwoorden
  </li>
  <li>
    <a href="../devel"> Ontwikkelaarshoek:</a>
    Informatie die hoofdzakelijk voor de ontwikkelaars van Debian van belang is
  </li>
  <li>
    <a href="../ports"> Ports/Architecturen:</a>
    CPU-architecturen die door ons ondersteund worden
  </li>
</ul>
