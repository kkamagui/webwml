#use wml::debian::translation-check translation="907a0371eb05342911768c66ad56f028349d3301" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Mathy Vanhoef (NYUAD) et Eyal Ronen (Tel Aviv University et KU Leuven)
ont trouvé plusieurs vulnérabilités dans l'implémentation de WPA
découvertes dans wpa_supplication (station) et hostapd (point d'accès). Ces
vulnérabilités sont aussi connues ensemble sous le nom de <q>Dragonblood</q>.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-9495">CVE-2019-9495</a>

<p>Une attaque par canal auxiliaire basée sur le cache à l'encontre de
l'implémentation de EAP-pwd : un attaquant ayant la possibilité d'exécuter
du code sans droit sur la machine cible (y compris par exemple du code
javascript dans un navigateur sur un smartphone) durant la négociation de
connexion pourrait déduire suffisamment d'informations pour découvrir le
mot de passe dans une attaque par dictionnaire.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-9497">CVE-2019-9497</a>

<p>Une attaque par réflexion à l'encontre de l'implémentation du serveur
EAP-pwd : un manque de validation de la valeur des scalaires et éléments
dans les messages EAP-pwd-Commit pourrait avoir pour conséquence des
attaques qui permettraient de réussir des échanges d'authentification
EAP-pwd sans que l'attaquant ait à connaître le mot de passe. Cela
n'aboutit pas à ce que l'attaquant soit capable d'obtenir la clé de
session, de réussir l'échange de clé suivant et d'accéder au réseau.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-9498">CVE-2019-9498</a>

<p>L'absence de validation par le serveur EAP-pwd de « commit » pour les
scalaires et les éléments : hostapd ne valide pas les valeurs reçues dans
le message EAP-pwd-Commit, aussi un attaquant pourrait utiliser un message
de « commit » contrefait pour l'occasion pour manipuler les échanges afin
que hostapd obtienne une clé de session à partir d'un ensemble limité de
valeurs possibles. Cela pourrait avoir pour conséquence qu'un attaquant
soit capable de réussir une authentification et obtienne l'accès au réseau.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-9499">CVE-2019-9499</a>

<p>L'absence de validation par le pair EAP-pwd de « commit » pour les
scalaires et les éléments : wpa_supplicant ne valide pas les valeurs reçues
dans le message EAP-pwd-Commit, aussi un attaquant pourrait utiliser un
message de « commit » contrefait pour l'occasion pour manipuler les
échanges afin que wpa_supplicant obtienne une clé de session à partir d'un
ensemble limité de valeurs possibles. Cela pourrait avoir pour conséquence
qu'un attaquant soit capable de réussir une authentification et fonctionne
comme un protocole d'authentification (AP) véreux.</p>

</ul>

<p>Notez que le surnom Dragonblood s'applique aussi
à <a href="https://security-tracker.debian.org/tracker/CVE-2019-9494">\
CVE-2019-9494</a> et à <a
href="https://security-tracker.debian.org/tracker/CVE-2014-9496">\
CVE-2014-9496</a> qui sont des vulnérabilités dans le protocole SAE dans
WPA3. SAE n'est pas activé dans les constructions dans Debian Stretch de
wpa qui n'est donc pas vulnérable par défaut.</p>

<p>Du fait de la complexité du processus de rétroportage, le correctif pour
ces vulnérabilités est partiel. Il est recommandé aux utilisateurs
d'utiliser des mots de passe robustes pour éviter les attaques par
dictionnaire ou d'utiliser une version basée sur 2.7 issue de
stretch-backports (version supérieure à 2:2.7+git20190128+0c1e29f-4).</p>

<p>Pour la distribution stable (Stretch), ces problèmes ont été corrigés
dans la version 2:2.4-1+deb9u3.</p>

<p>Nous vous recommandons de mettre à jour vos paquets wpa.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de wpa, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/wpa">\
https://security-tracker.debian.org/tracker/wpa</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2019/dsa-4430.data"
# $Id: $
