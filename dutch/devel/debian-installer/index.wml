#use wml::debian::template title="Het installatieprogramma van Debian" NOHEADER="true"
#use wml::debian::recent_list
#include "$(ENGLISHDIR)/releases/info"
#include "$(ENGLISHDIR)/devel/debian-installer/images.data"
#use wml::debian::translation-check translation="27016443323dac06f67403e3c8fddd149558e1e2"

<h1>Nieuws</h1>

<p><:= get_recent_list('News/$(CUR_YEAR)', '2',
'$(ENGLISHDIR)/devel/debian-installer', '', '\d+\w*' ) :>
<a href="News">Ouder nieuws</a>
</p>

<h1>Installeren met het installatieprogramma van Debian</h1>


<p>
<if-stable-release release="buster">
<strong>Voor de officiële installatie-media voor Debian <current_release_buster> en voor
informatie</strong>, zie
<a href="$(HOME)/releases/bullseye/debian-installer">de buster-pagina</a>.
</if-stable-release>
<if-stable-release release="bullseye">
<strong>Voor de officiële installatie-media voor Debian <current_release_bullseye> en voor
informatie</strong>, zie
<a href="$(HOME)/releases/bullseye/debian-installer">de bullseye-pagina</a>.
</if-stable-release>
</p>

<div class="tip">
<p>
Alle images waarnaar hieronder wordt verwezen betreffen de versie van het
installatieprogramma van Debian
dat wordt ontwikkeld voor de volgende release van Debian.
Standaard wordt hiermee Debian testing (<q><current_testing_name></q>) geïnstalleerd.
</p>
</div>

<if-stable-release release="buster">
<p>

<strong>Bij de installatie van Debian testing</strong> raden we u aan om release
<strong><humanversion /></strong> van het installatieprogramma te
gebruiken, nadat u de <a href="errata">errata</a> heeft doorgenomen.
De volgende images zijn beschikbaar voor <humanversion />:
<!--
<strong>Bij de installatie van Debian testing</strong> raden we u aan om de
<strong>dagelijks gemaakte images</strong> van het installatieprogramma te
gebruiken.  De volgende dagelijks gemaakte images zijn beschikbaar:
-->
</p>

<h2>Officiële release</h2>

<div class="line">
<div class="item col50">
<strong>netinst (meestal 180-450 MB) cd-images</strong>
<netinst-images />
</div>


</div>

<div class="line">
<div class="item col50">
<strong>cd</strong>
<full-cd-images />
</div>

<div class="item col50 lastcol">
<strong>dvd</strong>
<full-dvd-images />
</div>

</div>


<div class="line">
<div class="item col50">
<strong>cd (via <a href="$(HOME)/CD/jigdo-cd">jigdo</a>)</strong>
<full-cd-jigdo />
</div>

<div class="item col50 lastcol">
<strong>dvd (via <a href="$(HOME)/CD/jigdo-cd">jigdo</a>)</strong>
<full-dvd-jigdo />
</div>

</div>

<div class="line">
<div class="item col50">
<strong>Blu-ray  (via <a href="$(HOME)/CD/jigdo-cd">jigdo</a>)</strong>
<full-bd-jigdo />
</div>

<div class="item col50 lastcol">
<strong>andere images (netboot, USB-stick, enz.)</strong>
<other-images />
</div>
</div>

<p>
Of installeer het <b>huidige wekelijkse momentopname van Debian testing</b> die
dezelfde versie van het installatieprogramma gebruikt als de laatste release:
</p>

<h2>Actuele wekelijkse momentopnames</h2>

<div class="line">
<div class="item col50">
<strong>cd</strong>
<devel-full-cd-images />
</div>

<div class="item col50 lastcol">
<strong>dvd</strong>
<devel-full-dvd-images />
</div>
</div>

<div class="line">
<div class="item col50">
<strong>cd (via <a href="$(HOME)/CD/jigdo-cd">jigdo</a>)</strong>
<devel-full-cd-jigdo />
</div>

<div class="item col50 lastcol">
<strong>dvd (via <a href="$(HOME)/CD/jigdo-cd">jigdo</a>)</strong>
<devel-full-dvd-jigdo />
</div>
</div>

<div class="line">
<div class="item col50">
<strong>Blu-ray (via <a href="$(HOME)/CD/jigdo-cd">jigdo</a>)</strong>
<devel-full-bd-jigdo />
</div>
</div>


</if-stable-release>
<!--
<p>
Als u liever het nieuwste en beste gebruikt, om ons te helpen met het
testen van de komende release van het installatieprogramma ofwel vanwege
hardware of andere problemen, probeer dan een van deze <strong>dagelijks
gemaakte images</strong> welke de meest recente versies bevatten van de
componenten van het installatieprogramma.
</p>
-->

<h2>Actuele dagelijkse momentopnames</h2>

<div class="line">
<div class="item col50">
<strong>netinst (meestal 150-280 MB) <!-- en businesscard (meestal 20-50 MB) --> cd-images</strong>
<devel-small-cd-images />
</div>

<div class="item col50 lastcol">
<strong>netinst <!-- en businesscard --> cd-images (via <a href="$(HOME)/CD/jigdo-cd">jigdo</a>)</strong>
<devel-small-cd-jigdo />
</div>
</div>

<div class="line">
<div class="item col50">
<strong>netinst multi-arch cd-images</strong>
<devel-multi-arch-cd />
</div>

<div class="item col50 lastcol">
<strong>andere images (netboot, USB-stick, etc.)</strong>
<devel-other-images />
</div>
</div>

<p>
Als uw systeem hardware bevat die vereist dat voor
het stuurprogramma van het apparaat <strong>firmware geladen wordt</strong>,
dan kunt u een van de <a
href="https://cdimage.debian.org/cdimage/unofficial/non-free/firmware/">tarballs
van veelvoorkomende firmware-pakketten</a> gebruiken. Instructies over het
gebruik van deze tarballs en algemene informatie over het laden van firmware
tijdens de installatie is te vinden in de installatiehandleiding (zie
documentatie hieronder).
</p>

<p>
<strong>Opmerkingen</strong>
</p>
<ul>
#	<li>We raden u aan om de
#	<a href="https://wiki.debian.org/DebianInstaller/Today">bekende
#	problemen</a>
#	door te nemen voordat u de dagelijks gemaakte images probeert.</li>
	<li>Een architectuur kan (tijdelijk) worden weggelaten uit het overzicht
	van de dagelijks gemaakte images als deze images niet (betrouwbaar)
	beschikbaar zijn.</li>
	<li>Voor de installatie-images zijn er verificatiebestanden
    (<tt>SHA512SUMS</tt> en <tt>SHA256SUMS</tt>)
	beschikbaar in de map waar de images staan.</li>
	<li>Voor het downloaden van volledige cd- en dvd-images wordt het gebruik
	van jigdo aanbevolen.</li>
	<li>Er is slechts een beperkt aantal images van de volledige dvd-set
	beschikbaar als ISO-bestand om direct te downloaden. De meeste gebruikers
	hebben geen behoefte aan alle software die beschikbaar is op alle
	schijven, en dus is de volledige set enkel via jigdo beschikbaar
	om ruimte te sparen op download-servers en spiegelservers.</li>
	<li>Het multi-arch <em>netinst cd</em>-image ondersteunt i386/amd64; de
	installatie is hetzelfde als installeren vanaf een netinst-image van een
	enkele architectuur.</li>
</ul>

<p>
<strong>Wanneer u het installatieprogramma van Debian heeft
gebruikt</strong>, stuur ons dan
een <a
href="https://d-i.debian.org/manual/nl.amd64/ch05s04.html#submit-bug">installatie-rapport</a>,
ook als u geen problemen hebt ondervonden.
</p>

<h1>Documentatie</h1>

<p>
<strong>Als u maar één document doorleest</strong> voor de installatie, lees
dan onze <a
href="https://d-i.debian.org/manual/nl.amd64/apa.html">Installatie-howto</a>,
een snelle doorloop van het installatieproces. Andere nuttige documentatie is
onder andere:
</p>

<ul>
<li>Installatiehandleiding:
#  <a href="$(HOME)/releases/stable/installmanual">versie voor de
#  huidige release</a>
#  &mdash;
  <a href="$(HOME)/releases/testing/installmanual">ontwikkelingsversie (testing)</a>
  &mdash;
  <a href="https://d-i.debian.org/manual/">meest recente versie (Git)</a>
<br />
gedetailleerde installatie-instructies</li>
<li><a
href="https://wiki.debian.org/DebianInstaller/FAQ">Debian-
Installer FAQ</a>
en <a href="$(HOME)/CD/faq/">Debian-CD FAQ</a><br />
veelgestelde vragen en antwoorden</li>
<li><a
href="https://wiki.debian.org/DebianInstaller">Debian-Installer Wiki</a><br />
door de gemeenschap onderhouden documentatie</li>
</ul>

<h1>Contact opnemen</h1>

<p>
De <a href="https://lists.debian.org/debian-boot/">debian-boot mailinglijst</a>
is de voornaamste plek voor discussie over en werk aan het
installatieprogramma van Debian.
</p>

<p>
We hebben ook een IRC-kanaal, #debian-boot op <tt>irc.debian.org</tt>. Dit
kanaal wordt voornamelijk gebruikt voor ontwikkeling, maar soms ook voor
ondersteuning. Als u hier geen reactie krijgt, probeer dan de mailinglijst.
</p>
