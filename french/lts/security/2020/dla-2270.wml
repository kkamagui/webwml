#use wml::debian::translation-check translation="91e9e0da7a452359c7acb051ea8260a29b255826" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Plusieurs CVE ont été signalés à l’encontre de src:jackson-databind.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-14060">CVE-2020-14060</a>

<p>FasterXML jackson-databind 2.x avant la version 2.9.10.5 gère incorrectement
l’interaction entre les gadgets de sérialisation et la saisie, en relation avec
oadd.org.apache.xalan.lib.sql.JNDIConnectionPool (alias apache/drill).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-14061">CVE-2020-14061</a>

<p>FasterXML jackson-databind 2.x avant la version 2.9.10.5 gère incorrectement
l’interaction entre les gadgets de sérialisation et la saisie, en relation avec
oracle.jms.AQjmsQueueConnectionFactory,
oracle.jms.AQjmsXATopicConnectionFactory,
oracle.jms.AQjmsTopicConnectionFactory,
oracle.jms.AQjmsXAQueueConnectionFactory et oracle.jms.AQjmsXAConnectionFactory
(alias weblogic/oracle-aqjms).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-14062">CVE-2020-14062</a>

<p>FasterXML jackson-databind 2.x avant la version 2.9.10.5 gère incorrectement
l’interaction entre les gadgets de sérialisation et la saisie, en relation avec
com.sun.org.apache.xalan.internal.lib.sql.JNDIConnectionPool
(alias xalan2).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-14195">CVE-2020-14195</a>

<p>FasterXML jackson-databind 2.x avant la version 2.9.10.5 gère incorrectement
l’interaction entre les gadgets de sérialisation et la saisie, en relation avec
org.jsecurity.realm.jndi.JndiRealmFactory (alias org.jsecurity).</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 2.4.2-2+deb8u15.</p>

<p>Nous vous recommandons de mettre à jour vos paquets jackson-databind.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2270.data"
# $Id: $
