#use wml::debian::template title="Debian 7 -- Errata" BARETITLE=true
#use wml::debian::toc
#use wml::debian::translation-check translation="e673bc1c67aefd757c294a7f38eb9aa9f501bd2e"

#include "$(ENGLISHDIR)/releases/info"

<toc-display/>


# <toc-add-entry name="known_probs">Problemas conhecidos</toc-add-entry>
<toc-add-entry name="security">Problemas de segurança</toc-add-entry>

<p>O time de segurança do Debian envia atualizações de pacotes para a versão
estável (stable), na qual identificaram problemas relacionados à segurança.
Por favor, consulte as <a href="$(HOME)/security/">páginas de segurança</a>
para informações sobre qualquer problema de segurança identificado no
<q>Wheezy</q>.</p>

<p>Se você usa o APT, adicione a seguinte linha em <tt>/etc/apt/sources.list</tt>
para ser capaz de acessar as últimas atualizações de segurança:</p>

<pre>
  deb http://security.debian.org/ wheezy/updates main contrib non-free
</pre>

<p>A seguir, execute <kbd>apt-get update</kbd> seguido de
<kbd>apt-get upgrade</kbd>.</p>


<toc-add-entry name="pointrelease">Versões pontuais</toc-add-entry>

<p>Às vezes, no caso de diversos problemas críticos ou atualizações de
segurança, a distribuição já lançada é atualizada. Geralmente, isso é
indicado como uma versão pontual.</p>

<ul>
  <li>A primeira versão pontual, 7.1, foi lançada em
      <a href="$(HOME)/News/2013/20130615">15 de junho de 2013</a>.</li>
  <li>A segunda versão pontual, 7.2, foi lançada em
      <a href="$(HOME)/News/2013/20131012">12 de outubro de 2013</a>.</li>
  <li>A terceira versão pontual, 7.3, foi lançada em
      <a href="$(HOME)/News/2013/20131214">14 de dezembro de 2013</a>.</li>
  <li>A quarta versão pontual, 7.4, foi lançada em
      <a href="$(HOME)/News/2014/20140208">8 de fevereiro de 2014</a>.</li>
  <li>A quinta versão pontual, 7.5, foi lançada em
      <a href="$(HOME)/News/2014/20140426">26 de abril de 2014</a>.</li>
  <li>A sexta versão pontual, 7.6, foi lançada em
      <a href="$(HOME)/News/2014/20140712">12 de julho de 2014</a>.</li>
  <li>A sétima versão pontual, 7.7, foi lançada em
      <a href="$(HOME)/News/2014/20141018">18 de outubro de 2014</a>.</li>
  <li>A oitava versão pontual, 7.8, foi lançada em
      <a href="$(HOME)/News/2015/20150110">10 de janeiro de 2015</a>.</li>
  <li>A nona versão pontual, 7.9, foi lançada em
      <a href="$(HOME)/News/2015/2015090502">5 de setembro de 2015</a>.</li>
  <li>A décima versão pontual, 7.10, foi lançada em
      <a href="$(HOME)/News/2016/2016040202">2 de abril de 2016</a>.</li>
  <li>A décima primeira versão pontual, 7.11, foi lançada em
      <a href="$(HOME)/News/2016/2016060402">4 de junho de 2016</a>.</li>
</ul>

<ifeq <current_release_wheezy> 7.0 "

<p>Ainda não existem versões pontuais para o Debian 7.</p>" "

<p>Veja o <a
href=http://http.us.debian.org/debian/dists/wheezy/ChangeLog>\
ChangeLog</a>
para detalhes sobre as mudanças entre a 7.0 e a <current_release_wheezy/>.</p>"/>


<p>As correções para a versão estável (stable) lançada
passam por um período estendido de testes antes de serem aceitas no repositório.
Contudo, essas correções estão disponíveis no diretório
<a href="http://ftp.debian.org/debian/dists/wheezy-proposed-updates/">\
dists/wheezy-proposed-updates</a> de qualquer espelho de repositório do
Debian.</p>

<p>Se você usa o APT para atualizar seus pacotes, você pode instalar
as atualizações propostas ao adicionar a seguinte linha em
<tt>/etc/apt/sources.list</tt>:</p>

<pre>
  \# adições propostas para uma versão pontual 7
  deb http://ftp.us.debian.org/debian wheezy-proposed-updates main contrib non-free
</pre>

<p>A seguir, execute <kbd>apt-get update</kbd> seguido de
<kbd>apt-get upgrade</kbd>.</p>


<toc-add-entry name="installer">Sistema de instalação</toc-add-entry>

<p>
Para informações sobre errata e atualizações do sistema de instalação, veja a
página <a href="debian-installer/">informações de instalação</a>.
</p>
