#use wml::debian::blend title="Usando os metapacotes"
#use "../navbar.inc"
#use wml::debian::translation-check translation="6d2e0afb56e760364713c2cca2c9f6407c9a744f"

<p>Metapacotes são usados pelo blend como um modo conveniente de reunir
pacotes de software relacionados. Cada metapacote, quando instalado, fará
com que o sistema de gerenciamento de pacotes instale os pacotes relacionados
à tarefa.</p>

<p>Os seguintes metapacotes são mantidos atualmente pelo blend:</p>

<table>
	<tr><th>Nome da tarefa</th><th>Metapacote</th><th>Descrição</th><th>Catálogo</th></tr>
	<tr>
		<td>Antennas</td>
		<td><a href="https://packages.debian.org/unstable/hamradio-antenna"><code>hamradio-antenna</code></a></td>
		<td>Esta tarefa contém pacotes úteis para modelagem de antenas.</td>
		<td><a href="https://blends.debian.org/hamradio/tasks/antenna">Link</a></td>
	</tr>
	<tr>
		<td>Data Modes</td>
		<td><a href="https://packages.debian.org/unstable/hamradio-datamodes"><code>hamradio-datamodes</code></a></td>
		<td>Esta tarefa contém pacotes úteis para usar modos de dados
                         tais como RTTY e SSTV, incluindo modos de sinal fraco
                         como JT65.</td>
		<td><a href="https://blends.debian.org/hamradio/tasks/datamodes">Link</a></td>
	</tr>
	<tr>
		<td>Digital Voice</td>
		<td><a href="https://packages.debian.org/unstable/hamradio-digitalvoice"><code>hamradio-digitalvoice</code></a></td>
		<td>Esta tarefa contém pacotes úteis para usar modos de voz
                         digital em RF e para internet linking.
		<td><a href="https://blends.debian.org/hamradio/tasks/digitalvoice">Link</a></td>
	</tr>
	<tr>
		<td>Logging</td>
		<td><a href="https://packages.debian.org/unstable/hamradio-logging"><code>hamradio-logging</code></a></td>
		<td>Esta tarefa contém pacotes úteis para logs (incluindo
			 contests).</td>
		<td><a href="https://blends.debian.org/hamradio/tasks/logging">Link</a></td>
	</tr>
	<tr>
		<td>Morse</td>
		<td><a href="https://packages.debian.org/unstable/hamradio-morse"><code>hamradio-morse</code></a></td>
		<td>Esta tarefa contém pacotes úteis para operação CW e para
			 aprender morse.</td>
		<td><a href="https://blends.debian.org/hamradio/tasks/morse">Link</a></td>
	</tr>
	<tr>
		<td>Non-Amateur Modes</td>
		<td><a href="https://packages.debian.org/unstable/hamradio-nonamateur"><code>hamradio-nonamateur</code></a></td>
		<td>Esta tarefa contém pacotes úteis para escuta de modos
			não amadores como AIS e ADS-B.</td>
		<td><a href="https://blends.debian.org/hamradio/tasks/nonamateur">Link</a></td>
	</tr>
	<tr>
		<td>Packet Modes</td>
		<td><a href="https://packages.debian.org/unstable/hamradio-packetmodes"><code>hamradio-packetmodes</code></a></td>
		<td>Esta tarefa contém pacotes úteis para usar AX.25, incluindo
			IPv4 sobre AX.25 e APRS.</td>
		<td><a href="https://blends.debian.org/hamradio/tasks/packetmodes">Link</a></td>
	</tr>
	<tr>
		<td>Rig Control</td>
		<td><a href="https://packages.debian.org/unstable/hamradio-rigcontrol"><code>hamradio-rigcontrol</code></a></td>
		<td>Esta tarefa contém pacotes úteis para controle de
			 equipamento e programação.</td>
		<td><a href="https://blends.debian.org/hamradio/tasks/rigcontrol">Link</a></td>
	</tr>
	<tr>
		<td>Satellite operation</td>
		<td><a href="https://packages.debian.org/unstable/hamradio-satellite"><code>hamradio-satellite</code></a></td>
		<td>Esta tarefa contém pacotes úteis para operação amadora de
                         satélites.</td>
		<td><a href="https://blends.debian.org/hamradio/tasks/satellite">Link</a></td>
	</tr>
	<tr>
		<td>Software-Defined Radio</td>
		<td><a href="https://packages.debian.org/unstable/hamradio-sdr"><code>hamradio-sdr</code></a></td>
		<td>Esta tarefa contém pacotes úteis para trabalhar com
                         rádio definido por software.</td>
		<td><a href="https://blends.debian.org/hamradio/tasks/sdr">Link</a></td>
	</tr>
	<tr>
		<td>Tools</td>
		<td><a href="https://packages.debian.org/unstable/hamradio-tools"><code>hamradio-tools</code></a></td>
		<td>Esta tarefa contém pacotes úteis para ferramentas
                         relacionadas a radioamadorismo.</td>
		<td><a href="https://blends.debian.org/hamradio/tasks/tools">Link</a></td>
	</tr>
	<tr>
		<td>Training</td>
		<td><a href="https://packages.debian.org/unstable/hamradio-training"><code>hamradio-training</code></a></td>
		<td>Esta tarefa contém pacotes úteis quando em treinamento para
                         provas de radioamadorismo.</td>
		<td><a href="https://blends.debian.org/hamradio/tasks/training">Link</a></td>
	</tr>
</table>

<p><i>Nota: os metapacotes foram só recentemente introduzidos nos repositórios.
Eles estarão disponíveis na versão Debian stretch (testing) e não
estarão disponíveis na versão Debian jessie (stable).</i></p>

<p>Para instalar qualquer um dos metapacotes de tarefa, use sua ferramenta
favorita de gerenciamento de pacotes da mesma forma que você faria com
qualquer outro pacote Debian. Para o <code>apt-get</code>:</p>

<pre>apt-get install hamradio-&lt;task&gt;</pre>

<p>Se quiser instalar o blend inteiro:</p>

<pre>apt-get install hamradio-antenna hamradio-datamodes hamradio-digitalvoice hamradio-logging hamradio-morse hamradio-nonamateur hamradio-packetmodes hamradio-rigcontrol hamradio-satellite hamradio-sdr hamradio-tasks hamradio-tools hamradio-training</pre>
