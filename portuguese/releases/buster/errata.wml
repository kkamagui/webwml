#use wml::debian::template title="Debian 10 -- Errata" BARETITLE=true
#use wml::debian::toc
#use wml::debian::translation-check translation="5c427e44dc4a1503d5262b5edfba60b490ea0ab1"

#include "$(ENGLISHDIR)/releases/info"

<toc-display/>


# <toc-add-entry name="known_probs">Problemas conhecidos</toc-add-entry>
<toc-add-entry name="security">Problemas de segurança</toc-add-entry>

<p>A equipe de segurança do Debian emite atualizações para pacotes na versão
estável (stable), nos quais eles(as) identificaram problemas relacionados à
segurança. Por favor consulte as <a href="$(HOME)/security/">páginas de segurança</a>
para obter informações sobre quaisquer problemas de segurança identificados no
<q>buster</q>.</p>

<p>Se você usa o APT, adicione a seguinte linha ao <tt>/etc/apt/sources.list</tt>
para poder acessar as atualizações de segurança mais recentes:</p>

<pre>
  deb http://security.debian.org/ buster/updates main contrib non-free
</pre>

<p>Depois disso, execute <kbd>apt update</kbd> seguido por
<kbd>apt upgrade</kbd>.</p>


<toc-add-entry name="pointrelease">Lançamentos pontuais</toc-add-entry>

<p>Às vezes, no caso de vários problemas críticos ou atualizações de segurança,
a versão lançada já é atualizada. Geralmente eles são indicados como
lançamentos pontuais.</p>

<ul>
  <li>A primeira versão pontual, 10.1, foi lançada em
      <a href="$(HOME)/News/2019/20190907">7 de setembro de 2019</a>.</li>
  <li>A segunda versão pontual, 10.2, foi lançada em
      <a href="$(HOME)/News/2019/20191116">16 de novembro de 2019</a>.</li>
 <li>A terceira versão pontual, 10.3, foi lançada em
      <a href="$(HOME)/News/2020/20200208">8 de fevereiro de 2020</a>.</li>
 <li>A quarta versão pontual, 10.4, foi lançada em
      <a href="$(HOME)/News/2020/20200509">9 de maio de 2020</a>.</li>
 <li>A quinta versão pontual, 10.5, foi lançada em
      <a href="$(HOME)/News/2020/20200801">1 de agosto de 2020</a>.</li>
 <li>A sexta versão pontual, 10.6, foi lançada em
      <a href="$(HOME)/News/2020/20200926">26 de setembro de 2020</a>.</li>
 <li>A sétima versão pontual, 10.7, foi lançada em
      <a href="$(HOME)/News/2020/20201205">5 de dezembro de 2020</a>.</li>
</ul>

<ifeq <current_release_buster> 10.0 "

<p>Ainda não há lançamentos pontuais para o Debian 10</p>" "

<p>Consulte o <a
href="http://http.us.debian.org/debian/dists/buster/ChangeLog">\
ChangeLog</a>
para obter detalhes sobre alterações entre 10 e <current_release_buster/>.</p>"/>


<p>As correções na versão estável (stable) lançada geralmente passam por um
longo período de teste antes de serem aceitas no repositório.
No entanto, essas correções estão disponíveis no repositório
<a href="http://ftp.debian.org/debian/dists/buster-proposed-updates/">\
dists/buster-proposed-updates</a> de qualquer espelho do repositório Debian.</p>

<p>Se você usar o APT para atualizar seus pacotes, poderá instalar as
atualizações propostas adicionando a seguinte linha ao
<tt>/etc/apt/sources.list</tt>:</p>

<pre>
  \# Atualizações propostas para a versão 10
  deb http://deb.debian.org/debian buster-proposed-updates main contrib non-free
</pre>

<p>Depois disso, execute <kbd>apt update</kbd> seguido por
<kbd>apt upgrade</kbd>.</p>


<toc-add-entry name="installer">Sistema de instalação</toc-add-entry>

<p>
Para obter informações sobre erratas e atualizações para o sistema de instalação,
consulte a página <a href="debian-installer/">informações de instalação</a> .
</p>
