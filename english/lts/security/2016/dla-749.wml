<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-5385">CVE-2016-5385</a>

      <p>PHP through 7.0.8 does not attempt to address RFC 3875 section 4.1.18
      namespace conflicts and therefore does not protect applications from
      the presence of untrusted client data in the HTTP_PROXY environment
      variable, which might allow remote attackers to redirect an application's
      outbound HTTP traffic to an arbitrary proxy server via a crafted Proxy
      header in an HTTP request, as demonstrated by (1) an application that
      makes a getenv('HTTP_PROXY') call or (2) a CGI configuration of PHP,
      aka an <q>httpoxy</q> issue.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-7124">CVE-2016-7124</a>

      <p>ext/standard/var_unserializer.c in PHP before 5.6.25 and 7.x before 7.0.10
      mishandles certain invalid objects, which allows remote attackers to cause
      a denial of service or possibly have unspecified other impact via crafted
      serialized data that leads to a (1) __destruct call or (2) magic method
      call.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-7128">CVE-2016-7128</a>

      <p>The exif_process_IFD_in_TIFF function in ext/exif/exif.c in PHP before
      5.6.25 and 7.x before 7.0.10 mishandles the case of a thumbnail offset
      that exceeds the file size, which allows remote attackers to obtain
      sensitive information from process memory via a crafted TIFF image.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-7129">CVE-2016-7129</a>

      <p>The php_wddx_process_data function in ext/wddx/wddx.c in PHP before
      5.6.25 and 7.x before 7.0.10 allows remote attackers to cause a denial
      of service (segmentation fault) or possibly have unspecified other
      impact via an invalid ISO 8601 time value, as demonstrated by
      a wddx_deserialize call that mishandles a dateTime element in
      a wddxPacket XML document.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-7130">CVE-2016-7130</a>

      <p>The php_wddx_pop_element function in ext/wddx/wddx.c in PHP before
      5.6.25 and 7.x before 7.0.10 allows remote attackers to cause a
      denial of service (NULL pointer dereference and application crash)
      or possibly have unspecified other impact via an invalid base64
      binary value, as demonstrated by a wddx_deserialize call that
      mishandles a binary element in a wddxPacket XML document.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-7131">CVE-2016-7131</a>

      <p>ext/wddx/wddx.c in PHP before 5.6.25 and 7.x before 7.0.10 allows
      remote attackers to cause a denial of service (NULL pointer
      dereference and application crash) or possibly have unspecified
      other impact via a malformed wddxPacket XML document that is
      mishandled in a wddx_deserialize call, as demonstrated by a tag
      that lacks a < (less than) character.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-7132">CVE-2016-7132</a>

      <p>ext/wddx/wddx.c in PHP before 5.6.25 and 7.x before 7.0.10 allows
      remote attackers to cause a denial of service (NULL pointer
      dereference and application crash) or possibly have unspecified
      other impact via an invalid wddxPacket XML document that is
      mishandled in a wddx_deserialize call, as demonstrated by
      a stray element inside a boolean element, leading to incorrect
      pop processing.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-7411">CVE-2016-7411</a>

      <p>ext/standard/var_unserializer.re in PHP before 5.6.26 mishandles
      object-deserialization failures, which allows remote attackers
      to cause a denial of service (memory corruption) or possibly
      have unspecified other impact via an unserialize call that
      references a partially constructed object.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-7412">CVE-2016-7412</a>

      <p>ext/mysqlnd/mysqlnd_wireprotocol.c in PHP before 5.6.26 and 7.x
      before 7.0.11 does not verify that a BIT field has the
      UNSIGNED_FLAG flag, which allows remote MySQL servers to cause
      a denial of service (heap-based buffer overflow) or possibly
      have unspecified other impact via crafted field metadata.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-7413">CVE-2016-7413</a>

      <p>Use-after-free vulnerability in the wddx_stack_destroy function in
      ext/wddx/wddx.c in PHP before 5.6.26 and 7.x before 7.0.11 allows
      remote attackers to cause a denial of service or possibly have
      unspecified other impact via a wddxPacket XML document that lacks
      an end-tag for a recordset field element, leading to mishandling
      in a wddx_deserialize call.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-7414">CVE-2016-7414</a>

      <p>The ZIP signature-verification feature in PHP before 5.6.26 and 7.x
      before 7.0.11 does not ensure that the uncompressed_filesize field
      is large enough, which allows remote attackers to cause a denial of
      service (out-of-bounds memory access) or possibly have unspecified
      other impact via a crafted PHAR archive, related to ext/phar/util.c
      and ext/phar/zip.c.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-7416">CVE-2016-7416</a>

      <p>ext/intl/msgformat/msgformat_format.c in PHP before 5.6.26 and 7.x
      before 7.0.11 does not properly restrict the locale length provided
      to the Locale class in the ICU library, which allows remote attackers
      to cause a denial of service (application crash) or possibly have
      unspecified other impact via a MessageFormatter::formatMessage call
      with a long first argument.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-7417">CVE-2016-7417</a>

      <p>ext/spl/spl_array.c in PHP before 5.6.26 and 7.x before 7.0.11
      proceeds with SplArray unserialization without validating a
      return value and data type, which allows remote attackers to
      cause a denial of service or possibly have unspecified other
      impact via crafted serialized data.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-7418">CVE-2016-7418</a>

      <p>The php_wddx_push_element function in ext/wddx/wddx.c in PHP before
      5.6.26 and 7.x before 7.0.11 allows remote attackers to cause a
      denial of service (invalid pointer access and out-of-bounds read)
      or possibly have unspecified other impact via an incorrect boolean
      element in a wddxPacket XML document, leading to mishandling in
      a wddx_deserialize call.</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
5.4.45-0+deb7u6.</p>

<p>We recommend that you upgrade your php5 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-749.data"
# $Id: $
