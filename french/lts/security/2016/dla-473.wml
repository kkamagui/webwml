#use wml::debian::translation-check translation="ce41d997301872adfc27a79ea546429856226b67" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Une vulnérabilité a été découverte dans la manière dont hostapd et
wpa_supplicant écrivent la mise à jour du fichier de configuration pour
les paramètres de la phrase secrète WPA ou WPA2. Si le paramètre a été mis
à jour pour inclure des caractères de contrôle soit par une opération WPS
(<a href="https://security-tracker.debian.org/tracker/CVE-2016-4476">CVE-2016-4476</a>) 
soit par une modification locale de configuration avec l'interface de
contrôle de wpa_supplicant
(<a href="https://security-tracker.debian.org/tracker/CVE-2016-4477">CVE-2016-4477</a>),
le fichier de configuration résultant peut empêcher le démarrage d'hostpad
et de wpa_supplicant quand le fichier mis à jour est utilisé. De plus pour
wpa_supplicant, il peut être possible de charger un fichier local de la
bibliothèque et d'exécuter le code à partir de là avec les mêmes privilèges
que ceux avec lesquels le processus wpa_supplicant est exécuté.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-4476">CVE-2016-4476</a>

<p>hostapd, version 0.6.7 à 2.5, et wpa_supplicant, versions 0.6.7 à 2.5,
ne rejettent par les caractères \n et \r dans les paramètres de la phrase
secrète. Cela permet à des attaquants distants de provoquer un déni de
service (indisponibilité du démon) à l'aide d'une opération WPS
contrefaite.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-4477">CVE-2016-4477</a>

<p>wpa_supplicant, versions 0.4.0 à 2.5 ne rejette pas les caractères \n et
 \r dans les paramètres de la phrase secrète. Cela permet à des attaquants
locaux de déclencher le chargement d'une bibliothèque arbitraire et en
conséquence d'obtenir des privilèges ou provoquer un déni de service
(indisponibilité du démon) à l'aide d'une commande (1) SET, (2) SET_CRED,
ou (3) SET_NETWORK contrefaite.</p></li>

</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans la
version 1.0-3+deb7u4.</p>

<p>Nous vous recommandons de mettre à jour vos paquets wpa.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-473.data"
# $Id: $
