msgid ""
msgstr ""
"Project-Id-Version: Debian webwml organization\n"
"POT-Creation-Date: \n"
"PO-Revision-Date: 2016-08-28 22:23+0700\n"
"Last-Translator: Izharul Haq <atoz.chevara@yahoo.com>\n"
"Language-Team: L10N Debian Indonesian <debian-l10n-indonesian@lists.debian."
"org>\n"
"Language: id\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.6.10\n"

#: ../../english/CD/vendors/vendors.CD.def:14
msgid "Vendor"
msgstr "Pabrikan"

#: ../../english/CD/vendors/vendors.CD.def:15
msgid "Allows Contributions"
msgstr "Memungkinkan Kontribusi"

#: ../../english/CD/vendors/vendors.CD.def:16
msgid "CD/DVD/BD/USB"
msgstr "CD/DVD/BD/USB"

#: ../../english/CD/vendors/vendors.CD.def:17
msgid "Architectures"
msgstr "Arsitektur"

#: ../../english/CD/vendors/vendors.CD.def:18
msgid "Ship International"
msgstr "Pengiriman Internasional"

#: ../../english/CD/vendors/vendors.CD.def:19
msgid "Contact"
msgstr "Kontak"

#. ###################
#. Vendor home page link, Debian page link
#: ../../english/CD/vendors/vendors.CD.def:44
msgid "Vendor Home"
msgstr "Halaman web Pabrikan"

#: ../../english/CD/vendors/vendors.CD.def:70
msgid "page"
msgstr "halaman"

#: ../../english/CD/vendors/vendors.CD.def:71
msgid "email"
msgstr "email"

#: ../../english/CD/vendors/vendors.CD.def:114
msgid "within Europe"
msgstr "di Eropa"

#: ../../english/CD/vendors/vendors.CD.def:118
msgid "To some areas"
msgstr "Untuk beberapa daerah"

#: ../../english/CD/vendors/vendors.CD.def:122
msgid "source"
msgstr "sumber"

#: ../../english/CD/vendors/vendors.CD.def:126
msgid "and"
msgstr "dan"
