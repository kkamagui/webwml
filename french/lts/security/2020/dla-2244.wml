#use wml::debian::translation-check translation="c7b1f00773c8d435d64a0af9c987fb337f62109c" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Il a été découvert qu’il existait un problème d’échappement dans
<tt>libphp-phpmailer</tt>, une classe d’utilitaires de création de courriel pour
le langage de programmation PHP.</p>

<p>Les en-têtes <tt>Content-Type</tt> et <tt>Content-Disposition</tt> pourraient
 avoir permis des attachements de fichier qui contournent les filtres
de correspondance à des extensions de nom de fichier pour les attachements. Pour
plus d’informations, veuillez consulter
l’<a href="https://github.com/PHPMailer/PHPMailer/security/advisories/GHSA-f7hx-fqxw-rvvj">annonce
de l’amont</a>.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-13625">CVE-2020-13625</a>

<p>PHPMailer avant 6.1.6 contenait un bogue d’échappement de sortie quand le
nom d’un attachement de fichier contenait un caractère guillemet. Cela pouvait
aboutir à ce que le type de fichier soit mal interprété par le receveur ou par
n’importe quel relais de courriel traitant le message.</p></li>
</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 5.2.9+dfsg-2+deb8u6.</p>

<p>Nous vous recommandons de mettre à jour vos paquets libphp-phpmailer.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2244.data"
# $Id: $
