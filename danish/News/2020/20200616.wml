#use wml::debian::translation-check translation="a3ce03f0ff8939281b7a4da3bb955c91e6857f6f"
<define-tag pagetitle>Ampere donerer Arm64-serverhardware til Debian for at styke Arm-økosystemet</define-tag>
<define-tag release_date>2020-06-16</define-tag>
#use wml::debian::news

# Status: [content-frozen]

<p><a href="https://amperecomputing.com/">Ampere®</a> er gået i partnerskab med 
Debian om at udbygge dets hardwareinfrastruktur gennem en donation af tre af 
Amperes højtydende Arm64-servere.  Lenovo ThinkSystem HR330A-serverene 
indeholder Amperes eMAG-CPU med en 64 bit Arm®v8-processor, specifikt designet 
til cloudservere, udstyret med 256GB RAM, to 960GB-SSD'er og en 25GbE-NIC med to 
porte.</p>

<p>De donerede servere er placeret hos University of British Columbia, vores 
hostingpartner i Vancouver, Canada.  Debian System Administrators (DSA) har 
opsat serverne til at afvikle arm64/armhf/armel-builddæmoner, som erstatning for 
builddæmoner med mindre ydedygtige bundkort på udviklerniveau.  På virtuelle 
maskiner med halvt så mange vCPU'er, har resultatet været at den tid det tager, 
at opbygge Arm*-pakker, er blevet halveret med Amperes eMAG-system.  En anden 
fordel ved denne generøse gave, er at det er blevet muligt for DSA at migrere 
nogle generelle Debian-tjenester, som kører i vores nuværende infrastruktur, og 
vil desuden forsyne andre Debian-hold (eksempelvis Continuous Integration, 
Quality Assurance, m.fl.) med virtuelle maskiner, hvis de har brug for adgang 
til Arm64-arkitektur.</p>

<p><q>Vores partnerskab med Debian, støtter vores udviklingsstrategi, som har 
til formål at få flere open source-fællesskaber til at benytte Ampere-servere, 
for yderligere at udbygge Arm64-økosystemet og gøre det muligt at skabe nye 
applikationer,</q> sagde Mauri Whalen, Amperes vicedirektør for 
softwareudvikling.  <q>Debian er et velfungerede og respekteret fællesskab, og 
vi er stolte over at samarbejde med dem.</q></p>

<p><q>Debian System Administrators er taknemmelige over at Ampere har doneret 
Arm64-servere med en høj pålidelighed og ydeevne.  At have servere med 
integreret standardmanagementgrænseflader, så som Intelligent Platform 
Management Interface (IPMI), og med Lenovos hardwaregarantier og 
supportorganisation bag dem, er det præcis hvad DSA har ønsket sig vedrørende 
Arm64-arkitekturen.  Disse servere er meget ydedygtige og særdeles veludrustede: 
vi forventer at anvende til generelle tjenester, foruden anvendelse som 
Arm64-builddæmoner.  Jeg tror, at de vil vise sig at være meget tiltrækkende for 
cloudoperatører. og jeg er begejstret for at Ampere Computing er gået i 
partnerskab med Debian.</q> - Luca Filipozzi, Debian System Administrator.</p>

<p>Det er kun på grund af donation af frivilliges arbejdskraft, udstyr og 
tjensteydelser, samt økonomisk støtte, at Debian er i stand til at leve op til 
sit mål om at skabe et frit styresystem.  Vi sætter stor på pris Amperes 
generøsitet.</p>


<h2>Om Ampere Computing</h2>

<p>Ampere designer fremtiden for hyperscale-cloud- og -edgecomputing med verdens 
første processor, som er født til cloud'en.  Med sin opbygning beregnet til 
cloud'en og en moderne 64 bit serverbaseret Arm-arkitektur, giver Ampere sine 
kunder frihed til at vælge at accelere leverancen af alle applikationer, som gør 
brug af cloudcomputing.  Med toneangivende cloudydeevne i industrien, effektivt 
strømforbrug og skalérbarhed, er Amperes processorer skræddersyet til fortsat 
vækst i cloud- og edgecomputing.</p>


<h2>Om Debian</h2>

<p>Debian-projektet blev grundlagt i 1993 af Ian Murdock, som et helt frit
fællesskabsprojekt.  Siden den gang, er projektet vokset til at være et af de
største og mest indflydelsesrige open source-projekter.  Tusindvis af
frivillige fra hele verden samarbejder om at fremstille og vedligeholde
Debian-software.  Med oversættelser til 70 sprog, og med understøttelse af et
enormt antal computertyper, kalder Debian sig det <q>universelle
styresystem</q>.</p>


<h2>Kontaktoplysninger</h2>

<p>For flere oplysninger, besøg Debians websider på 
<a href="$(HOME)/">https://www.debian.org/</a> eller send e-mail på engelsk til
&lt;press@debian.org&gt;.</p>
