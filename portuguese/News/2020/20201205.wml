#use wml::debian::translation-check translation="5c427e44dc4a1503d5262b5edfba60b490ea0ab1"
<define-tag pagetitle>Atualização Debian 10: 10.7 lançado</define-tag>
<define-tag release_date>2020-12-05</define-tag>
#use wml::debian::news

<define-tag release>10</define-tag>
<define-tag codename>buster</define-tag>
<define-tag revision>10.7</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>O projeto Debian está feliz em anunciar a sétima atualização de sua
versão estável (stable) do Debian <release> (codinome <q><codename></q>).
Esta versão pontual adiciona principalmente correções para problemas de
segurança, além de pequenos ajustes para problemas mais sérios. Avisos de
segurança já foram publicados em separado e são referenciados quando
necessário.</p>

<p>Por favor note que a versão pontual não constitui uma nova versão do Debian
<release>, mas apenas atualiza alguns dos pacotes já incluídos. Não há
necessidade de jogar fora as antigas mídias do <q><codename></q>. Após a
instalação, os pacotes podem ser atualizados para as versões atuais usando um
espelho atualizado do Debian.</p>

<p>Aquelas pessoas que frequentemente instalam atualizações a partir do security.debian.org
não terão que atualizar muitos pacotes, e a maioria de tais atualizações estão
incluídas na versão pontual.</p>

<p>Novas imagens de instalação logo estarão disponíveis nos locais
habituais.</p>

<p>A atualização de uma instalação existente para esta revisão pode ser feita
apontando o sistema de gerenciamento de pacotes para um dos muitos espelhos
HTTP do Debian. Uma lista abrangente de espelhos está disponível em:</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>




<h2>Correções diversas de bugs</h2>

<p>Esta atualização da versão estável (stable) adiciona algumas correções
importantes para os seguintes pacotes:</p>

<table border=0>
<tr><th>Pacote</th>               <th>Justificativa</th></tr>
<correction base-files "Atualização para a versão pontual">
<correction choose-mirror "Atualização da lista de espelhos">
<correction cups "Correção de 'printer-alert' invalid free">
<correction dav4tbsync "Nova versão do aplicativo, compatível com versões mais novas do Thunderbird">
<correction debian-installer "Usa o Linux kernel ABI 4.19.0-13; adiciona grub2 para Built-Using">
<correction debian-installer-netboot-images "Reconstruído contra proposed-updates">
<correction distro-info-data "Adiciona Ubuntu 21.04, Hirsute Hippo">
<correction dpdk "Nova versão estável do aplicativo; corrige problema de execução remota de código [CVE-2020-14374], problemas com TOCTOU [CVE-2020-14375], buffer overflow [CVE-2020-14376], buffer over read [CVE-2020-14377] e integer underflow [CVE-2020-14377]; corrige construção armhf com NEON">
<correction eas4tbsync "Nova versão do aplicativo, compatível com versões mais novas do Thunderbird">
<correction edk2 "Corrige integer overflow em DxeImageVerificationHandler [CVE-2019-14562]">
<correction efivar "Adiciona suporte para dispositivos nvme-fabrics e nvme-subsystem; corrige variável não inicializada em parse_acpi_root, evitando potencial segfault">
<correction enigmail "Introduz assistente de migração para suporte embutido do GPG no Thunderbird">
<correction espeak "Corrige o uso do espeak com mbrola-fr4 quando mbrola-fr1 não está instalado">
<correction fastd "Corrige vazamento de memória ao receber muitos pacotes inválidos [CVE-2020-27638]">
<correction fish "Assegura que opções TTY sejam restauradas na saída">
<correction freecol "Corrige a vulnerabilidade XML External Entity [CVE-2018-1000825]">
<correction gajim-omemo "Usa 12-byte IV, para melhor compatibilidade com clientes iOS">
<correction glances "Escuta somente no localhost por padrão">
<correction iptables-persistent "Não força carregamento (force-load) de módulos do kernel; melhora a regra flushing logic">
<correction lacme "Usa cadeia de certificado do(a) autor(a) original em vez de um codificado, facilitando o suporte a novos certificados intermediários e root do Let's Encrypt">
<correction libdatetime-timezone-perl "Atualiza dados incluídos para tzdata 2020d">
<correction libimobiledevice "Adiciona suporte parcial para iOS 14">
<correction libjpeg-turbo "Corrige recusa de serviço [CVE-2018-1152], buffer over read [CVE-2018-14498], potencial execução de código remoto [CVE-2019-2201], buffer over read [CVE-2020-13790]">
<correction libxml2 "Corrige recusa de serviço [CVE-2017-18258], dereferência de ponteiro NULL [CVE-2018-14404], loop infinito [CVE-2018-14567], vazamento de memória [CVE-2019-19956 CVE-2019-20388], loop infinito [CVE-2020-7595]">
<correction linux "Nova versão estável do aplicativo">
<correction linux-latest "Atualizado para kernel ABI 4.19.0-13">
<correction linux-signed-amd64 "Nova versão estável do aplicativo">
<correction linux-signed-arm64 "Nova versão estável do aplicativo">
<correction linux-signed-i386 "Nova versão estável do aplicativo">
<correction lmod "Altera arquitetura para <q>any</q> - requerido devido a LUA_PATH e LUA_CPATH serem determinados durante a construção">
<correction mariadb-10.3 "Nova versão estável do aplicativo; correções de segurança [CVE-2020-14765 CVE-2020-14776 CVE-2020-14789 CVE-2020-14812 CVE-2020-28912]">
<correction mutt "Assegura que conexão IMAP é fechada após erro de conexão [CVE-2020-28896]">
<correction neomutt "Assegura que conexão IMAP é fechada após erro de conexão [CVE-2020-28896]">
<correction node-object-path "Corrige poluição de protótipo em set() [CVE-2020-15256]">
<correction node-pathval "Corrige poluição de protótipo [CVE-2020-7751]">
<correction okular "Corrige execução de código via link de ação [CVE-2020-9359]">
<correction openjdk-11 "Nova versão do aplicativo; corrige quebra de JVM">
<correction partman-auto "Aumento de tamanho de /boot na maior parte das receitas para entre 512 e 768M, para melhor lidar com alterações do ABI do kernel e initramfses maiores; limita tamanho da RAM como usado para cálculos de partição swap, resolvendo problemas em máquinas com mais RAM que espaço de disco">
<correction pcaudiolib "Limita latência de cancelamento para 10ms">
<correction plinth "Apache: Desabilita mod_status [CVE-2020-25073]">
<correction puma "Corrige problemas de injeção HTTP e contrabando de solicitações HTTP (HTTP smuggling) [CVE-2020-5247 CVE-2020-5249 CVE-2020-11076 CVE-2020-11077]">
<correction ros-ros-comm "Corrige integer overflow [CVE-2020-16124]">
<correction ruby2.5 "Corrige potencial vulnerabilidade de HTTP request smuggling em WEBrick [CVE-2020-25613]">
<correction sleuthkit "Corrige stack buffer overflow em yaffsfs_istat [CVE-2020-10232]">
<correction sqlite3 "Corrige divisão por zero [CVE-2019-16168], dereferência de ponteiro NULL [CVE-2019-19923], erro ao lidar com nome do caminho de NULL durante uma atualização de um arquivo ZIP [CVE-2019-19925], erro ao lidar com NULs embutidos em nomes de arquivos [CVE-2019-19959], possível quebra com reversão de empilhamento [CVE-2019-20218], integer overflow [CVE-2020-13434], segmentation fault [CVE-2020-13435], problema com use-after-free [CVE-2020-13630], dereferência de ponteiro NULL [CVE-2020-13632], heap overflow [CVE-2020-15358]">
<correction systemd "Lista básica/limitante: processa/imprime capacidades numéricas; reconhece novas capacidades do kernel Linux 5.8; networkd: não gera MAC para dispositivo bridge">
<correction tbsync "Nova versão do aplicativo, compatível com versõs mais novas do Thunderbird">
<correction tcpdump "Correção de problema de entrada não confiável na impressora PPP [CVE-2020-8037]">
<correction tigervnc "Armazena apropriadamente exceções de certificado no visualizador VNC nativo e Java [CVE-2020-26117]">
<correction tor "Nova versão estável do aplicativo; múltiplas correções de segurança, usabilidade, portabilidade e confiabilidade">
<correction transmission "Corrige vazamento de memória">
<correction tzdata "Nova versão do aplicativo">
<correction ublock-origin "Nova versão do aplicativo; divide pacotes de plugin para pacotes específicos a navegadores">
<correction vips "Corrige o uso de variável não inicializada [CVE-2020-20739]">
</table>


<h2>Atualizações de segurança</h2>


<p>Esta revisão adiciona as seguintes atualizações de segurança para a versão
estável (stable).
O time de segurança já lançou um aviso para cada uma dessas atualizações:</p>

<table border=0>
<tr><th>ID do aviso</th>  <th>Pacote</th></tr>
<dsa 2020 4766 rails>
<dsa 2020 4767 mediawiki>
<dsa 2020 4768 firefox-esr>
<dsa 2020 4769 xen>
<dsa 2020 4770 thunderbird>
<dsa 2020 4771 spice>
<dsa 2020 4772 httpcomponents-client>
<dsa 2020 4773 yaws>
<dsa 2020 4774 linux-latest>
<dsa 2020 4774 linux-signed-amd64>
<dsa 2020 4774 linux-signed-arm64>
<dsa 2020 4774 linux-signed-i386>
<dsa 2020 4774 linux>
<dsa 2020 4775 python-flask-cors>
<dsa 2020 4776 mariadb-10.3>
<dsa 2020 4777 freetype>
<dsa 2020 4778 firefox-esr>
<dsa 2020 4779 openjdk-11>
<dsa 2020 4780 thunderbird>
<dsa 2020 4781 blueman>
<dsa 2020 4782 openldap>
<dsa 2020 4783 sddm>
<dsa 2020 4784 wordpress>
<dsa 2020 4785 raptor2>
<dsa 2020 4786 libexif>
<dsa 2020 4787 moin>
<dsa 2020 4788 firefox-esr>
<dsa 2020 4789 codemirror-js>
<dsa 2020 4790 thunderbird>
<dsa 2020 4791 pacemaker>
<dsa 2020 4792 openldap>
<dsa 2020 4793 firefox-esr>
<dsa 2020 4794 mupdf>
<dsa 2020 4795 krb5>
<dsa 2020 4796 thunderbird>
<dsa 2020 4798 spip>
<dsa 2020 4799 x11vnc>
<dsa 2020 4800 libproxy>
</table>


<h2>Pacotes removidos</h2>

<p>Os seguintes pacotes foram removidos devido a circunstâncias além do nosso
controle:</p>

<table border=0>
<tr><th>Pacote</th>               <th>Justificativa</th></tr>
<correction freshplayerplugin "Não suportado por navegadores; descontinuado pelo(a) desenvolvedor(a)">
<correction nostalgy "Incompatível com versões mais novas do Thunderbird">
<correction sieve-extension "Incompatível com versões mais novas do Thunderbird">

</table>

<h2>Instalador do Debian</h2>
<p>O instalador foi atualizado para incluir as correções incorporadas
na versão estável (stable) pela versão pontual.</p>

<h2>URLs</h2>

<p>As listas completas dos pacotes que foram alterados por esta revisão:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>A atual versão estável (stable):</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/stable/">
</div>

<p>Atualizações propostas para a versão estável (stable):</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/proposed-updates">
</div>

<p>Informações da versão estável (stable) (notas de lançamento, errata,
etc):</p>

<div class="center">
  <a
  href="$(HOME)/releases/stable/">https://www.debian.org/releases/stable/</a>
</div>

<p>Anúncios de segurança e informações:</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>

<h2>Sobre o Debian</h2>

<p>O projeto Debian é uma associação de desenvolvedores(as) de Software Livre
que dedicam seu tempo e esforço como voluntários(as) para produzir o sistema
operacional completamente livre Debian.</p>

<h2>Contact Information</h2>

<h2>Informações de contato</h2>

<p>Para mais informações, por favor visite as páginas web do Debian em
<a href="$(HOME)/">https://www.debian.org/</a>, envie um e-mail (em inglês) para
&lt;press@debian.org&gt;, ou entre em contato (em inglês) com o time de
lançamento da estável (stable) em &lt;debian-release@lists.debian.org&gt;.</p>
