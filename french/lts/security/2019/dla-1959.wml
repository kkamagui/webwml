#use wml::debian::translation-check translation="1c29a3c93778e051a9450dd72e5a9bf1b622c5bd" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Les périphériques tactiles multipoints n’étaient pas désactivés par
l’utilitaire de verrouillage d’écran xtrlock.</p>

<p>Xtrlock ne bloquait pas les évènements d’écran multipoints, aussi un
attaquant pouvait encore entrer et ainsi contrôler divers programmes tel
Chromium, etc., à l’aide d’évènements « multipoints » incluant le défilement
d’écran, « pincer et agrandir » ou même être capable de fournir des clics de
souris normaux en pressant le pavé tactile et cliquant avec un autre doigt.
</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-10894">CVE-2016-10894</a>

<p>Xtrlock jusqu’à 2.10 ne bloquait pas ces évènements. En conséquence, un
attaquant d’un écran verrouillé pouvait envoyer une entrée (et par conséquent
contrôler) divers programmes tel Chromium à l’aide d’évènements tels que le
défilement d’écran, « pincer et agrandir » ou même des clics de souris normaux
(en pressant le pavé tactile et cliquant avec un autre doigt).</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 2.6+deb8u1. Cependant, cela ne corrige pas la situation où un
attaquant branche un périphérique tactile multipoint <em>après</em> le
verrouillage d’écran (<a href="https://bugs.debian.org/830726#115">plus d’info</a>).</p>

<p>Nous recommandons de mettre à niveau vos paquets xtrlock en attente d’un
correctif plus important.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1959.data"
# $Id: $
