<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Debian Bug     : 870020 870019 876105 869727 886281 873059 870504
                 870530 870107 872609 875338 875339 875341 873871
                 873131 875352 878506 875503 875502 876105 876099
                 878546 878545 877354 877355 878524 878547 878548
                 878555 878554 878548 878555 878554 878579 885942
                 886584 928206 941670 931447 932079</p>

<p>Several security vulnerabilities were found in Imagemagick. Various
memory handling problems and cases of missing or incomplete input
sanitizing may result in denial of service, memory or CPU exhaustion,
information disclosure or potentially the execution of arbitrary code
when a malformed image file is processed.</p>

<p>For Debian 9 stretch, these problems have been fixed in version
8:6.9.7.4+dfsg-11+deb9u10.</p>

<p>We recommend that you upgrade your imagemagick packages.</p>

<p>For the detailed security status of imagemagick please refer to
its security tracker page at:
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/imagemagick">https://security-tracker.debian.org/tracker/imagemagick</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2366.data"
# $Id: $
