msgid ""
msgstr ""
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../english/template/debian/stats_tags.wml:6
msgid "Debian web site translation statistics"
msgstr "Statistik over oversættelser på Debians websted"

#: ../../english/template/debian/stats_tags.wml:10
msgid "There are %d pages to translate."
msgstr "Der er %d sider at oversætte."

#: ../../english/template/debian/stats_tags.wml:14
msgid "There are %d bytes to translate."
msgstr "Der er %d bytes at oversætte."

#: ../../english/template/debian/stats_tags.wml:18
msgid "There are %d strings to translate."
msgstr "Der er %d strenge at oversætte."

#: ../../stattrans.pl:282 ../../stattrans.pl:498
msgid "Wrong translation version"
msgstr "Forkert oversættelsesversion"

#: ../../stattrans.pl:284
msgid "This translation is too out of date"
msgstr "Denne oversættelse er forældet"

#: ../../stattrans.pl:286
msgid "The original is newer than this translation"
msgstr "Originalen er nyere end denne oversættelse"

#: ../../stattrans.pl:290 ../../stattrans.pl:498
msgid "The original no longer exists"
msgstr "Originalen findes ikke længere"

#: ../../stattrans.pl:474
msgid "hit count N/A"
msgstr "antal forekomster ikke tilgængelig"

#: ../../stattrans.pl:474
msgid "hits"
msgstr "forekomster"

#: ../../stattrans.pl:492 ../../stattrans.pl:493
msgid "Click to fetch diffstat data"
msgstr "Klik for at hente diffstat-data"

#: ../../stattrans.pl:603 ../../stattrans.pl:743
msgid "Created with <transstatslink>"
msgstr "Oprettet med <transstatslink>"

#: ../../stattrans.pl:608
msgid "Translation summary for"
msgstr "Oversættelsesresume for"

#: ../../stattrans.pl:611 ../../stattrans.pl:767 ../../stattrans.pl:813
#: ../../stattrans.pl:856
msgid "Not translated"
msgstr "Ikke oversat"

#: ../../stattrans.pl:611 ../../stattrans.pl:766 ../../stattrans.pl:812
msgid "Outdated"
msgstr "Forældet"

#: ../../stattrans.pl:611
msgid "Translated"
msgstr "Oversat"

#: ../../stattrans.pl:611 ../../stattrans.pl:691 ../../stattrans.pl:765
#: ../../stattrans.pl:811 ../../stattrans.pl:854
msgid "Up to date"
msgstr "Ajourført"

#: ../../stattrans.pl:612 ../../stattrans.pl:613 ../../stattrans.pl:614
#: ../../stattrans.pl:615
msgid "files"
msgstr "filer"

#: ../../stattrans.pl:618 ../../stattrans.pl:619 ../../stattrans.pl:620
#: ../../stattrans.pl:621
msgid "bytes"
msgstr "bytes"

#: ../../stattrans.pl:628
msgid ""
"Note: the lists of pages are sorted by popularity. Hover over the page name "
"to see the number of hits."
msgstr ""
"Note: Listerne over sider er sorteret efter popularitet.  Svæv over "
"sidenavnet for at se antallet af forekomster."

#: ../../stattrans.pl:634
msgid "Outdated translations"
msgstr "Forældede oversættelser"

#: ../../stattrans.pl:636 ../../stattrans.pl:690
msgid "File"
msgstr "Fil"

#: ../../stattrans.pl:638
msgid "Diff"
msgstr "Diff"

#: ../../stattrans.pl:640
msgid "Comment"
msgstr "Kommentar"

#: ../../stattrans.pl:641
msgid "Diffstat"
msgstr "Diffstat"

#: ../../stattrans.pl:642
msgid "Git command line"
msgstr "Git-kommandolinje"

#: ../../stattrans.pl:644
msgid "Log"
msgstr "Log"

#: ../../stattrans.pl:645
msgid "Translation"
msgstr "Oversættelse"

#: ../../stattrans.pl:646
msgid "Maintainer"
msgstr "Vedligeholder"

#: ../../stattrans.pl:648
msgid "Status"
msgstr "Status"

#: ../../stattrans.pl:649
msgid "Translator"
msgstr "Oversætter"

#: ../../stattrans.pl:650
msgid "Date"
msgstr "Dato"

#: ../../stattrans.pl:657
msgid "General pages not translated"
msgstr "Generelle sider ikke oversat"

#: ../../stattrans.pl:658
msgid "Untranslated general pages"
msgstr "Uoversatte generelle sider"

#: ../../stattrans.pl:663
msgid "News items not translated"
msgstr "Nyheder ikke oversat"

#: ../../stattrans.pl:664
msgid "Untranslated news items"
msgstr "Uoversatte nyheder"

#: ../../stattrans.pl:669
msgid "Consultant/user pages not translated"
msgstr "Konsulent-/brugersider ikke oversat"

#: ../../stattrans.pl:670
msgid "Untranslated consultant/user pages"
msgstr "Uoversatte konsulent-/brugersider"

#: ../../stattrans.pl:675
msgid "International pages not translated"
msgstr "Internationale sider ikke oversat"

#: ../../stattrans.pl:676
msgid "Untranslated international pages"
msgstr "Uoversatte internationale sider"

#: ../../stattrans.pl:681
msgid "Translated pages (up-to-date)"
msgstr "Oversatte sider (ajourførte)"

#: ../../stattrans.pl:688 ../../stattrans.pl:838
msgid "Translated templates (PO files)"
msgstr "Oversatte skabeloner (PO-filer)"

#: ../../stattrans.pl:689 ../../stattrans.pl:841
msgid "PO Translation Statistics"
msgstr "Statistik over PO-oversættelser"

#: ../../stattrans.pl:692 ../../stattrans.pl:855
msgid "Fuzzy"
msgstr "Fuzzy"

#: ../../stattrans.pl:693
msgid "Untranslated"
msgstr "Uoversat"

#: ../../stattrans.pl:694
msgid "Total"
msgstr "Total"

#: ../../stattrans.pl:711
msgid "Total:"
msgstr "Total:"

#: ../../stattrans.pl:745
msgid "Translated web pages"
msgstr "Oversatte websider"

#: ../../stattrans.pl:748
msgid "Translation Statistics by Page Count"
msgstr "Oversættelsesstatistik efter antal sider"

#: ../../stattrans.pl:763 ../../stattrans.pl:809 ../../stattrans.pl:853
msgid "Language"
msgstr "Sprog"

#: ../../stattrans.pl:764 ../../stattrans.pl:810
msgid "Translations"
msgstr "Oversættelser"

#: ../../stattrans.pl:791
msgid "Translated web pages (by size)"
msgstr "Oversatte websider (efter størrelse)"

#: ../../stattrans.pl:794
msgid "Translation Statistics by Page Size"
msgstr "Oversættelsesstatistik efter sidestørrelse"

#~ msgid "Unified diff"
#~ msgstr "Samlet diff"

#~ msgid "Colored diff"
#~ msgstr "Farvelagt diff"

#~ msgid "Commit diff"
#~ msgstr "Commitdiff"

#~ msgid "Created with"
#~ msgstr "Oprettet med"

#~ msgid "Hit data from %s, gathered %s."
#~ msgstr "Forekomstdata fra %s, indsamlet %s."

#~ msgid "Origin"
#~ msgstr "Ophav"
