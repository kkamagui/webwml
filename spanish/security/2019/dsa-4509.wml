#use wml::debian::translation-check translation="b917e690cbacd447496fcc36bb3b22df5d6873b2"
<define-tag description>actualización de seguridad</define-tag>
<define-tag moreinfo>
<p>Se han encontrado varias vulnerabilidades en el servidor Apache HTTPD.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-9517">CVE-2019-9517</a>

    <p>Jonathan Looney informó de que un cliente malicioso podría llevar a cabo un
    ataque de denegación de servicio (usando todos los trabajadores h2) inundando una
    conexión con peticiones y, básicamente, no leyendo nunca respuestas en
    la conexión TCP.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-10081">CVE-2019-10081</a>

    <p>Craig Young informó de que los PUSH HTTP/2 podían dar lugar a sobreescritura
    de memoria en el área («pool») de peticiones adelantadas («pushing requests»), provocando caídas.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-10082">CVE-2019-10082</a>

    <p>Craig Young informó de que se podía hacer que la gestión de sesiones HTTP/2
    leyera memoria que había sido liberada previamente, durante el cierre («shutdown») de conexiones.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-10092">CVE-2019-10092</a>

    <p>Matei <q>Mal</q> Badanoiu informó de una vulnerabilidad limitada de ejecución de scripts entre sitios («limited cross-site
    scripting») en la página de error de mod_proxy.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-10097">CVE-2019-10097</a>

    <p>Daniel McCarney informó de que cuando se configuraba mod_remoteip para
    usar un servidor proxy intermediario de confianza que utilizara el protocolo <q>PROXY</q>,
    una cabecera PROXY preparada de una manera determinada podía desencadenar un desbordamiento
    de pila o una desreferencia de puntero NULL. Esta vulnerabilidad solo podía ser
    desencadenada por un proxy de confianza y no por clientes HTTP no confiables. Este
    problema no afecta a la distribución stretch.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-10098">CVE-2019-10098</a>

    <p>Yukitsugu Sasaki informó de una potencial vulnerabilidad de redirección abierta en
    el módulo mod_rewrite.</p></li>

</ul>

<p>Para la distribución «antigua estable» (stretch), estos problemas se han corregido
en la versión 2.4.25-3+deb9u8.</p>

<p>Para la distribución «estable» (buster), estos problemas se han corregido en
la versión 2.4.38-3+deb10u1.</p>

<p>Le recomendamos que actualice los paquetes de apache2.</p>

<p>Para información detallada sobre el estado de seguridad de apache2, consulte su página
en el sistema de seguimiento de problemas de seguridad:
<a href="https://security-tracker.debian.org/tracker/apache2">https://security-tracker.debian.org/tracker/apache2</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2019/dsa-4509.data"
