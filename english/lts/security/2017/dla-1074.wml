<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several buffer and integer overflow issues were discovered in Poppler,
a PDF library, that could lead to application crash or possibly other
unspecified impact via maliciously crafted files.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
0.18.4-6+deb7u2.</p>

<p>We recommend that you upgrade your poppler packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-1074.data"
# $Id: $
