#use wml::debian::translation-check translation="8ce24a8757eea2129e97c895ce1eb6f1605eb1ee" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans le noyau Linux qui
pourraient conduire à une élévation des privilèges, un déni de service ou une
fuite d'informations.</p>

<p>Ce texte d’alerte mis à jour ajoute une note sur le besoin d’installer de
nouveaux paquets binaires.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-5995">CVE-2018-5995</a>

<p>ADLab de VenusTech a découvert que le noyau journalisait les adresses
virtuelles assignées dans des données particulières à chaque CPU. Cela pourrait
faciliter l’exploitation d’autres vulnérabilités.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-12126">CVE-2018-12126</a>, <a href="https://security-tracker.debian.org/tracker/CVE-2018-12127">CVE-2018-12127</a>, <a href="https://security-tracker.debian.org/tracker/CVE-2018-12130">CVE-2018-12130</a>, <a href="https://security-tracker.debian.org/tracker/CVE-2019-11091">CVE-2019-11091</a>

<p>Plusieurs chercheurs ont découvert des vulnérabilités dans la manière dont
la conception des processeurs d’Intel implémente la redirection spéculative de
données accumulées dans des structures microarchitecturales temporaires
(tampons). Ce défaut pourrait permettre à un attaquant contrôlant des processus
sans privilège de lire des informations sensibles, y compris celles du noyau et
de tous les autres processus exécutés dans le système, ou à travers les limites
d’invité/hôte pour lire la mémoire de l’hôte.</p>

<p>Consulter <a href="https://www.kernel.org/doc/html/latest/admin-guide/hw-vuln/mds.html">https://www.kernel.org/doc/html/latest/admin-guide/hw-vuln/mds.html</a>
pour davantage de détails.</p>

<p>Pour remédier complètement à ces vulnérabilités, il est aussi nécessaire
d’installer le microcode mis à jour du CPU. Un paquet intel-microcode mis à jour
(seulement disponible dans Debian non-free) a été fourni avec DLA-1789-1. Le
microcode de CPU mis à jour est aussi disponible comme partie de la mise à jour
du micrologiciel du système (« BIOS »).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-2024">CVE-2019-2024</a>

<p>Un bogue d’utilisation de mémoire après libération a été découvert dans le
pilote de capture vidéo em28xx. Des utilisateurs locaux pourraient être capables
d’utiliser cela pour un déni de service (corruption de mémoire ou plantage) ou
éventuellement pour une élévation des privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-3459">CVE-2019-3459</a>, <a href="https://security-tracker.debian.org/tracker/CVE-2019-3460">CVE-2019-3460</a>

<p>Shlomi Oberman, Yuli Shapiro et l’équipe de recherche Karamba Security Ltd.
ont découvert une vérification manquante d’intervalle dans l’implémentation
Bluetooth L2CAP. Si le bluetooth est activé, un attaquant à proximité pourrait
utiliser cela pour lire des informations sensibles du noyau.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-3882">CVE-2019-3882</a>

<p>L’implémentation de vfio ne limitait pas le nombre de mappages DMA à la
mémoire du périphérique. Un utilisateur local possédant un périphérique vfio
pourrait utiliser cela pour provoquer un déni de service (condition d’épuisement
de mémoire).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-3901">CVE-2019-3901</a>

<p>Jann Horn de Google a signalé une situation de compétition qui pourrait
permettre à un utilisateur de lire des évènements de performance d’une tâche
après son exécution de programme setuid. Cela pourrait divulguer des
informations sensibles traitées par des programmes setuid. La configuration du
noyau Debian ne permet pas aux utilisateurs non privilégiés d’accéder aux
évènements de performance par défaut, ce qui atténue ce problème.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-6133">CVE-2019-6133</a>

<p>Jann Horn de Google a trouvé que la vérification d’authentification de Policykit
pourrait être contournée par un utilisateur local créant un processus avec les
mêmes instants de départ et ID de processus qu’un ancien processus authentifié.
PolicyKit a été déjà été mis à jour pour corriger cela dans DLA-1644-1. Le noyau a
été de plus mis à jour pour éviter un délai entre les assignations d’instant de
départ et d’ID de processus, ce qui devrait rendre cette attaque irréalisable.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-9503">CVE-2019-9503</a>

<p>Hugues Anguelkov et d’autres de Quarkslab ont découvert que le pilote
brcmfmac (Broadcom wifi FullMAC) ne différenciait pas correctement les messages
envoyés par le micrologiciel wifi d’autres paquets. Un attaquant utilisant le
même réseau wifi pourrait utiliser cela pour un déni de service ou pour
exploiter d’autres vulnérabilités dans le pilote.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-11190">CVE-2019-11190</a>

<p>Robert Święcki a signalé que quand un programme setuid était exécuté, il
était toujours possible de lire les évènements de performance pendant que le
noyau réglait l’espace d’adresses du programme. Un utilisateur local pourrait
utiliser cela pour contrecarrer ASLR dans un programme setuid, facilitant
l’exploitation d’autres vulnérabilités dans le programme. La configuration du
noyau Debian ne permet pas aux utilisateurs non privilégiés d’accéder aux
évènements de performance par défaut, ce qui atténue ce problème.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-11486">CVE-2019-11486</a>

<p>Jann Horn de Google a signalé de nombreuses situations de compétition dans
la discipline de ligne R3964 de Siemens. Un utilisateur local pourrait utiliser
cela pour provoquer des impacts de sécurité non précisés. Ce module a été
par conséquent désactivé.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-11599">CVE-2019-11599</a>

<p>Jann Horn de Google a signalé une situation de compétition dans
l’implémentation centrale de dump qui pourrait conduire à utilisation de mémoire
après libération. Un utilisateur local pourrait utiliser cela pour lire des
informations sensibles, pour provoquer un déni de service (corruption de
mémoire) ou pour une élévation des privilèges.</p></li>

</ul>
<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 3.16.68-1. Cette version inclut aussi un correctif pour le bogue
n° 927781 de Debian et d’autres correctifs inclus dans les mises à jour de
l’amont pour stable.</p>

<p>Nous vous recommandons de mettre à jour vos paquets linux et linux-latest.
Vous devez utiliser « apt-get upgrade --with-new-pkgs »
ou <q>apt upgrade</q> car les noms des paquets binaires ont changé.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1799-2.data"
# $Id: $
