# Turkish messages for Debian Web pages (debian-webwml).
# Copyright (C) 2003, 2004 Software in the Public Interest, Inc.
# This file is distributed under the same license as Debian Web pages.
#
# Murat Demirten <murat@debian.org>, 2002, 2003, 2004.
# Recai Oktaş <roktas@omu.edu.tr>, 2005.
#
msgid ""
msgstr ""
"Project-Id-Version: debian-webwml\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2003-11-10 15:06+0100\n"
"PO-Revision-Date: 2006-01-18 05:44+0200\n"
"Last-Translator: Recai Oktaş <roktas@omu.edu.tr>\n"
"Language-Team: Debian L10n Turkish <debian-l10n-turkish@lists.debian.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms:  nplurals=1; plural=0;\n"

#: ../../english/doc/books.data:35
msgid ""
"\n"
"  Debian 9 is the must-have handbook for learning Linux. Start on the\n"
"  beginners level and learn how to deploy the system with graphical\n"
"  interface and terminal.\n"
"  This book provides the basic knowledge to grow and become a 'junior'\n"
"  systems administrator. Start off with exploring the GNOME desktop\n"
"  interface and adjust it to your personal needs. Overcome your fear of\n"
"  using the Linux terminal and learn the most essential commands in\n"
"  administering Debian. Expand your knowledge of system services (systemd)\n"
"  and learn how to adapt them. Get more out of the software in Debian and\n"
"  outside of Debian. Manage your home-network with network-manager, etc.\n"
"  10 percent of the profits on this book will be donated to the Debian\n"
"  Project."
msgstr ""

#: ../../english/doc/books.data:64 ../../english/doc/books.data:174
#: ../../english/doc/books.data:229
msgid ""
"Written by two Debian developers, this free book\n"
"  started as a translation of their French best-seller known as Cahier de\n"
"  l'admin Debian (published by Eyrolles). Accessible to all, this book\n"
"  teaches the essentials to anyone who wants to become an effective and\n"
"  independent Debian GNU/Linux administrator.\n"
"  It covers all the topics that a competent Linux administrator should\n"
"  master, from the installation and the update of the system, up to the\n"
"  creation of packages and the compilation of the kernel, but also\n"
"  monitoring, backup and migration, without forgetting advanced topics\n"
"  like SELinux setup to secure services, automated installations, or\n"
"  virtualization with Xen, KVM or LXC."
msgstr ""

#: ../../english/doc/books.data:86
msgid ""
"The aim of this freely available, up-to-date, book is to get you up to\n"
"  speed with Debian (including both the current stable release and the\n"
"  current unstable distribution). It is comprehensive with basic support\n"
"  for the user who installs and maintains the system themselves (whether\n"
"  in the home, office, club, or school)."
msgstr ""

#: ../../english/doc/books.data:108
msgid ""
"The first French book about Debian is already in its fifth edition. It\n"
"  covers all aspects of the administration of Debian from the installation\n"
"  to the configuration of network services.\n"
"  Written by two Debian developers, this book can be of interest to many\n"
"  people: the beginner wishing to discover Debian, the advanced user "
"looking\n"
"  for tips to enhance his mastership of the Debian tools and the\n"
"  administrator who wants to build a reliable network with Debian."
msgstr ""

#: ../../english/doc/books.data:128
msgid ""
"The book covers topics ranging from concepts of package\n"
"  management over the available tools and how they're used to concrete "
"problems\n"
"  which may occur in real life and how they can be solved. The book is "
"written\n"
"  in German, an English translation is planned. Format: e-book (Online, "
"HTML,\n"
"  PDF, ePub, Mobi), printed book planned.\n"
msgstr ""

#: ../../english/doc/books.data:149
msgid ""
"This book teaches you how to install and configure the system and also how "
"to use Debian in a professional environment.  It shows the full potential of "
"the distribution (in its current version 8) and provides a practical manual "
"for all users who want to learn more about Debian and its range of services."
msgstr ""

#: ../../english/doc/books.data:203
msgid ""
"Written by two penetration researcher - Annihilator, Firstblood.\n"
"  This book teaches you how to build and configure Debian 8.x server system\n"
"  security hardening using Kali Linux and Debian simultaneously.\n"
"  DNS, FTP, SAMBA, DHCP, Apache2 webserver, etc.\n"
"  From the perspective that 'the origin of Kali Linux is Debian', it "
"explains\n"
"  how to enhance Debian's security by applying the method of penetration\n"
"  testing.\n"
"  This book covers various security issues like SSL cerificates, UFW "
"firewall,\n"
"  MySQL Vulnerability, commercial Symantec antivirus, including Snort\n"
"  intrusion detection system."
msgstr ""

#: ../../english/doc/books.def:38
msgid "Author:"
msgstr "Yazar"

#: ../../english/doc/books.def:41
#, fuzzy
msgid "Debian Release:"
msgstr "Debian paketi"

#: ../../english/doc/books.def:44
msgid "email:"
msgstr "eposta:"

#: ../../english/doc/books.def:48
msgid "Available at:"
msgstr "Bulunan yerler:"

#: ../../english/doc/books.def:51
msgid "CD Included:"
msgstr "CD içeriyor:"

#: ../../english/doc/books.def:54
msgid "Publisher:"
msgstr "Yayımcı:"

#: ../../english/doc/manuals.defs:28
msgid "Authors:"
msgstr "Yazarlar:"

#: ../../english/doc/manuals.defs:35
msgid "Editors:"
msgstr "Editörler:"

#: ../../english/doc/manuals.defs:42
msgid "Maintainer:"
msgstr "Geliştirici:"

#: ../../english/doc/manuals.defs:49
msgid "Status:"
msgstr "Durum:"

#: ../../english/doc/manuals.defs:56
msgid "Availability:"
msgstr "Bulunabilirlik:"

#: ../../english/doc/manuals.defs:85
msgid "Latest version:"
msgstr "En son sürüm:"

#: ../../english/doc/manuals.defs:101
msgid "(version <get-var version />)"
msgstr "(sürüm <get-var version />)"

#: ../../english/doc/manuals.defs:131 ../../english/releases/arches.data:38
msgid "plain text"
msgstr "düz metin"

#: ../../english/doc/manuals.defs:147 ../../english/doc/manuals.defs:157
#: ../../english/doc/manuals.defs:165
msgid ""
"The latest <get-var srctype /> source is available through the <a href="
"\"https://packages.debian.org/git\">Git</a> repository."
msgstr ""

#: ../../english/doc/manuals.defs:149 ../../english/doc/manuals.defs:159
#: ../../english/doc/manuals.defs:167
msgid "Web interface: "
msgstr ""

#: ../../english/doc/manuals.defs:150 ../../english/doc/manuals.defs:160
#: ../../english/doc/manuals.defs:168
msgid "VCS interface: "
msgstr ""

#: ../../english/doc/manuals.defs:175 ../../english/doc/manuals.defs:179
msgid "Debian package"
msgstr "Debian paketi"

#: ../../english/doc/manuals.defs:184 ../../english/doc/manuals.defs:188
#, fuzzy
msgid "Debian package (archived)"
msgstr "Debian paketi"

#: ../../english/releases/arches.data:36
msgid "HTML"
msgstr ""

#: ../../english/releases/arches.data:37
msgid "PDF"
msgstr ""

#~ msgid "Language:"
#~ msgstr "Dil:"

#~ msgid ""
#~ "<p>You can visit the current <a href=\"manuals/<get-var doc />/"
#~ "\">development version</a> here, use <a href=\"cvs\">CVS</a> to download "
#~ "the SGML source text."
#~ msgstr ""
#~ "<p>Şu an <a href=\"manuals/<get-var doc />/\">geliştirilmekte olan</a> "
#~ "versiyona bakabilir veya isterseniz <a href=\"cvs\">CVS</a> kullanarak "
#~ "SGML formatında kaynağı bilgisayarınıza indirebilirsiniz."

#, fuzzy
#~| msgid ""
#~| "Use <a href=\"cvs\">CVS</a> to download the SGML source text for <get-"
#~| "var ddp_pkg_loc />."
#~ msgid ""
#~ "Use <a href=\"cvs\">SVN</a> to download the SGML source text for <get-var "
#~ "ddp_pkg_loc />."
#~ msgstr ""
#~ "<get-var ddp_pkg_loc /> SGML kaynak metnini indirmek için <a href=\"cvs"
#~ "\">CVS</a> kullanın."

#~ msgid "CVS via web"
#~ msgstr "Web üzerinden CVS"

#~ msgid ""
#~ "CVS sources working copy: set <code>CVSROOT</code>\n"
#~ "  to <kbd>:ext:<var>userid</var>@cvs.debian.org:/cvs/debian-boot</kbd>,\n"
#~ "  and check out the <kbd>boot-floppies/documentation</kbd> module."
#~ msgstr ""
#~ "CVS kaynaklarından çalışan kopya: <code>CVSROOT</code>'u\n"
#~ "  <kbd>:ext:<var>userid</var>@cvs.debian.org:/cvs/debian-boot</kbd>\n"
#~ "  olarak ayarla ve <kbd>boot-floppies/documentation</kbd> modülünü "
#~ "getirin."
