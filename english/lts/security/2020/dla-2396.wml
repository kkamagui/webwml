<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>A vulnerability was discovered in tigernvc, a Virtual Network Computing
client and server implementation.  The viewer implementation mishandles
TLS certificate exceptions, storing the certificates as authorities,
meaning that the owner of a certificate could impersonate any server
after a client had added an exception.</p>

<p>For Debian 9 stretch, this problem has been fixed in version
1.7.0+dfsg-7+deb9u2.</p>

<p>We recommend that you upgrade your tigervnc packages.</p>

<p>For the detailed security status of tigervnc please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/tigervnc">https://security-tracker.debian.org/tracker/tigervnc</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2396.data"
# $Id: $
