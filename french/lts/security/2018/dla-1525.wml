#use wml::debian::translation-check translation="ab3be4ee01879fd4484c795bbaa824377c218575" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-7653">CVE-2017-7653</a>

<p>Des chaînes UTF-8 non valables ne sont pas correctement vérifiées. Un
attaquant pourrait causer un déni de service pour d’autres clients en les
déconnectant du courtier avec des sujets spécialement contrefaits.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-7654">CVE-2017-7654</a>

 <p>À cause d’une fuite de mémoire, des clients non authentifiés peuvent envoyer
 des paquets CONNECT spécialement contrefaits qui pourraient causer un déni de
 service dans le courtier.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-9868">CVE-2017-9868</a>

<p>À cause de permissions incorrectes de fichier, des utilisateurs locaux
 pourraient obtenir des informations de sujet dans la base de données de
 mosquitto.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 1.3.4-2+deb8u3.</p>
<p>Nous vous recommandons de mettre à jour vos paquets mosquitto.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1525.data"
# $Id: $
