<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Several issues have been discovered in the MariaDB database server. The
vulnerabilities are addressed by upgrading MariaDB to the new upstream
version 10.1.37. Please see the MariaDB 10.1 Release Notes for further
details:</p>

<ul>
<li><a href="https://mariadb.com/kb/en/mariadb/mariadb-10127-release-notes/">https://mariadb.com/kb/en/mariadb/mariadb-10127-release-notes/</a></li>
<li><a href="https://mariadb.com/kb/en/mariadb/mariadb-10128-release-notes/">https://mariadb.com/kb/en/mariadb/mariadb-10128-release-notes/</a></li>
<li><a href="https://mariadb.com/kb/en/mariadb/mariadb-10129-release-notes/">https://mariadb.com/kb/en/mariadb/mariadb-10129-release-notes/</a></li>
<li><a href="https://mariadb.com/kb/en/mariadb/mariadb-10130-release-notes/">https://mariadb.com/kb/en/mariadb/mariadb-10130-release-notes/</a></li>
<li><a href="https://mariadb.com/kb/en/mariadb/mariadb-10131-release-notes/">https://mariadb.com/kb/en/mariadb/mariadb-10131-release-notes/</a></li>
<li><a href="https://mariadb.com/kb/en/mariadb/mariadb-10132-release-notes/">https://mariadb.com/kb/en/mariadb/mariadb-10132-release-notes/</a></li>
<li><a href="https://mariadb.com/kb/en/mariadb/mariadb-10133-release-notes/">https://mariadb.com/kb/en/mariadb/mariadb-10133-release-notes/</a></li>
<li><a href="https://mariadb.com/kb/en/mariadb/mariadb-10134-release-notes/">https://mariadb.com/kb/en/mariadb/mariadb-10134-release-notes/</a></li>
<li><a href="https://mariadb.com/kb/en/mariadb/mariadb-10135-release-notes/">https://mariadb.com/kb/en/mariadb/mariadb-10135-release-notes/</a></li>
<li><a href="https://mariadb.com/kb/en/mariadb/mariadb-10136-release-notes/">https://mariadb.com/kb/en/mariadb/mariadb-10136-release-notes/</a></li>
<li><a href="https://mariadb.com/kb/en/mariadb/mariadb-10137-release-notes/">https://mariadb.com/kb/en/mariadb/mariadb-10137-release-notes/</a></li>
</ul>

<p>For the stable distribution (stretch), these problems have been fixed in
version 10.1.37-0+deb9u1.</p>

<p>We recommend that you upgrade your mariadb-10.1 packages.</p>

<p>For the detailed security status of mariadb-10.1 please refer to its
security tracker page at:
<a href="https://security-tracker.debian.org/tracker/mariadb-10.1">\
https://security-tracker.debian.org/tracker/mariadb-10.1</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2018/dsa-4341.data"
# $Id: $
