#use wml::debian::translation-check translation="b25cf9c3843d7f18528e53f42a827ce1cde4d5a6" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Plusieurs problèmes ont été découvertes dans tiff, une bibliothèque pour Tag
Image File Format.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-17546">CVE-2019-17546</a>

<p>L’interface RGBA contenait un dépassement d'entier qui pouvait conduire à un
dépassement en écriture de tampon basé sur le tas.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-6128">CVE-2019-6128</a>

<p>Une fuite de mémoire existait due à un code de nettoyage manquant.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-18661">CVE-2018-18661</a>

<p>En cas d’épuisement de mémoire un déréférencement de pointeur NULL existait
dans tiff2bw.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-12900">CVE-2018-12900</a>

<p>Correction pour un dépassement de tampon basé sur le tas qui pourrait être
utilisé pour planter l’application ou même pour exécuter du code arbitraire
(avec les droits de l’utilisateur exécutant cette application).</p></li>

<li><p><a href="https://security-tracker.debian.org/tracker/CVE-2017-17095">CVE-2017-17095</a></p>

<p>Un fichier tiff contrefait pourrait conduire à un dépassement de tampon basé
sur le tas dans pal2rgb.</p>


<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 4.0.3-12.3+deb8u10.</p>

<p>Nous vous recommandons de mettre à jour vos paquets tiff.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p></li>

</ul>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-2009.data"
# $Id: $
