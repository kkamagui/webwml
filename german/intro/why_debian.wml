#use wml::debian::template title="Gründe für die Wahl von Debian" MAINPAGE="true"
#include "$(ENGLISHDIR)/releases/info"
#use wml::debian::translation-check translation="6bf7f6cbd44963bfd9857f4e2970c7f8a876a801"
#use wml::debian::acronyms
# $Id$
# Translator  : Philipp Frauenfelder <pfrauenf@debian.org>
# Changed by: Thimo Neubauer <thimo@debian.org>
# Changed by: Martin Schulze <joey@debian.org>
# and others (see changelog)
# Updated: Holger Wansing <linux@wansing-online.de>, 2013.
# Updated: Holger Wansing <hwansing@mailbox.org>, 2020.

<p>Es gibt eine Reihe von Gründen, warum Benutzer Debian als Betriebssystem auswählen:</p>

<h1>Hauptgründe</h1>

# try to list reasons for end users first

<dl>
  <dt><strong>Debian ist Freie Software.</strong></dt>
  <dd>
    Debian wird aus freier und quelloffener Software erstellt und wird auch selbst
    immer zu 100% <a href="free">frei</a> sein. Frei für jeden zur Benutzung, Änderung
    und Weiterverbreitung. Dies ist im Kern unser Versprechen an
    <a href="../users">unsere Benutzer</a>. Es wird auch kostenfrei abgegeben.
  </dd>
</dl>

<dl>
  <dt><strong>Debian ist ein stabiles und sicheres Linux-basiertes Betriebssystem.</strong></dt>
  <dd>
    Debian ist ein Betriebssystem für eine breite Palette an Geräten, inklusive
    Laptops, Arbeitsplatzrechner und Server. Benutzer mögen seine langjährige
    Stabilität und Zuverlässigkeit (Debian existiert seit 1993).
    Wir stellen sinnvolle Standardkonfigurationen für jedes unserer Pakete
    zur Verfügung. Wenn irgendwie möglich, bieten die Debian-Entwickler
    Sicherheitsaktualisierungen für alle Pakete über deren gesamte Lebenszeit an.
  </dd>
</dl>

<dl>
  <dt><strong>Debian hat reichhaltige Hardware-Unterstützung.</strong></dt>
  <dd>
    Die meiste Hardware wird bereits durch den Linux-Kernel unterstützt.
    Proprietäre Hardware-Treiber sind verfügbar, wenn die freie Software hier
    nicht ausreicht.
  </dd>
</dl>

<dl>
  <dt><strong>Debian bietet problemlose Versions-Upgrades.</strong></dt>
  <dd>
    Debian ist bekannt für seine einfachen und problemlosen Upgrades, sowohl
    innerhalb eines Release-Zyklus' wie auch zur nächsthöheren Version.
  </dd>
</dl>

<dl>
  <dt><strong>Debian ist die Saat und die Basis für viele andere Distributionen.</strong></dt>
  <dd>
    Viele andere populäre Distributionen, wie Ubuntu, Knoppix, PureOS, SteamOS oder
    Tails, nutzen Debian als Basis für Ihre eigene Software.
    Debian stellt all die Werkzeuge zur Verfügung, mit denen jeder die Pakete aus
    dem Debian-Archiv um weitere Software erweitern kann, um die eigenen Bedürfnisse
    zu erfüllen.
  </dd>
</dl>

<dl>
  <dt><strong>Das Debian-Projekt ist eine Gemeinschaft.</strong></dt>
  <dd>
    Debian ist nicht nur ein Linux-Betriebssystem. Die Software wird zusammen von
    Hunderten von Freiwilligen auf der ganzen Welt erstellt. Sie können auch Teil
    der Debian-Gemeinschaft sein, selbst wenn Sie kein Programmierer oder
    Systemadministrator sind. Debian wird von Gemeinschaft und Einigkeit
    angetrieben und hat eine
    <a href="../devel/constitution">demokratische Kontrollstruktur</a>.
    Da alle Debian-Entwickler die gleichen Rechte haben, kann Debian nicht von
    einer einzigen Firma kontrolliert oder gesteuert werden. Es gibt Debian-Entwickler
    in mehr als 60 Ländern und wir unterstützen in unserem Debian Installer mehr als
    80 Sprachen.
  </dd>
</dl>

<dl>
  <dt><strong>Debian unterstützt in seinem Installer eine Vielzahl von Optionen.</strong></dt>
  <dd>
    Endbenutzer werden unsere
    <a href="https://cdimage.debian.org/images/release/current-live/amd64/iso-hybrid/">Live-CDs</a>
    mögen, die den einfach zu verwendenden Calamares-Installer enthalten; dieser erfordert
    für die Installation nur wenige Eingaben und ein geringes Vorwissen.
    Erfahrene Nutzer können auch unseren einzigartigen voll ausgestatteten Installer
    verwenden, und Experten haben sogar die Möglichkeit, alle Aspekte der Installation
    fein granular zu steuern bis hin zu einer automatisierten Netzwerk-Installation.
  </dd>
</dl>

<br>

<h1>Professionelle Umgebung</h1>

<p>
    Wenn Sie Debian in einer professionellen Umgebung einsetzen, werden Sie diese
    zusätzlichen Vorteile mögen:
</p>

<dl>
  <dt><strong>Debian ist verlässlich.</strong></dt>
  <dd>
    Debian beweist seine Zuverlässigkeit jeden Tag in Tausenden Szenarien der
    realen Welt, die von einem Laptop für eine einzelne Person über
    Super-Computer-Rechenzentren und Börsen für den Aktienhandel bis hin zur
    Automobilbranche reichen. Es ist auch im akademischen Umfeld beliebt sowie
    in der Wissenschaft und im öffentlichen Sektor.
  </dd>
</dl>

<dl>
  <dt><strong>Debian hat viele Experten.</strong></dt>
  <dd>
    Unsere Paketbetreuer kümmern sich nicht nur um die Paketierung im Debian-Projekt
    und die Integration von neuen Upstream-Versionen. Oft sind sie auch Experten
    im Umfeld der Upstream-Software und tragen direkt zur Upstream-Entwicklung bei.
    Teilweise sind sie sogar selbst Upstream-Autoren.
  </dd>
</dl>

<dl>
  <dt><strong>Debian ist sicher.</strong></dt>
  <dd>
    Debian bietet Sicherheitsunterstützung für alle seine Stable-Veröffentlichungen.
    Viele andere Distributionen und Sicherheitsforscher verlassen sich auf
    Debians Security Tracker.
  </dd>
</dl>

<dl>
  <dt><strong>Langzeitunterstützung.</strong></dt>
  <dd>
    Es gibt kostenlose <a href="https://wiki.debian.org/LTS">Langzeitunterstützung</a>
    (Long Term Support, LTS). Sie erhalten dadurch erweiterte Unterstützung für
    die Stable-Veröffentlichung über einen Zeitraum von 5 Jahren.
    Darüber hinaus gibt es sogar noch eine Initiative für erweiterte Unterstützung
    (<a href="https://wiki.debian.org/LTS/Extended">Extended LTS</a>), die die
    Versorung mit Sicherheits-Updates für eine eingeschränkte Menge an Paketen
    auch noch über den 5-Jahres-Zeitraum hinaus anbietet.
  </dd>
</dl>

<dl>
  <dt><strong>Cloud-Images.</strong></dt>
  <dd>
    Es gibt offizielle Cloud-Images für alle großen Cloud-Plattformen.
    Wir bieten auch die Werkzeuge und Konfigurationen, so dass Sie sich Ihr
    eigenes angepasstes Cloud-Image bauen können.
    Sie können Debian auch in einer virtuellen Maschine auf einem Arbeitsplatzrechner
    oder in einem Container verwenden.
  </dd>
</dl>

<br>

<h1>Entwickler</h1>
<p>Debian wird in hohem Maße von allen Arten von Software- und
   Hardware-Entwicklern eingesetzt.</p>

<dl>
  <dt><strong>Öffentlich verfügbare Fehlerdatenbank.</strong></dt>
  <dd>
    Unsere <a href="../Bugs">Fehlerdatenbank</a> (Bug Tracking System, BTS)
    ist für jeden über einen Webbrowser öffentlich zugänglich.
    Wir verstecken die Fehler in unserer Software nicht und Sie können ganz
    einfach selbst neue Fehlerberichte einreichen.
  </dd>
</dl>

<dl>
  <dt><strong>IoT (Internet of Things) und Embedded Devices.</strong></dt>
  <dd>
    Wir unterstützen eine breite Palette von Geräten wie den Raspberry Pi,
    Varianten des QNAP, mobile Geräte, Heimrouter und eine Reihe von
    Single-Board-Computern (SBC).
  </dd>
</dl>

<dl>
  <dt><strong>Eine Vielzahl von Hardware-Architekturen.</strong></dt>
  <dd>
    Unterstützung für eine <a href="../ports">lange Liste</a> von CPU-Architekturen
    inklusive amd64, i386, mehrere Versionen von ARM und MIPS, POWER7, POWER8,
    IBM System z und RISC-V. Debian ist auch für ältere und spezielle
    Nischenarchitekturen verfügbar.
  </dd>
</dl>

<dl>
  <dt><strong>Eine riesige Anzahl an verfügbaren Software-Paketen.</strong></dt>
  <dd>
    Debian hat die größte Anzahl installierbarer Paketen (derzeit
    <packages_in_stable>). Unsere Pakete nutzen das deb-Format, welches sehr bekannt
    ist für seine hohe Qualität.
  </dd>
</dl>

<dl>
  <dt><strong>Verschiedene Veröffentlichungen zur Auswahl.</strong></dt>
  <dd>
    Neben unserer Stable-Veröffentlichung können Sie die neuesten
    verfügbaren Versionen von Software erhalten, indem Sie die Testing-
    und Unstable-Releases nutzen.
  </dd>
</dl>

<dl>
  <dt><strong>Hohe Qualität dank der Hilfe von Entwicklerwerkzeugen und Policy.</strong></dt>
  <dd>
    Eine Reihe von Entwicklerwerkzeugen helfen dabei, die Paketqualität auf
    einem hohen Niveau zu halten und unsere <a href="../doc/debian-policy/">Policy</a>
    definiert die technischen Anforderungen, die jedes Paket erfüllen muss, um
    in der Distribution enthalten zu sein. In unserer Continuous-Integration-Umgebung
    läuft die autopkgtest-Software, piuparts ist unser Werkzeug zum Testen von
    Paketinstallation, -aktualisierung und -entfernung, und lintian ist ein
    umfangreiches und detailreiches Paketprüfprogramm für Debian-Pakete.
  </dd>
</dl>

<br>

<h1>Was unsere Nutzer sagen</h1>

<ul style="line-height: 3em">
  <li>
    <q><strong>
      Für mich ist es das perfekte Level von einfacher Bedienung und Stabilität.
      Ich habe über viele Jahre bereits viele verschiedene Distributionen genutzt,
      aber Debian ist die einzige, die einfach funktioniert.
    </strong></q>
  </li>

  <li>
    <q><strong>
      Solide wie ein Fels. Massenhaft Pakete. Exzellente Gemeinschaft.
    </strong></q>
  </li>

  <li>
    <q><strong>
      Debian ist für das Symbol für Stabilität und einfache Nutzung.
    </strong></q>
  </li>
</ul>
