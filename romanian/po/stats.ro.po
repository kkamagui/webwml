# SOME DESCRIPTIVE TITLE.
# Copyright ©
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
msgid ""
msgstr ""
"Project-Id-Version: Debian webwml organization\n"
"POT-Creation-Date: \n"
"PO-Revision-Date: 2016-09-20 12:30+0100\n"
"Last-Translator: Automatically generated\n"
"Language-Team: none\n"
"Language: ro\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.8.8\n"

#: ../../english/template/debian/stats_tags.wml:6
msgid "Debian web site translation statistics"
msgstr "Statistici cu privire la traducerea site-ului Debian"

#: ../../english/template/debian/stats_tags.wml:10
msgid "There are %d pages to translate."
msgstr "Mai sunt %d pagini de tradus."

#: ../../english/template/debian/stats_tags.wml:14
msgid "There are %d bytes to translate."
msgstr "Mai sunt %d bytes de tradus."

#: ../../english/template/debian/stats_tags.wml:18
msgid "There are %d strings to translate."
msgstr "Mai sunt %d șiruri de caractere de tradus."

#: ../../stattrans.pl:282 ../../stattrans.pl:498
msgid "Wrong translation version"
msgstr "Versiune greșită a traducerii"

#: ../../stattrans.pl:284
msgid "This translation is too out of date"
msgstr "Această traducere este învechită."

#: ../../stattrans.pl:286
msgid "The original is newer than this translation"
msgstr "Versiunea originală este mai nouă decât traducerea."

#: ../../stattrans.pl:290 ../../stattrans.pl:498
msgid "The original no longer exists"
msgstr "Versiunea originală nu mai există."

#: ../../stattrans.pl:474
msgid "hit count N/A"
msgstr "număr de clicuri N/A"

#: ../../stattrans.pl:474
msgid "hits"
msgstr "clicuri"

#: ../../stattrans.pl:492 ../../stattrans.pl:493
msgid "Click to fetch diffstat data"
msgstr "Clic aici ca să afli informații despre modificare"

#: ../../stattrans.pl:603 ../../stattrans.pl:743
msgid "Created with <transstatslink>"
msgstr ""

#: ../../stattrans.pl:608
msgid "Translation summary for"
msgstr "Rezumatul traducerii pentru"

#: ../../stattrans.pl:611 ../../stattrans.pl:767 ../../stattrans.pl:813
#: ../../stattrans.pl:856
msgid "Not translated"
msgstr "Netradus"

#: ../../stattrans.pl:611 ../../stattrans.pl:766 ../../stattrans.pl:812
msgid "Outdated"
msgstr "Învechit"

#: ../../stattrans.pl:611
msgid "Translated"
msgstr "Tradus"

#: ../../stattrans.pl:611 ../../stattrans.pl:691 ../../stattrans.pl:765
#: ../../stattrans.pl:811 ../../stattrans.pl:854
msgid "Up to date"
msgstr "La zi"

#: ../../stattrans.pl:612 ../../stattrans.pl:613 ../../stattrans.pl:614
#: ../../stattrans.pl:615
msgid "files"
msgstr "fișiere"

#: ../../stattrans.pl:618 ../../stattrans.pl:619 ../../stattrans.pl:620
#: ../../stattrans.pl:621
msgid "bytes"
msgstr "bytes"

#: ../../stattrans.pl:628
msgid ""
"Note: the lists of pages are sorted by popularity. Hover over the page name "
"to see the number of hits."
msgstr ""
"Notă: aceste liste de pagini sunt ordonate după popularitate. Pune cursorul "
"pe numele paginii ca să vezi numărul de clicuri,"

#: ../../stattrans.pl:634
msgid "Outdated translations"
msgstr "Traduceri învechite"

#: ../../stattrans.pl:636 ../../stattrans.pl:690
msgid "File"
msgstr "Fișier"

#: ../../stattrans.pl:638
msgid "Diff"
msgstr "Diferențe"

#: ../../stattrans.pl:640
msgid "Comment"
msgstr "Comentariu"

#: ../../stattrans.pl:641
msgid "Diffstat"
msgstr "Modificări"

#: ../../stattrans.pl:642
msgid "Git command line"
msgstr ""

#: ../../stattrans.pl:644
msgid "Log"
msgstr "Log"

#: ../../stattrans.pl:645
msgid "Translation"
msgstr "Traducere"

#: ../../stattrans.pl:646
msgid "Maintainer"
msgstr "Menținător"

#: ../../stattrans.pl:648
msgid "Status"
msgstr "Stare"

#: ../../stattrans.pl:649
msgid "Translator"
msgstr "Traducător"

#: ../../stattrans.pl:650
msgid "Date"
msgstr "Dată"

#: ../../stattrans.pl:657
msgid "General pages not translated"
msgstr "Pagini generale netraduse"

#: ../../stattrans.pl:658
msgid "Untranslated general pages"
msgstr "Pagini generale netraduse"

#: ../../stattrans.pl:663
msgid "News items not translated"
msgstr "Știri netraduse"

#: ../../stattrans.pl:664
msgid "Untranslated news items"
msgstr "Știri netraduse"

#: ../../stattrans.pl:669
msgid "Consultant/user pages not translated"
msgstr "Pagini de consultanță/ pentru utilizatori netradus"

#: ../../stattrans.pl:670
msgid "Untranslated consultant/user pages"
msgstr "Pagini de consultanță/ pentru utilizatori netraduse"

#: ../../stattrans.pl:675
msgid "International pages not translated"
msgstr "Pagini internaționale netraduse"

#: ../../stattrans.pl:676
msgid "Untranslated international pages"
msgstr "Pagini internaționale netraduse"

#: ../../stattrans.pl:681
msgid "Translated pages (up-to-date)"
msgstr "Pagini traduse (la zi)"

#: ../../stattrans.pl:688 ../../stattrans.pl:838
msgid "Translated templates (PO files)"
msgstr "Tipare de tradus (fișiere PO)"

#: ../../stattrans.pl:689 ../../stattrans.pl:841
msgid "PO Translation Statistics"
msgstr "Evidență a traducerilor PO"

#: ../../stattrans.pl:692 ../../stattrans.pl:855
msgid "Fuzzy"
msgstr "Fuzzy"

#: ../../stattrans.pl:693
msgid "Untranslated"
msgstr "Netraduse"

#: ../../stattrans.pl:694
msgid "Total"
msgstr "Total"

#: ../../stattrans.pl:711
msgid "Total:"
msgstr "Total:"

#: ../../stattrans.pl:745
msgid "Translated web pages"
msgstr "Pagini web traduse"

#: ../../stattrans.pl:748
msgid "Translation Statistics by Page Count"
msgstr "Evidență a traducerilor după numarul de pagini"

#: ../../stattrans.pl:763 ../../stattrans.pl:809 ../../stattrans.pl:853
msgid "Language"
msgstr "Limbi"

#: ../../stattrans.pl:764 ../../stattrans.pl:810
msgid "Translations"
msgstr "Traduceri"

#: ../../stattrans.pl:791
msgid "Translated web pages (by size)"
msgstr "Pagini web traduse (ordonate după marime)"

#: ../../stattrans.pl:794
msgid "Translation Statistics by Page Size"
msgstr "Evidență a traducerilor după mărimea paginilor"

#~ msgid "Unified diff"
#~ msgstr "Diferențe unificate"

#~ msgid "Colored diff"
#~ msgstr "Diferențe în culoare"

#, fuzzy
#~| msgid "Colored diff"
#~ msgid "Commit diff"
#~ msgstr "Diferențe în culoare"

#~ msgid "Created with"
#~ msgstr "Create cu"
