#use wml::debian::template title="Debian squeeze -- Guia de instalação" BARETITLE=true
#use wml::debian::release
#include "$(ENGLISHDIR)/releases/info"
#include "$(ENGLISHDIR)/releases/arches.data"
#include "$(ENGLISHDIR)/releases/squeeze/release.data"
#use wml::debian::translation-check translation="b0a7173484a7c517a30aa1fd7c4bf04ddf081b99"

<if-stable-release release="lenny">
<p>Esta é uma <strong>versão beta</strong> do guia de instalação para o Debian
6.0, codinome squeeze, que ainda não foi lançado. A informação
apresentada aqui pode estar desatualizada e/ou incorreta devido a mudanças
no instalador. Você talvez se interesse pelo
<a href="../lenny/installmanual">guia de instalação para o Debian GNU/Linux 5.0,
codinome lenny</a>, que é a última versão lançada do Debian; ou pela
<a href="https://d-i.debian.org/manual/">versão de desenvolvimento do guia de instalação</a>,
que é a versão mais atualizada deste documento.</p>
</if-stable-release>

<p>Instruções de instalação, junto aos arquivos que podem ser baixados, estão
disponíveis para cada arquitetura suportada:</p>

<ul>
<:= &permute_as_list('', 'Installation Guide'); :>
</ul>

<p>Se você definiu a localização do seu navegador de forma
apropriada, você pode usar o link acima para obter a versão HTML correta
automaticamente &mdash; veja sobre
<a href="$(HOME)/intro/cn">negociação de conteúdo</a>.
De outra forma, escolha a arquitetura exata, idioma e formato que você deseja
da tabela abaixo.</p>

<div class="centerdiv">
<table class="reltable">
<tr>
  <th align="left"><strong>Arquitetura</strong></th>
  <th align="left"><strong>Formato</strong></th>
  <th align="left"><strong>Idiomas</strong></th>
</tr>
<: &permute_as_matrix_new( file => 'install', langs => \%langsinstall,
			   formats => \%formats, arches => \@arches,
			   html_file => 'index', namingscheme => sub {
			   "$_[0].$_[2].$_[1]" } ); :>
</table>
</div>
