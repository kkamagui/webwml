#use wml::debian::blend title="블렌드 지원"
#use "navbar.inc"
#use wml::debian::translation-check translation="ac688ebb94c4334be47e6f542320d90001163462" maintainer="Sebul"

<h2>문서</h2>

<p>대개 다른 사람의 도움을 받기 전에 문제에 대한 답을 직접 찾아 보는 것이 좋습니다.
이러면 일반적으로 필요한 답변을 얻을 수 있으며, 그렇지 않더라도 문서를 읽는 경험이 앞으로 유용할 겁니다.
</p>

<p><a href="../../doc/">데비안 문서 페이지</a> 가능한 문서 목록.</p>

<h2>메일링 리스트</h2>

<p>데비안은 전 세계의 분산 개발을 통해 개발되었습니다.
따라서 이메일은 다양한 항목에 대해 선호하는 방법입니다.
데비안 개발자와 사용자 간의 많은 대화는 여러 메일링 리스트를 통해 관리됩니다.
</p>

# Translators: please swap this next one out for the local language's list.

<p>일반적 데비안 지원 <a
href="https://lists.debian.org/debian-user/">데비안 사용자 메일링 리스트</a>.</p>

<p>다른 언어 사용자 지원 <a
href="https://lists.debian.org/users.html">사용자를 위한 메일링 리스트 인덱스</a>.</p>

<h2>IRC를 사용한 온라인 실시긴 도움</h2>

<p><a href="http://www.irchelp.org/">IRC (Internet Relay Chat)</a>는 
전세계 사람들과 실시간으로 대화하는 방법입니다.
데비안 전용 IRC 채널은 <a href="https://www.oftc.net/">OFTC</a>에 있습니다.
</p>

<p>연결하려면 IRC 클라이언트가 필요합니다.
가장 인기있는 클라이언트 중 일부는
<a href="https://packages.debian.org/stable/net/hexchat">HexChat</a>,
<a href="https://packages.debian.org/stable/net/ircii">ircII</a>,
<a href="https://packages.debian.org/stable/net/irssi">irssi</a>,
<a href="https://packages.debian.org/stable/net/epic5">epic5</a> 및
<a href="https://packages.debian.org/stable/net/kvirc">KVIrc</a> 이며,
모두 Debian 용으로 패키지 되었습니다.
클라이언트가 설치되면 서버에 연결하도록 알려야합니다. 대부분의 클라이언트에서 다음을 입력하여 이를 수행할 수 있습니다:</p>

<pre>
/server irc.debian.org
</pre>

<p>연결되면, 채널 <code>#debian-user</code>에 참여하기 위해 아래와 같이 칩니다</p>

<pre>
/join #debian-user
</pre>

<p>주의: HexChat 같은 클라이언트는 때때로 다른, 그래픽 사용자 인터페이스를 써서 서버/채널에 조인합니다.</p>

<p>데비안에 대해 이야기할 수 있는 다른 IRC 네트워크도 많습니다. 눈에 띄는 하나는
<kbd>chat.freenode.net</kbd>에 있는
<a href="https://freenode.net/">freenode IRC network</a>입니다.</p>
