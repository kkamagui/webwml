<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Multiple security issues have been found in Thunderbird, which may lead
to the execution of arbitrary code or denial of service.</p>

<p>For Debian 9 stretch, these problems have been fixed in version
1:78.4.0-1~deb9u1.</p>

<p>We recommend that you upgrade your thunderbird packages.</p>

<p>For the detailed security status of thunderbird please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/thunderbird">https://security-tracker.debian.org/tracker/thunderbird</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2416.data"
# $Id: $
