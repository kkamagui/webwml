<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Several vulnerabilities were discovered in libass, a library for
manipulating the SubStation Alpha (SSA) subtitle file format. The Common
Vulnerabilities and Exposures project identifies the following issues.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-7969">CVE-2016-7969</a>

  <p>Mode 0/3 line wrapping equalization in specific cases which could
  result in illegal reads while laying out and shaping text.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-7972">CVE-2016-7972</a>

  <p>Memory reallocation issue in the shaper which lead to undefined
  behavior</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
0.10.0-3+deb7u1.</p>

<p>We recommend that you upgrade your libass packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-668.data"
# $Id: $
