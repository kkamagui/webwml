# Jorge Barreiro <yortx.barry@gmail.com>, 2012, 2014.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"PO-Revision-Date: 2014-03-16 21:28+0100\n"
"Last-Translator: Jorge Barreiro <yortx.barry@gmail.com>\n"
"Language-Team: Galician <proxecto@trasno.net>\n"
"Language: gl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 1.4\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#: ../../english/intro/organization.data:15
msgid "delegation mail"
msgstr "correo de delegacións"

#: ../../english/intro/organization.data:16
msgid "appointment mail"
msgstr "correo de nomeamentos"

#. One male delegate
#. Pronoun tags with delegate combinations
#: ../../english/intro/organization.data:18
#: ../../english/intro/organization.data:22
msgid "<void id=\"male\"/>delegate"
msgstr "<void id=\"male\"/>delegado"

#. One female delegate
#: ../../english/intro/organization.data:20
#: ../../english/intro/organization.data:23
msgid "<void id=\"female\"/>delegate"
msgstr "<void id=\"female\"/>delegada"

#: ../../english/intro/organization.data:22
#: ../../english/intro/organization.data:25
#, fuzzy
#| msgid "<void id=\"female\"/>delegate"
msgid "<void id=\"he_him\"/>he/him"
msgstr "<void id=\"female\"/>delegada"

#: ../../english/intro/organization.data:23
#: ../../english/intro/organization.data:26
#, fuzzy
#| msgid "<void id=\"female\"/>delegate"
msgid "<void id=\"she_her\"/>she/her"
msgstr "<void id=\"female\"/>delegada"

#: ../../english/intro/organization.data:24
#, fuzzy
#| msgid "<void id=\"female\"/>delegate"
msgid "<void id=\"gender_neutral\"/>delegate"
msgstr "<void id=\"female\"/>delegada"

#: ../../english/intro/organization.data:24
#: ../../english/intro/organization.data:27
#, fuzzy
#| msgid "<void id=\"female\"/>delegate"
msgid "<void id=\"they_them\"/>they/them"
msgstr "<void id=\"female\"/>delegada"

#: ../../english/intro/organization.data:30
#: ../../english/intro/organization.data:32
msgid "current"
msgstr "actual"

#: ../../english/intro/organization.data:34
#: ../../english/intro/organization.data:36
msgid "member"
msgstr "membro"

#: ../../english/intro/organization.data:39
msgid "manager"
msgstr "xestor"

#: ../../english/intro/organization.data:41
msgid "SRM"
msgstr "SRM"

#: ../../english/intro/organization.data:41
msgid "Stable Release Manager"
msgstr "Xestor da versión estable"

#: ../../english/intro/organization.data:43
msgid "wizard"
msgstr "mago"

#. we only use the chair tag once, for techctte, I wonder why it's here.
#: ../../english/intro/organization.data:45
#, fuzzy
msgid "chair"
msgstr "presidente"

#: ../../english/intro/organization.data:48
msgid "assistant"
msgstr "asistente"

#: ../../english/intro/organization.data:50
msgid "secretary"
msgstr "secretario"

#: ../../english/intro/organization.data:52
msgid "representative"
msgstr ""

#: ../../english/intro/organization.data:54
msgid "role"
msgstr ""

#: ../../english/intro/organization.data:62
msgid ""
"In the following list, <q>current</q> is used for positions that are\n"
"transitional (elected or appointed with a certain expiration date)."
msgstr ""

#: ../../english/intro/organization.data:70
#: ../../english/intro/organization.data:82
msgid "Officers"
msgstr "Dirixentes"

#: ../../english/intro/organization.data:71
#: ../../english/intro/organization.data:108
msgid "Distribution"
msgstr "Distribución"

#: ../../english/intro/organization.data:72
#: ../../english/intro/organization.data:239
msgid "Communication and Outreach"
msgstr ""

#: ../../english/intro/organization.data:74
#: ../../english/intro/organization.data:242
msgid "Data Protection team"
msgstr ""

#: ../../english/intro/organization.data:75
#: ../../english/intro/organization.data:246
#, fuzzy
msgid "Publicity team"
msgstr "Publicidade"

#: ../../english/intro/organization.data:77
#: ../../english/intro/organization.data:319
msgid "Membership in other organizations"
msgstr ""

#: ../../english/intro/organization.data:78
#: ../../english/intro/organization.data:347
msgid "Support and Infrastructure"
msgstr "Asistencia e infraestrutura"

#: ../../english/intro/organization.data:85
msgid "Leader"
msgstr "Líder"

#: ../../english/intro/organization.data:87
msgid "Technical Committee"
msgstr "Comité técnico"

#: ../../english/intro/organization.data:103
msgid "Secretary"
msgstr "Secretaría"

#: ../../english/intro/organization.data:111
msgid "Development Projects"
msgstr "Proxectos de desenvolvemento"

#: ../../english/intro/organization.data:112
msgid "FTP Archives"
msgstr "Arquivos FTP"

#: ../../english/intro/organization.data:114
msgid "FTP Masters"
msgstr "Xestores do FTP"

#: ../../english/intro/organization.data:120
msgid "FTP Assistants"
msgstr "Asistentes do FTP"

#: ../../english/intro/organization.data:126
msgid "FTP Wizards"
msgstr "Magos do FTP"

#: ../../english/intro/organization.data:130
msgid "Backports"
msgstr "Backports"

#: ../../english/intro/organization.data:132
msgid "Backports Team"
msgstr "Equipo de backports"

#: ../../english/intro/organization.data:136
msgid "Release Management"
msgstr "Xestión das publicacións"

#: ../../english/intro/organization.data:138
msgid "Release Team"
msgstr "Equipo de publicación"

#: ../../english/intro/organization.data:147
msgid "Quality Assurance"
msgstr "Garantía de calidade"

#: ../../english/intro/organization.data:148
msgid "Installation System Team"
msgstr "Equipo do sistema de instalación"

#: ../../english/intro/organization.data:149
msgid "Debian Live Team"
msgstr ""

#: ../../english/intro/organization.data:150
msgid "Release Notes"
msgstr "Notas da publicación"

#: ../../english/intro/organization.data:152
#, fuzzy
#| msgid "CD Images"
msgid "CD/DVD/USB Images"
msgstr "Imaxes de CD"

#: ../../english/intro/organization.data:154
msgid "Production"
msgstr "Produción"

#: ../../english/intro/organization.data:161
msgid "Testing"
msgstr "Probas"

#: ../../english/intro/organization.data:163
msgid "Cloud Team"
msgstr ""

#: ../../english/intro/organization.data:167
msgid "Autobuilding infrastructure"
msgstr "Infraestrutura de construción automática"

#: ../../english/intro/organization.data:169
msgid "Wanna-build team"
msgstr "Equipo de peticións de construción"

#: ../../english/intro/organization.data:176
msgid "Buildd administration"
msgstr "Administración de buildd"

#: ../../english/intro/organization.data:193
msgid "Documentation"
msgstr "Documentación"

#: ../../english/intro/organization.data:198
msgid "Work-Needing and Prospective Packages list"
msgstr "Lista de paquetes que precisan traballo e por crear"

#: ../../english/intro/organization.data:200
msgid "Ports"
msgstr "Adaptacións"

#: ../../english/intro/organization.data:230
msgid "Special Configurations"
msgstr "Configuracións especiais"

#: ../../english/intro/organization.data:232
msgid "Laptops"
msgstr "Portátiles"

#: ../../english/intro/organization.data:233
msgid "Firewalls"
msgstr "Devasas"

#: ../../english/intro/organization.data:234
msgid "Embedded systems"
msgstr "Sistemas embebidos"

#: ../../english/intro/organization.data:249
msgid "Press Contact"
msgstr "Contacto coa prensa"

#: ../../english/intro/organization.data:251
msgid "Web Pages"
msgstr "Páxinas web"

#: ../../english/intro/organization.data:263
msgid "Planet Debian"
msgstr "Planeta Debian"

#: ../../english/intro/organization.data:268
msgid "Outreach"
msgstr ""

#: ../../english/intro/organization.data:273
msgid "Debian Women Project"
msgstr "Proxecto muller Debian"

#: ../../english/intro/organization.data:281
msgid "Community"
msgstr ""

#: ../../english/intro/organization.data:290
msgid ""
"To send a private message to all the members of the Community Team, use the "
"GPG key <a href=\"community-team-pubkey.txt"
"\">817DAE61E2FE4CA28E1B7762A89C4D0527C4C869</a>."
msgstr ""

#: ../../english/intro/organization.data:292
msgid "Events"
msgstr "Eventos"

#: ../../english/intro/organization.data:299
#, fuzzy
msgid "DebConf Committee"
msgstr "Comité técnico"

#: ../../english/intro/organization.data:306
msgid "Partner Program"
msgstr "Programa de socios"

#: ../../english/intro/organization.data:310
msgid "Hardware Donations Coordination"
msgstr "Coordinador das doazóns de hardware"

#: ../../english/intro/organization.data:325
msgid "GNOME Foundation"
msgstr ""

#: ../../english/intro/organization.data:327
msgid "Linux Professional Institute"
msgstr ""

#: ../../english/intro/organization.data:329
msgid "Linux Magazine"
msgstr ""

#: ../../english/intro/organization.data:331
msgid "Linux Standards Base"
msgstr ""

#: ../../english/intro/organization.data:333
msgid "Free Standards Group"
msgstr ""

#: ../../english/intro/organization.data:334
msgid "SchoolForge"
msgstr ""

#: ../../english/intro/organization.data:337
msgid ""
"OASIS: Organization\n"
"      for the Advancement of Structured Information Standards"
msgstr ""

#: ../../english/intro/organization.data:340
msgid ""
"OVAL: Open Vulnerability\n"
"      Assessment Language"
msgstr ""

#: ../../english/intro/organization.data:343
msgid "Open Source Initiative"
msgstr ""

#: ../../english/intro/organization.data:350
msgid "User support"
msgstr "Asistencia ao usuario"

#: ../../english/intro/organization.data:417
msgid "Bug Tracking System"
msgstr "Sistema de seguimento dos informes de fallos"

#: ../../english/intro/organization.data:422
msgid "Mailing Lists Administration and Mailing List Archives"
msgstr "Administración das listas de correo o dos seus arquivos"

#: ../../english/intro/organization.data:431
msgid "New Members Front Desk"
msgstr "Recepción de novos membros"

#: ../../english/intro/organization.data:437
msgid "Debian Account Managers"
msgstr "Xestores de contas de Debian"

#: ../../english/intro/organization.data:441
msgid ""
"To send a private message to all DAMs, use the GPG key "
"57731224A9762EA155AB2A530CA8D15BB24D96F2."
msgstr ""

#: ../../english/intro/organization.data:442
msgid "Keyring Maintainers (PGP and GPG)"
msgstr "Mantedores dos chaveiros (PGP e GPG)"

#: ../../english/intro/organization.data:446
msgid "Security Team"
msgstr "Equipo de seguridade"

#: ../../english/intro/organization.data:457
msgid "Consultants Page"
msgstr "Páxina de asesores técnicos"

#: ../../english/intro/organization.data:462
msgid "CD Vendors Page"
msgstr "Páxina de vendedores de CDs"

#: ../../english/intro/organization.data:465
msgid "Policy"
msgstr "Políticas"

#: ../../english/intro/organization.data:468
msgid "System Administration"
msgstr "Administración de sistemas"

#: ../../english/intro/organization.data:469
msgid ""
"This is the address to use when encountering problems on one of Debian's "
"machines, including password problems or you need a package installed."
msgstr ""
"Este é o enderezo a usar cando se atopen problemas en algunha das máquinas "
"de Debian, incluídos problemas cos contrasinais ou se precisa que se instale "
"un paquete."

#: ../../english/intro/organization.data:479
msgid ""
"If you have hardware problems with Debian machines, please see <a href="
"\"https://db.debian.org/machines.cgi\">Debian Machines</a> page, it should "
"contain per-machine administrator information."
msgstr ""
"Se ten problemas co hardware de máquinas Debian, consulte a páxina <a href="
"\"https://db.debian.org/machines.cgi\">máquinas Debian</a>. Debería conter "
"información dos administradores de cada máquina."

#: ../../english/intro/organization.data:480
msgid "LDAP Developer Directory Administrator"
msgstr "Administrador do directorio LDAP de desenvolvedores"

#: ../../english/intro/organization.data:481
msgid "Mirrors"
msgstr "Réplicas"

#: ../../english/intro/organization.data:488
msgid "DNS Maintainer"
msgstr "Mantedor do DNS"

#: ../../english/intro/organization.data:489
msgid "Package Tracking System"
msgstr "Sistema de seguimento de paquetes"

#: ../../english/intro/organization.data:491
msgid "Treasurer"
msgstr ""

#: ../../english/intro/organization.data:498
#, fuzzy
msgid ""
"<a name=\"trademark\" href=\"m4_HOME/trademark\">Trademark</a> use requests"
msgstr "Peticións de uso da<a href=\"m4_HOME/trademark\">marca</a>"

#: ../../english/intro/organization.data:502
#, fuzzy
msgid "Salsa administrators"
msgstr "Administradores de alioth"

#~ msgid "Anti-harassment"
#~ msgstr "Anti-acoso"

#~ msgid "Debian Pure Blends"
#~ msgstr "Debian Pure Blends"

#~ msgid "Individual Packages"
#~ msgstr "Paquetes individuais"

#~ msgid "Debian for children from 1 to 99"
#~ msgstr "Debian para nenos de 1 a 99"

#~ msgid "Debian for medical practice and research"
#~ msgstr "Debian para práctica e investigación médica"

#~ msgid "Debian for education"
#~ msgstr "Debian para a educación"

#~ msgid "Debian in legal offices"
#~ msgstr "Debian en oficinas legais"

#~ msgid "Debian for people with disabilities"
#~ msgstr "Debian para persoas con discapacidades"

#~ msgid "Debian for science and related research"
#~ msgstr "Debian para investigación científica e relacionada"

#, fuzzy
#~| msgid "Debian for education"
#~ msgid "Debian for astronomy"
#~ msgstr "Debian para a educación"

#~ msgid "Live System Team"
#~ msgstr "Equipo do sistema Live"

#~ msgid "Auditor"
#~ msgstr "Auditor"

#~ msgid "Publicity"
#~ msgstr "Publicidade"

#~ msgid "Bits from Debian"
#~ msgstr "Novas breves de Debian"

#~ msgid "Debian Maintainer (DM) Keyring Maintainers"
#~ msgstr "Mantedores do chaveiro de mantedores de Debian (DM)"

#~ msgid "DebConf chairs"
#~ msgstr "Presidencias DebConf"

#~ msgid "current Debian Project Leader"
#~ msgstr "Actual líder do proxecto Debian"

#~ msgid "Security Audit Project"
#~ msgstr "Proxecto de auditoría de seguridade"

#~ msgid "Testing Security Team"
#~ msgstr "Equipo de seguridade en testing"

#~ msgid "Alioth administrators"
#~ msgstr "Administradores de alioth"
