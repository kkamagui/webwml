<define-tag pagetitle>Uppdaterad Debian 10; 10.4 utgiven</define-tag>
<define-tag release_date>2020-05-09</define-tag>
#use wml::debian::news
#use wml::debian::translation-check translation="6547bb7720bfba1c2481b95c44642a4a6d3df030" maintainer="Andreas Rönnquist"

<define-tag release>10</define-tag>
<define-tag codename>buster</define-tag>
<define-tag revision>10.4</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>Debianprojektet presenterar stolt sin fjärde uppdatering till dess
stabila utgåva Debian <release> (med kodnamnet <q><codename></q>).
Denna punktutgåva lägger huvudsakligen till rättningar för säkerhetsproblem,
tillsammans med ytterligare rättningar för allvarliga problem. Säkerhetsbulletiner
har redan publicerats separat och refereras när de finns tillgängliga.</p>

<p>Vänligen notera att punktutgåvan inte innebär en ny version av Debian
<release> utan endast uppdaterar några av de inkluderade paketen. Det behövs
inte kastas bort gamla media av <q><codename></q>. Efter installationen
kan paket uppgraderas till de aktuella versionerna genom att använda en uppdaterad
Debianspegling..</p>

<p>Dom som frekvent installerar uppdateringar från security.debian.org kommer inte att behöva
uppdatera många paket, och de flesta av sådana uppdateringar finns
inkluderade i punktutgåvan.</p>

<p>Nya installationsavbildningar kommer snart att finnas tillgängliga på dom vanliga platserna.</p>

<p>En uppgradering av en existerande installation till denna revision kan utföras genom att
peka pakethanteringssystemet på en av Debians många HTTP-speglingar.
En utförlig lista på speglingar finns på:</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>




<h2>Blandade felrättningar</h2>

<p>Denna uppdatering av den stabila utgåvan lägger till några viktiga felrättningar till följande paket:</p>

<table border=0>
<tr><th>Paket</th>               <th>Orsak</th></tr>
<correction apt-cacher-ng "Enforce secured call to the server in maintenance job triggering [CVE-2020-5202]; allow .zst compression for tarballs; increase size of the decompression line buffer for configuration file reading">
<correction backuppc "Pass the username to start-stop-daemon when reloading, preventing reload failures">
<correction base-files "Update for the point release">
<correction brltty "Reduce severity of log message to avoid generating too many messages when used with new Orca versions">
<correction checkstyle "Fix XML External Entity injection issue [CVE-2019-9658 CVE-2019-10782]">
<correction choose-mirror "Update included mirror list">
<correction clamav "New upstream release [CVE-2020-3123]">
<correction corosync "totemsrp: Reduce MTU to avoid generating oversized packets">
<correction corosync-qdevice "Fix service startup">
<correction csync2 "Fail HELLO command when SSL is required">
<correction cups "Fix heap buffer overflow [CVE-2020-3898] and <q>the `ippReadIO` function may under-read an extension field</q> [CVE-2019-8842]">
<correction dav4tbsync "New upstream release, restoring compatibility with newer Thunderbird versions">
<correction debian-edu-config "Add policy files for Firefox ESR and Thunderbird to fix the TLS/SSL setup">
<correction debian-installer "Update for the 4.19.0-9 kernel ABI">
<correction debian-installer-netboot-images "Rebuild against proposed-updates">
<correction debian-security-support "New upstream stable release; update status of several packages; use <q>runuser</q> rather than <q>su</q>">
<correction distro-info-data "Add Ubuntu 20.10, and likely end of support date for stretch">
<correction dojo "Fix improper regular expression usage [CVE-2019-10785]">
<correction dpdk "New upstream stable release">
<correction dtv-scan-tables "New upstream snapshot; add all current German DVB-T2 muxes and the Eutelsat-5-West-A satellite">
<correction eas4tbsync "New upstream release, restoring compatibility with newer Thunderbird versions">
<correction edk2 "Security fixes [CVE-2019-14558 CVE-2019-14559 CVE-2019-14563 CVE-2019-14575 CVE-2019-14586 CVE-2019-14587]">
<correction el-api "Fix stretch to buster upgrades that involve Tomcat 8">
<correction fex "Fix a potential security issue in fexsrv">
<correction filezilla "Fix untrusted search path vulnerability [CVE-2019-5429]">
<correction frr "Fix extended next hop capability">
<correction fuse "Remove outdated udevadm commands from post-install scripts; don't explicitly remove fuse.conf on purge">
<correction fuse3 "Remove outdated udevadm commands from post-install scripts; don't explicitly remove fuse.conf on purge; fix memory leak in fuse_session_new()">
<correction golang-github-prometheus-common "Extend validity of test certificates">
<correction gosa "Replace (un)serialize with json_encode/json_decode to mitigate PHP object injection [CVE-2019-14466]">
<correction hbci4java "Support EU directive on payment services (PSD2)">
<correction hibiscus "Support EU directive on payment services (PSD2)">
<correction iputils "Correct an issue in which ping would improperly exit with a failure code when there were untried addresses still available in the getaddrinfo() library call return value">
<correction ircd-hybrid "Use dhparam.pem to avoid crash on startup">
<correction jekyll "Allow use of ruby-i18n 0.x and 1.x">
<correction jsp-api "Fix stretch to buster upgrades that involve Tomcat 8">
<correction lemonldap-ng "Prevent unwanted access to administration endpoints [CVE-2019-19791]; fix the GrantSession plugin which could not prohibit logon when two factor authentication was used; fix arbitrary redirects with OIDC if redirect_uri was not used">
<correction libdatetime-timezone-perl "Update included data">
<correction libreoffice "Fix OpenGL slide transitions">
<correction libssh "Fix possible denial of service issue when handling AES-CTR keys with OpenSSL [CVE-2020-1730]">
<correction libvncserver "Fix heap overflow [CVE-2019-15690]">
<correction linux "New upstream stable release">
<correction linux-latest "Update kernel ABI to 4.19.0-9">
<correction linux-signed-amd64 "New upstream stable release">
<correction linux-signed-arm64 "New upstream stable release">
<correction linux-signed-i386 "New upstream stable release">
<correction lwip "Fix buffer overflow [CVE-2020-8597]">
<correction lxc-templates "New upstream stable release; handle languages that are only UTF-8 encoded">
<correction manila "Fix missing access permissions check [CVE-2020-9543]">
<correction megatools "Add support for the new format of mega.nz links">
<correction mew "Fix server SSL certificate validity checking">
<correction mew-beta "Fix server SSL certificate validity checking">
<correction mkvtoolnix "Rebuild to tighten libmatroska6v5 dependency">
<correction ncbi-blast+ "Disable SSE4.2 support">
<correction node-anymatch "Remove unnecessary dependencies">
<correction node-dot "Prevent code execution after prototype pollution [CVE-2020-8141]">
<correction node-dot-prop "Fix prototype pollution [CVE-2020-8116]">
<correction node-knockout "Fix escaping with older Internet Explorer versions [CVE-2019-14862]">
<correction node-mongodb "Reject invalid _bsontypes [CVE-2019-2391 CVE-2020-7610]">
<correction node-yargs-parser "Fix prototype pollution [CVE-2020-7608]">
<correction npm "Fix arbitrary path access [CVE-2019-16775 CVE-2019-16776 CVE-2019-16777]">
<correction nvidia-graphics-drivers "New upstream stable release">
<correction nvidia-graphics-drivers-legacy-390xx "New upstream stable release">
<correction nvidia-settings-legacy-340xx "New upstream release">
<correction oar "Revert to stretch behavior for Storable::dclone perl function, fixing recursion depth issues">
<correction opam "Prefer mccs over aspcud">
<correction openvswitch "Fix vswitchd abort when a port is added and the controller is down">
<correction orocos-kdl "Fix string conversion with Python 3">
<correction owfs "Remove broken Python 3 packages">
<correction pango1.0 "Fix crash in pango_fc_font_key_get_variations() when key is null">
<correction pgcli "Add missing dependency on python3-pkg-resources">
<correction php-horde-data "Fix authenticated remote code execution vulnerability [CVE-2020-8518]">
<correction php-horde-form "Fix authenticated remote code execution vulnerability [CVE-2020-8866]">
<correction php-horde-trean "Fix authenticated remote code execution vulnerability [CVE-2020-8865]">
<correction postfix "New upstream stable release; fix panic with Postfix multi-Milter configuration during MAIL FROM; fix d/init.d running change so it works with multi-instance again">
<correction proftpd-dfsg "Fix memory access issue in keyboard-interative code in mod_sftp; properly handle DEBUG, IGNORE, DISCONNECT, and UNIMPLEMENTED messages in keyboard-interactive mode">
<correction puma "Fix Denial of Service issue [CVE-2019-16770]">
<correction purple-discord "Fix crashes in ssl_nss_read">
<correction python-oslo.utils "Fix leak of sensitive information via mistral logs [CVE-2019-3866]">
<correction rails "Fix possible cross-site scripting via Javascript escape helper [CVE-2020-5267]">
<correction rake "Fix command injection vulnerability [CVE-2020-8130]">
<correction raspi3-firmware "Fix dtb names mismatch in z50-raspi-firmware; fix boot on Raspberry Pi families 1 and 0">
<correction resource-agents "Fix <q>ethmonitor does not list interfaces without assigned IP address</q>; remove no longer required xen-toolstack patch; fix non-standard usage in ZFS agent">
<correction rootskel "Disable multiple console support if preseeding is in use">
<correction ruby-i18n "Fix gemspec generation">
<correction rubygems-integration "Avoid deprecation warnings when users install a newer version of Rubygems via <q>gem update --system</q>">
<correction schleuder "Improve patch to handle encoding errors introduced in the previous version; switch default encoding to UTF-8; let x-add-key handle mails with attached, quoted-printable encoded keys; fix x-attach-listkey with mails created by Thunderbird that include protected headers">
<correction scilab "Fix library loading with OpenJDK 11.0.7">
<correction serverspec-runner "Support Ruby 2.5">
<correction softflowd "Fix broken flow aggregation which might result in flow table overflow and 100% CPU usage">
<correction speech-dispatcher "Fix default pulseaudio latency which triggers <q>scratchy</q> output">
<correction spl-linux "Fix deadlock">
<correction sssd "Fix sssd_be busy-looping when LDAP connection is intermittent">
<correction systemd "when authorizing via PolicyKit re-resolve callback/userdata instead of caching it [CVE-2020-1712]; install 60-block.rules in udev-udeb and initramfs-tools">
<correction taglib "Fix corruption issues with OGG files">
<correction tbsync "New upstream release, restoring compatibility with newer Thunderbird versions">
<correction timeshift "Fix predictable temporary directory use [CVE-2020-10174]">
<correction tinyproxy "Only set PIDDIR, if PIDFILE is a non-zero length string">
<correction tzdata "New upstream stable release">
<correction uim "unregister modules that are not installed, fixing a regression in the previous upload">
<correction user-mode-linux "Fix build failure with current stable kernels">
<correction vite "Fix crash when there are more than 32 elements">
<correction waagent "New upstream release; support co-installation with cloud-init">
<correction websocket-api "Fix stretch to buster upgrades that involve Tomcat 8">
<correction wpa "Do not try to detect PSK mismatch during PTK rekeying; check for FT support when selecting FT suites; fix MAC randomisation issue with some cards">
<correction xdg-utils "xdg-open: fix pcmanfm check and handling of directories with spaces in their names; xdg-screensaver: Sanitise window name before sending it over D-Bus; xdg-mime: Create config directory if it does not exist yet">
<correction xtrlock "Fix blocking of (some) multitouch devices while locked [CVE-2016-10894]">
<correction zfs-linux "Fix potential deadlock issues">
</table>


<h2>Säkerhetsuppdateringar</h2>


<p>Denna revision lägger till följande säkerhetsuppdateringar till den stabila utgåvan.
Säkerhetsgruppen har redan släppt bulletiner för alla dessa
uppdateringar:</p>

<table border=0>
<tr><th>Bulletin-ID</th>  <th>Paket</th></tr>
<dsa 2020 4616 qemu>
<dsa 2020 4617 qtbase-opensource-src>
<dsa 2020 4618 libexif>
<dsa 2020 4619 libxmlrpc3-java>
<dsa 2020 4620 firefox-esr>
<dsa 2020 4623 postgresql-11>
<dsa 2020 4624 evince>
<dsa 2020 4625 thunderbird>
<dsa 2020 4627 webkit2gtk>
<dsa 2020 4629 python-django>
<dsa 2020 4630 python-pysaml2>
<dsa 2020 4631 pillow>
<dsa 2020 4632 ppp>
<dsa 2020 4633 curl>
<dsa 2020 4634 opensmtpd>
<dsa 2020 4635 proftpd-dfsg>
<dsa 2020 4636 python-bleach>
<dsa 2020 4637 network-manager-ssh>
<dsa 2020 4638 chromium>
<dsa 2020 4639 firefox-esr>
<dsa 2020 4640 graphicsmagick>
<dsa 2020 4641 webkit2gtk>
<dsa 2020 4642 thunderbird>
<dsa 2020 4643 python-bleach>
<dsa 2020 4644 tor>
<dsa 2020 4645 chromium>
<dsa 2020 4646 icu>
<dsa 2020 4647 bluez>
<dsa 2020 4648 libpam-krb5>
<dsa 2020 4649 haproxy>
<dsa 2020 4650 qbittorrent>
<dsa 2020 4651 mediawiki>
<dsa 2020 4652 gnutls28>
<dsa 2020 4653 firefox-esr>
<dsa 2020 4654 chromium>
<dsa 2020 4655 firefox-esr>
<dsa 2020 4656 thunderbird>
<dsa 2020 4657 git>
<dsa 2020 4658 webkit2gtk>
<dsa 2020 4659 git>
<dsa 2020 4660 awl>
<dsa 2020 4661 openssl>
<dsa 2020 4663 python-reportlab>
<dsa 2020 4664 mailman>
<dsa 2020 4665 qemu>
<dsa 2020 4666 openldap>
<dsa 2020 4667 linux-signed-amd64>
<dsa 2020 4667 linux-signed-arm64>
<dsa 2020 4667 linux-signed-i386>
<dsa 2020 4667 linux>
<dsa 2020 4669 nodejs>
<dsa 2020 4671 vlc>
<dsa 2020 4672 trafficserver>
</table>


<h2>Borttagna paket</h2>

<p>Följande paket har tagits bort på grund av omständigheter utom vår kontroll:</p>

<table border=0>
<tr><th>Paket</th>               <th>Orsak</th></tr>
<correction getlive "Broken due to Hotmail changes">
<correction gplaycli "Broken by Google API changes">
<correction kerneloops "Upstream service no longer available">
<correction lambda-align2 "[arm64 armel armhf i386 mips64el ppc64el s390x] Broken on non-amd64 architectures">
<correction libmicrodns "Security issues">
<correction libperlspeak-perl "Security issues; unmaintained">
<correction quotecolors "Incompatible with newer Thunderbird versions">
<correction torbirdy "Incompatible with newer Thunderbird versions">
<correction ugene "Non-free; fails to build">
<correction yahoo2mbox "Broken for several years">

</table>

<h2>Debianinstalleraren</h2>
<p>Installeraren har uppdaterats för att inkludera rättningarna som har inkluderats i den
stabila utgåvan med denna punktutgåva.</p>

<h2>URLer</h2>

<p>Den fullständiga listan på paket som har förändrats i denna revision:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>Den aktuella stabila utgåvan:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/stable/">
</div>

<p>Föreslagna uppdateringar till den stabila utgåvan:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/proposed-updates">
</div>

<p>Information om den stabila utgåvan (versionsfakta, kända problem osv.):</p>

<div class="center">
  <a
  href="$(HOME)/releases/stable/">https://www.debian.org/releases/stable/</a>
</div>

<p>Säkerhetsbulletiner och information:</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>

<h2>Om Debian</h2>

<p>Debianprojektet är en grupp utvecklare av Fri mjukvara som
donerar sin tid och kraft för att producera det helt
fria operativsystemet Debian.</p>

<h2>Kontaktinformation</h2>

<p>För ytterligare information, vänligen besök Debians webbplats på
<a href="$(HOME)/">https://www.debian.org/</a>, skicka e-post till
&lt;press@debian.org&gt;, eller kontakta gruppen för stabila utgåvor på
&lt;debian-release@lists.debian.org&gt;.</p>


