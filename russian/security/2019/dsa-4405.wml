#use wml::debian::translation-check translation="da347ceee9cca800740ef75deed5e600ef8e2b1d" mindelta="1" maintainer="Lev Lamberov"
<define-tag description>обновление безопасности</define-tag>
<define-tag moreinfo>
<p>В openjpeg2, кодеке JPEG 2000 с открытым исходным кодом, были обнаружены
многочисленные уязвимости, которые могут использоваться для вызова отказа
в обслуживании или удалённого выполнения кода.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-17480">CVE-2017-17480</a>

    <p>Запись за пределы выделенного буфера памяти в кодеках jp3d и jpwl могут приводить
    к отказу в обслуживании или удалённому выполнению кода при обработке специально
    сформированных файлов jp3d или jpwl.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-5785">CVE-2018-5785</a>

    <p>Переполнение целых чисел может приводить к отказу в обслуживании при обработке
    специально сформированного файла bmp.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-6616">CVE-2018-6616</a>

    <p>Избыточная итерация может приводить к отказу в обслуживании при обработке
    специально сформированного файла bmp.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-14423">CVE-2018-14423</a>

    <p>Деление на ноль может приводить к отказу в обслуживании при обработке
    специально сформированного файла j2k.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-18088">CVE-2018-18088</a>

    <p>Разыменование null-указателя может приводить к отказу в обслуживании при обработке
    специально сформированного файла bmp.</p></li>

</ul>

<p>В стабильном выпуске (stretch) эти проблемы были исправлены в
версии 2.1.2-1.1+deb9u3.</p>

<p>Рекомендуется обновить пакеты openjpeg2.</p>

<p>С подробным статусом поддержки безопасности openjpeg2 можно ознакомиться на
соответствующей странице отслеживания безопасности по адресу
<a href="https://security-tracker.debian.org/tracker/openjpeg2">\
https://security-tracker.debian.org/tracker/openjpeg2</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2019/dsa-4405.data"
