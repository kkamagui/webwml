#use wml::debian::template title="Our philosophy: why we do it and how we do it"
#include "$(ENGLISHDIR)/releases/info"

# translators: some text is taken from /intro/about.wml

<ul class="toc">
<li><a href="#what">QU'EST Debian?</a>
<li><a href="#free">Est-ce complétement libre?</a>
<li><a href="#how">Comment la communauté fonctionne-t-elle?</a>
<li><a href="#history">Comment cela a-t-il commencé?</a>
</ul>

<h2><a name="what">QU'EST Debian?</a></h2>

<p>Le <a href="$(HOME)/">Projet Debian</a> est une association d'individus qui
ont fait cause commune pour créer un système d'exploitation <a href="free">libre</a>. 
Ce système d'exploitation s'appelle <strong>Debian</strong>.</p>

<p>Un système d'exploitation est l'ensemble des programmes et utilitaires de base qui 
font fonctionner votre ordinateur. Au cœur d'un système d'exploitation se trouve le noyau.
Le noyau est le programme le plus fondamental de l'ordinateur et fait tout le travail 
de base et vous permet de lancer d'autres programmes.</p>

<p>Debian utilise pour l'instant le noyau <a href="https://www.kernel.org/">Linux</a>
ou le noyau <a href="https://www.freebsd.org/">FreeBSD</a>. Linux est un programme
créé par <a href="https://en.wikipedia.org/wiki/Linus_Torvalds">Linus Torvalds</a>
et soutenu par des milliers de programmeurs dans le monde.
FreeBSD est un système d'exploitation comprenant un noyau et d'autres programmes.</p>

<p>Cependant, des travaux sont en cours pour fournir Debian pour d'autres noyaux, 
principalement <a href="https://www.gnu.org/software/hurd/hurd.html">Hurd</a>.
Hurd est un ensemble de serveurs qui fonctionnent au-dessus d'un micro-noyau (tel que Mach) 
pour implémenter différentes fonctionnalités.
Hurd est un logiciel libre produit par le <a href="https://www.gnu.org/">projet GNU</a>.</p>

<p>Une grande partie des outils de base qui complètent le système d'exploitation viennent
du projet <a href="https://www.gnu.org/">GNU</a> ; d'où les noms : 
GNU/Linux, GNU/kFreeBSD, et GNU/Hurd.
Ces outils sont également libres.</p>

<p>Bien sûr, ce que les gens veulent, ce sont des applications : des programmes
pour les aider à faire ce qu'ils veulent faire, de l'édition des documents à
de la gestion d'une entreprise aux jeux et à la création de nouveaux logiciels. 
Debian vient avec plus de <packages_in_stable> <a href="$(DISTRIB)/packages">paquets</a> 
(programme pré-compilé qui est livré dans un format adapaté pour une installation facile sur votre
machine), un gestionnaire de paquets (APT), et d'autres services qui permettent
de gérer des milliers de paquets sur des milliers d'ordinateurs aussi facilement que
l'installation d'une application unique. Tout cela de manière entièrement <a href="free">libre</a>.
</p>

<p>C'est un peu comme une tour. A la base, il y a le noyau.
Au-dessus, il y a tous les outils de base. Ensuite, il y a tous les logiciels que vous 
exécutez sur l'ordinateur. Au sommet de la tour se trouve Debian, organisant et 
adaptant le tout avec soin pour que cela fonctionne bien ensemble.

<h2>Est-ce totalement <a href="free" name="free">libre?</a></h2>

<p>Quand nous utilisons le mot "libre", nous faisons référence à la <strong>liberté</strong> logicielle. 
Vous pouvez en lire plus à ce propos sur
<a href="free">que voulons nous dire par "logiciel libre"</a> et sur
<a href="https://www.gnu.org/philosophy/free-sw">ce que la Free Software
Foundation dit à ce sujetsays</a>.</p>

<p>Vous vous demandez peut-être : pourquoi les gens passent-ils des heures de leur temps à écrire
des programmes, à les assembler soigneusement, pour finalement <EM>donner</EM> tout cela?
Les réponses sont aussi variées que les personnes qui y contribuent. Certaines personnes aiment 
aider les autres. Beaucoup écrivent des programmes pour en savoir plus sur les ordinateurs.
De plus en plus de personnes cherchent des moyens d'éviter le prix excessif des de logiciels.
Un nombre croissant de personnes contribuent en remerciement de tous les excellents logiciels libres 
qu'elles ont reçues d'autres personnes. De nombreux universitaires créent des logiciels libres pour 
aider à ce que les résultats de leurs recherches soient utilisés plus largement.
Les entreprises aident à maintenir les logiciels libres afin qu'elles puissent avoir leur mot à dire
sur leur développement -- il n'y a pas de moyen plus rapide pour obtenir une nouvelle fonctionnalité 
que de la développer soi-même! Bien sûr, beaucoup d'entre nous trouvent cela très amusant.</p>

<p>Debian est tellement engagé dans le logiciel libre que nous avons pensé qu'il était utile que
notre engagement soit formalisé dans un document écrit. C'est ainsi qu'est né notre 
<a href="$(HOME)/social_contract">Contrat Social</a>.</p>

<p>Bien que Debian croit au logiciel libre, il y a des cas où les gens veulent ou doivent
mettre des logiciels non libres sur leur machine. Chaque fois que cela est possible, Debian le supportera.
Il y a même un nombre croissant de paquets dont le seul travail est d'installer des logiciels non libres
dans un système Debian.</p>

<h2><a name="how">Comment la communauté fonctionne-t-elle?</a></h2>

<p>Debian est produit par presque 1000 développeurs actifs répartis
<a href="$(DEVEL)/developers.loc">dans le monde entier</a>, et donnant de leur
temps volontairement.
Peu de ces développeurs se sont jamais rencontrés en personne.
La communication a donc principalement lieu via emails (les mailing lists de
lists.debian.org) et IRC (canal #debian sur irc.debian.org).
</p>

<p>Le projet Debian a une <a href="organization">structure organisationnelle
</a> bien étudiée. Pour plus d'information sur la manière dont fonctionne Debian
de l'intérieur, veuillez vous référer au <a href="$(DEVEL)/">coin des développeurs</a>.</p>

<p>
Les documents principaux expliquant comment la communauté fonctionne sont les suivant:
<ul>
<li><a href="$(DEVEL)/constitution">La Constitution de Debian</li>
<li><a href="../social_contract">Le Contrat Social et les Lignes Directrices du Logiciel Libre</li>
<li><a href="diversity">La Déclaration sur la diversité</li>
<li><a href="../code_of_conduct">le Code de conduite</li>
<li><a href="../doc/developers-reference/">La Référence du développeur Debian</li>
<li><a href="../doc/debian-policy/">La Charte de Debian</li>
</ul>

<h2><a name="history">Comment cela a-t-il commencé?</a></h2>

<p>Debian a été créé en août 1993 par Ian Murdock, comme une nouvelle distribution
qui serait produite de manière ouverte, dans l'esprit de Linux et GNU. Debian devait être
créé avec soin, et consiencieusement, et entretenu et soutenu avec un pareil soin.
Au départ, il s'agissait d'un petit groupe très soudé de hackers de logiciels libres, pour
ensuite devenir progressivement une grande communauté bien organisée de développeurs et
d'utilisateurs. Veuillez vous référer à <a href="$(DOC)/manuals/project-history/">l'histoire détaillée du projet</a>
pour plus de détails.</p>

<p>Comme beaucoup de personnes ont posé la question, Debian se prononce /&#712;de.bi.&#601;n/. Ce
nom vient des prénoms du créateur de Debian, Ian Murdock, et de sa femme, Debra.</p>
