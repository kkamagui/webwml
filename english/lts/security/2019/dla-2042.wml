<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>It was discovered that there was a potential account hijack
vulnerabilility in Django, the Python-based web development
framework.</p>

<p>Django's password-reset form used a case-insensitive query to
retrieve accounts matching the email address requesting the password
reset. Because this typically involves explicit or implicit case
transformations, an attacker who knew the email address associated
with a user account could craft an email address which is distinct
from the address associated with that account, but which -- due to
the behavior of Unicode case transformations -- ceases to be distinct
after case transformation, or which will otherwise compare equal
given database case-transformation or collation behavior. In such a
situation, the attacker can receive a valid password-reset token for
the user account.</p>

<p>To resolve this, two changes were made in Django:</p>

<ul>
  <li>After retrieving a list of potentially-matching accounts from the database,
  Django's password reset functionality now also checks the email address for
  equivalence in Python, using the recommended identifier-comparison process from
  Unicode Technical Report 36, section 2.11.2(B)(2).</li>

  <li>When generating password-reset emails, Django now sends to the email
  address retrieved from the database, rather than the email address submitted in
  the password-reset request form.</li>
</ul>

<p>For more information, please see:
<a href="https://www.djangoproject.com/weblog/2019/dec/18/security-releases/">https://www.djangoproject.com/weblog/2019/dec/18/security-releases/</a>.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-19844">CVE-2019-19844</a>

    <p>Potential account hijack via password reset form</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
1.7.11-1+deb8u8.</p>

<p>We recommend that you upgrade your python-django packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-2042.data"
# $Id: $
