<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>It was discovered that GLib does not properly restrict some file
permissions while a copy operation is in progress; instead, default
permissions are used.</p>

<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
2.42.1-1+deb8u1.</p>

<p>We recommend that you upgrade your glib2.0 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1826.data"
# $Id: $
