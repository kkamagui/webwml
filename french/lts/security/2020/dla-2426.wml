#use wml::debian::translation-check translation="83fc930dd70c52c7fded77f6690ede83d7e23294" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Dans junit4, la règle de test TemporaryFolder contient une vulnérabilité
locale de divulgation d'informations. Dans les systèmes Unix, le répertoire
temporaire du système est partagé par tous ses utilisateurs. À cause de cela,
lorsque des fichiers et des répertoires y sont écrits, ils sont par
défaut lisibles par tous les utilisateurs du même système. Cette vulnérabilité
ne permet pas d’écraser le contenu de ces répertoires ou de ces fichiers. Il
s’agit strictement d’une vulnérabilité de divulgation d'informations. Cette
vulnérabilité impacte si les tests JUnit écrivent des informations sensibles,
telles que des clés ou des mots de passe d’API, dans le répertoire temporaire,
et que les tests JUnit sont exécutés dans un environnement où le système
d’exploitation à des utilisateurs non fiables.</p>

<p>Pour Debian 9 <q>Stretch</q>, ce problème a été corrigé dans
la version 4.12-4+deb9u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets junit4.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de junit4, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/junit4">https://security-tracker.debian.org/tracker/junit4</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2426.data"
# $Id: $
