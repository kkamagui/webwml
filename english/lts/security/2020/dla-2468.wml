<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>An issue has been found in tcpflow, a TCP flow recorder.</p>

<p>Due to an overflow vulnerability in function handle_80211, an
out-of-bounds read with access to sensitive memory or a denial of service
might happen.</p>

<p>For Debian 9 stretch, this problem has been fixed in version
1.4.4+repack1-3+deb8u1.</p>

<p>We recommend that you upgrade your tcpflow packages.</p>

<p>For the detailed security status of tcpflow please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/tcpflow">https://security-tracker.debian.org/tracker/tcpflow</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2468.data"
# $Id: $
