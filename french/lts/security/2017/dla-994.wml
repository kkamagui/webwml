#use wml::debian::translation-check translation="16a228d71674819599fa1d0027d1603056286470" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-5974">CVE-2017-5974</a>

<p>Un dépassement de tampon basé sur le tas dans la fonction __zzip_get32 dans
fetch.c dans zziplib permet à des attaquants distants de provoquer un déni de
service (plantage) à l'aide d'un fichier ZIP contrefait.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-5975">CVE-2017-5975</a>

<p>Un dépassement de tampon basé sur le tas dans la fonction __zzip_get64 dans
fetch.c dans zziplib permet à des attaquants distants de provoquer un déni de
service (plantage) à l'aide d'un fichier ZIP contrefait.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-5976">CVE-2017-5976</a>

<p>Un dépassement de tampon basé sur le tas dans la fonction
zzip_mem_entry_extra_block dans memdisk.c dans zziplib permet à des attaquants
distants de provoquer un déni de service (plantage) à l'aide d'un fichier ZIP
contrefait.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-5978">CVE-2017-5978</a>

<p>La fonction zzip_mem_entry_new dans memdisk.c dans zziplib permet à des
attaquants distants de provoquer un déni de service (lecture hors limites et
plantage) à l'aide d'un fichier ZIP contrefait.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-5979">CVE-2017-5979</a>

<p>La fonction prescan_entry dans fseeko.c dans zziplib permet à des attaquants
distants de provoquer un déni de service (déréférencement de pointeur NULL
et plantage) à l'aide d'un fichier ZIP contrefait.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-5980">CVE-2017-5980</a>

<p>La fonction zzip_mem_entry_new dans memdisk.c dans zziplib permet à des
attaquants distants de provoquer un déni de service (déréférencement de pointeur
NULL et plantage) à l'aide d'un fichier ZIP contrefait.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-5981">CVE-2017-5981</a>

<p>La fonction seeko.c dans zziplib permet à des attaquants distants de
provoquer un déni de service (échec d’assertion et plantage) à l'aide d'un
fichier ZIP contrefait.</p></li>

</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans
la version 0.13.56-1.1+deb7u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets zziplib.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-994.data"
# $Id: $
