#use wml::debian::template title="우리에게 연락하기" NOCOMMENTS="yes"
#use wml::debian::translation-check translation="9a850e710e07ea9a4bc633da3c0e1a5f550cd42a" maintainer="Seunghun Han (kkamagui)"

<p>데비안은 큰 조직이며 데비안에 연락할 방법은 많습니다.
이 페이지는 자주 요청받는 연락 방법을 요약합니다. 즉, 결코 완전하지 않다는
의미입니다.
다른 연락 방법은 웹 페이지의 나머지 부분을 참조하세요.
</p>

<p>아래 이메일 주소는 대부분 공개 보관소가 있는 열린 메일링 리스트입니다.
<a href="$(HOME)/MailingLists/disclaimer">면책조항</a>을
먼저 읽어보고 메시지를 보내세요.
</p>

<p>영어는 데비안 개발자와 소통하는 공통 언어입니다. 그러므로 개발자들에게
최초 문의를 할 때 <strong>영어</strong>로 하길 부탁드립니다.
그럴 수 없다면
<a href="https://lists.debian.org/users.html#debian-user">여러분 언어를 위한
사용자 메일링 리스트</a>를 통하세요.
</p>

<ul class="toc">
  <li><a href="#generalinfo">일반 정보</a>
  <li><a href="#installuse">데비안 설치 및 사용</a>
  <li><a href="#press">홍보 / 보도</a>
  <li><a href="#events">행사 / 컨퍼런스</a>
  <li><a href="#helping">데비안 돕기</a>
  <li><a href="#packageproblems">데비안 패키지 문제 보고하기</a>
  <li><a href="#development">데비안 개발</a>
  <li><a href="#infrastructure">데비안 시설 관련 문제</a>
  <li><a href="#harassment">학대 이슈</a>
</ul>

<h2 id="generalinfo">일반 정보</h2>

<p>데비안과 관련된 대부분의 정보는
우리 웹 사이트<a href="$(HOME)">https://www.debian.org/</a>에 있으니,
먼저 훑어 보고 <a href="$(SEARCH)">검색</a>해 본 후 우리에게 연락하세요.

<p>우리의 <a href="doc/user-manuals#faq/">FAQ</a>에는 여러분이 많이 한 질문에
대한 답변이 있습니다.

<p>데비안 프로젝트 관련 일반적인 질문은
<em>debian-project</em> 메일링 리스트<email debian-project@lists.debian.org>에
보내주세요. 그 리스트에 리눅스 사용에 대해 질문 보내지 마세요. 이와 관련해서는
아래를 보세요.

<h2 id="installuse">데비안 설치 및 사용</h2>

<p>여러분 문제의 해결책이 설치 매체와 우리 웹 사이트에 있는 설치 문서에 확실하게
없다면, 데비안 사용자와 개발자가 여러분의 질문에 답을 해주는 매우 활동적인
<em>debian-user</em> 메일링 리스트가 있습니다.</p>

<ul>
  <li>설치
  <li>구성
  <li>지원되는 하드웨어
  <li>컴퓨터 관리
  <li>데비안 사용
</ul>
관련 질문은 모두 그 리스트에 보내야 합니다.
그냥 <a href="https://lists.debian.org/debian-user/"><em>debian-user</em>를
구독하고 </a> 여러분의 질문을 <email debian-user@lists.debian.org>에 보내세요.

<p>추가적으로, 다양한 언어를 사용하는 사람들을 위한 사용자 메일링 리스트도
있습니다. <a href="https://lists.debian.org/users.html#debian-user">국제 메일링
리스트 구독 정보</a>를 보세요.

<p>게다가, 웹 인터페이스를 써서 메일링 리스트를 뉴스 그룹처럼, 즉
<a href="http://groups.google.com/d/homeredir">구글 그룹</a>처럼 살펴볼 수도 있습니다.

<p>여러분이 우리 설치 시스템에서 버그를 찾았다고 생각하면, 해당 정보를
<email debian-boot@lists.debian.org>에 보내거나
<a href="https://bugs.debian.org/debian-installer">데비안 설치 관리자</a>의
의사 패키지(pseudo-package)를 통해
<a href="$(HOME)/releases/stable/i386/ch05s04#submit-bug">버그를 보고</a>하세요.

<h2 id="press">홍보 / 보도</h2>

<p>기사나 우리의 뉴스 페이지에 뉴스를 보내기 위해 정보를 요청할 때는 
<a href="mailto:press@debian.org">홍보 부서</a>에 연락하세요.</p>

<h2 id="events">행사 / 컨퍼런스</h2>

<p><a href="$(HOME)/events/">컨퍼런스</a>나 전시회, 그외 다른 종류의 행사의
초대장은 <a href="mailto:events@debian.org">행사 부서</a>에 연락하세요.

유럽 지역의 전단지, 포스터, 참가 요청은 유럽
<a href="mailto:debian-events-eu@lists.debian.org">행사 부서</a>에 연락하세요.</p>

<h2 id="helping">데비안 돕기</h2>

<p>여러분이 데비안에 연락해서 도움을 주고 싶으면, 우선
<a href="devel/join/">도울 수 있는 방법</a>을 먼저 보세요.

<p>데비안 미러 서버를 관리하고 싶다면,
<a href="mirror/">데비안 미러링(mirroring)</a> 페이지를 보세요.새로운 미러 서버는
<a href="mirror/submit">미러 서버 양식</a>을 이용해서 제출해야 합니다.
기존 미러 서버의 문제는 <email mirrors@debian.org>에 보고할 수 있습니다.

<p>데비안 CD를 팔고 싶다면 <a href="CD/vendors/info"> CD 판매자를 위한 정보</a>를
보세요.
CD 판매자 목록에 등록되고 싶다면,
<a href="CD/vendors/adding-form">CD 판매자 양식</a>을 사용해서 그렇게 하세요. 

<h2 id="packageproblems">데비안 패키지 문제 보고하기</h2>

<p>데비안 패키지의 문제를 보고하고 싶다면, 여러분이 쉽게 문제를 보고 할 수 있는
버그 추적 시스템이 있습니다.
<a href="Bugs/Reporting">문제 보고를 위한 절차</a>를 보세요.

<p>단지 데비안 패키지 메인테이너와 이야기하길 바란다면, 각 패키지를 위해 설정된
특별한 메일 주소를 사용할 수 있습니다.
&lt;<var>패키지 이름</var>&gt;@packages.debian.org로 보내진 이메일은 해당 패키지를
관리하는 메인테이너에게 전달됩니다.
<p>데비안 보안 문제를 개발자에게 신중한 방식으로 알려주고 싶다면,
<email security@debian.org>로 이메일을 보내세요.

<h2 id="development">데비안 개발</h2>

<p>여러분이 개발과 관련된 질문을 하고 싶다면, 우리 개발자와 연락할 수 있는 몇몇
<a href="https://lists.debian.org/devel.html">개발 메일링 리스트</a>가 있습니다.

<p>일반적인 개발 메일링 리스트는 <em>debian-devel</em>입니다. 여러분은 이 메일링
리스트를 <a href="https://lists.debian.org/debian-devel/">구독</a>할 수 있고,
<email debian-devel@lists.debian.org>로 이메일을 보낼 수 있습니다.

<h2 id="infrastructure">데비안 시설 관련 문제</h2>

<p>데비안 서비스에 대한 문제점을 보고하려면, 보통 적절한
<a href="Bugs/pseudo-packages">의사 패키지(pseudo-package)</a>를 선택하고
이에 대한 <a href="Bugs/Reporting">버그를 보고</a>합니다.

<p>다른 방법으로, 이메일을 써서 그들과 연락할 수 있습니다.

<define-tag btsurl>package: <a href="https://bugs.debian.org/%0">%0</a></define-tag>

<dl>
<dt>웹 페이지 편집</dt>
  <dd><btsurl www.debian.org><br />
      <email debian-www@lists.debian.org></dd>
#include "$(ENGLISHDIR)/devel/website/tc.data"
<ifneq "$(CUR_LANG)" "English" "
<dt>웹 페이지 번역</dt>
  <dd><: &list_translators($CUR_LANG); :></dd>
">
<dt>메일링 리스트 관리자 및 아카이브 메인테이너</dt>
  <dd><btsurl lists.debian.org><br />
      <email listmaster@lists.debian.org></dd>
<dt>버그 추적 시스템 관리자</dt>
  <dd><btsurl bugs.debian.org><br /> 
      <email owner@bugs.debian.org></dd>
</dl>

<h2 id="harassment">괴롭힘 이슈</h2>

<p>데비안은 존중과 대화에 가치를 둔 사람들의 공동체입니다.
여러분이 데비안 프로젝트가 주최한 컨퍼런스나 스프린트 중, 또는 일반 프로젝트
상호작용 중에 어떤 행동으로 해를 입은 피해자이거나 괴롭힘을 당했다고 느낀다면,
커뮤니티 팀 <email community@debian.org>에 연락하세요.</p>

<p>우리는 조직의 다양한 부서와 연락하기 위해, 다양한 <a href="intro/organization">
직무와 사용 가능한 이메일</a>의 전체 목록을 갖고 있습니다.</p>
