#use wml::debian::translation-check translation="5c427e44dc4a1503d5262b5edfba60b490ea0ab1" maintainer="Jean-Pierre Giraud"
<define-tag pagetitle>Publication de la mise à jour de Debian 10.7</define-tag>
<define-tag release_date>2020-12-05</define-tag>
#use wml::debian::news
# $Id:

<define-tag release>10</define-tag>
<define-tag codename>Buster</define-tag>
<define-tag revision>10.7</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>
Le projet Debian a l'honneur d'annoncer la septième mise à jour de sa
distribution stable Debian <release> (nom de code <q><codename></q>).
Tout en réglant quelques problèmes importants, cette mise à jour corrige
principalement des problèmes de sécurité de la version stable. Les annonces de
sécurité ont déjà été publiées séparément et sont simplement référencées dans
ce document.
</p>

<p>
Veuillez noter que cette mise à jour ne constitue pas une nouvelle version
de Debian <release> mais seulement une mise à jour de certains des paquets
qu'elle contient. Il n'est pas nécessaire de jeter les anciens médias de
la version <codename>. Après installation, les paquets peuvent être mis
à niveau vers les versions courantes en utilisant un miroir Debian à jour.
</p>

<p>
Ceux qui installent fréquemment les mises à jour à partir de
security.debian.org n'auront pas beaucoup de paquets à mettre à jour et la
plupart des mises à jour de security.debian.org sont comprises dans cette
mise à jour.
</p>

<p>
De nouvelles images d'installation seront prochainement disponibles à leurs
emplacements habituels.
</p>

<p>
Mettre à jour une installation vers cette révision peut se faire en faisant
pointer le système de gestion de paquets sur l'un des nombreux miroirs HTTP
de Debian. Une liste complète des miroirs est disponible à l'adresse :
</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>


<h2>Corrections de bogues divers</h2>

<p>
Cette mise à jour de la version stable apporte quelques corrections
importantes aux paquets suivants :
</p>

<table border=0>
<tr><th>Paquet</th>               <th>Raison</th></tr>
<correction base-files "Mise à jour pour cette version">
<correction choose-mirror "Mise à jour de la liste des miroirs">
<correction cups "Correction de libération non valable de « printer-alert »">
<correction dav4tbsync "Nouvelle version amont compatible avec les dernières versions de Thunderbird">
<correction debian-installer "Utilisation de l'ABI du noyau Linux 4.19.0-13 ; ajout de grub2 à Built-Using">
<correction debian-installer-netboot-images "Reconstruction avec proposed-updates">
<correction distro-info-data "Ajout d'Ubuntu 21.04, Hirsute Hippo">
<correction dpdk "Nouvelle version amont stable ; correction d'un problème d'exécution de code à distance [CVE-2020-14374], de problèmes de type TOCTOU [CVE-2020-14375], d'un dépassement de tampon [CVE-2020-14376], de lecture hors limites de tampon [CVE-2020-14377] et de dépassement d'entier par le bas [CVE-2020-14377] ; correction de la construction d'armhf avec NEON">
<correction eas4tbsync "Nouvelle version amont compatible avec les dernières versions de Thunderbird">
<correction edk2 "Correction de dépassement d'entier dans DxeImageVerificationHandler [CVE-2019-14562]">
<correction efivar "Ajout de la prise en charge des périphériques nvme-fabrics et nvme-subsystem ; correction de variable non initialisée dans parse_acpi_root évitant une possible erreur de segmentation">
<correction enigmail "Introduction de l'assistant de migration vers la prise en charge intégrée de GPG par Thunderbird">
<correction espeak "Correction d'utilisation d'espeak avec mbrola-fr4 quand mbrola-fr1 n'est pas installé">
<correction fastd "Correction d'une fuite de mémoire lors de la réception de paquets non valables trop nombreux [CVE-2020-27638]">
<correction fish "Assurance de la restauration des options de TTY en quittant">
<correction freecol "Correction d'une vulnérabilité d'entité externe XML [CVE-2018-1000825]">
<correction gajim-omemo "Utilisation d'IV de 12 octets, pour une meilleure compatibilité avec les clients iOS">
<correction glances "Seulement à l'écoute sur localhost par défaut">
<correction iptables-persistent "Plus de chargement forcé des modules du noyau ; amélioration de la logique de vidage de règle">
<correction lacme "Utilisation d'une chaîne de certificats amont à la place d'une chaîne codée en dur pour faciliter la prise en charge d'une nouvelle racine Let's Encrypt et des certificats intermédiaires">
<correction libdatetime-timezone-perl "Mise à jour des données incluses dans tzdata 2020d">
<correction libimobiledevice "Ajout de la prise en charge partielle d'iOS 14">
<correction libjpeg-turbo "Correction de déni de service [CVE-2018-1152], de lecture de tampon hors limites [CVE-2018-14498], d'exécution potentielle de code à distance [CVE-2019-2201], de lecture de tampon hors limites [CVE-2020-13790]">
<correction libxml2 "Correction de déni de service [CVE-2017-18258], de déréférencement de pointeur NULL [CVE-2018-14404], de boucle infinie [CVE-2018-14567], de fuite de mémoire [CVE-2019-19956 CVE-2019-20388], de boucle infinie [CVE-2020-7595]">
<correction linux "Nouvelle version amont stable">
<correction linux-latest "Mise à jour de l'ABI du noyau Linux à la version 4.19.0-13">
<correction linux-signed-amd64 "Nouvelle version amont stable">
<correction linux-signed-arm64 "Nouvelle version amont stable">
<correction linux-signed-i386 "Nouvelle version amont stable">
<correction lmod "Changement d'architecture à <q>any</q> requis parce que LUA_PATH et LUA_CPATH sont déterminés au moment de la construction">
<correction mariadb-10.3 "Nouvelle version amont stable ; corrections de sécurité [CVE-2020-14765 CVE-2020-14776 CVE-2020-14789 CVE-2020-14812 CVE-2020-28912]">
<correction mutt "Fermeture de connexion IMAP assurée après une erreur de connexion [CVE-2020-28896]">
<correction neomutt "Fermeture de connexion IMAP assurée après une erreur de connexion [CVE-2020-28896]">
<correction node-object-path "Correction de pollution de prototype dans set() [CVE-2020-15256]">
<correction node-pathval "Correction de pollution de prototype [CVE-2020-7751]">
<correction okular "Correction d'exécution de code à l'aide d'un lien d'action [CVE-2020-9359]">
<correction openjdk-11 "Nouvelle version amont ; correction de plantage de JVM">
<correction partman-auto "Augmentation de la taille de /boot dans la plupart des recettes à entre 512 et 768 Mo pour mieux gérer les changements d'ABI du noyau et des initramfs plus grands ; limitation de la taille de RAM comme utilisée pour le calcul des partitions d'échange, résolvant les problèmes sur les machines où la RAM est plus grande que l'espace disque">
<correction pcaudiolib "Limitation de la latence d'annulation à 10 ms">
<correction plinth "Apache : désactivation de mod_status [CVE-2020-25073]">
<correction puma "Correction de problèmes d'injection HTTP et de dissimulation d'HTTP [CVE-2020-5247 CVE-2020-5249 CVE-2020-11076 CVE-2020-11077]">
<correction ros-ros-comm "Correction de dépassement d'entier [CVE-2020-16124]">
<correction ruby2.5 "Correction d'une potentielle vulnérabilité de dissimulation de requête HTTP dans WEBrick [CVE-2020-25613]">
<correction sleuthkit "Correction de dépassement de pile dans yaffsfs_istat [CVE-2020-10232]">
<correction sqlite3 "Correction de division par zéro [CVE-2019-16168], de déréférencement de pointeur NULL [CVE-2019-19923], de mauvais traitement du nom de chemin NULL durant la mise à jour d'une archive ZIP [CVE-2019-19925], de mauvais traitement de NULL incorporé à des noms de fichiers [CVE-2019-19959], d'un possible plantage (vidage de la pile WITH) [CVE-2019-20218], d'un dépassement d'entier [CVE-2020-13434], d'une erreur de segmentation [CVE-2020-13435], d'un problème d'utilisation de mémoire après libération [CVE-2020-13630], de déréférencement de pointeur NULL [CVE-2020-13632], de dépassement de tas [CVE-2020-15358]">
<correction systemd "Basic/cap-list : analyse et affichage des capacités numériques ; reconnaissance de nouvelles capacités du noyau Linux 5.8 ; networkd : plus de création de MAC pour les périphériques pont">
<correction tbsync "Nouvelle version amont, compatible avec les dernières versions de Thunderbird">
<correction tcpdump "Correction d'un problème d'entrées non fiables dans l'afficheur PPP [CVE-2020-8037]">
<correction tigervnc "Stockage correct des exceptions de certificat dans le visualiseur VNC natif et Java [CVE-2020-26117]">
<correction tor "Nouvelle version amont stable ; multiples corrections de sécurité, d'ergonomie, de portabilité et de fiabilité">
<correction transmission "Correction de fuite de mémoire">
<correction tzdata "Nouvelle version amont">
<correction ublock-origin "Nouvelle version amont ; greffon fractionné en paquets spécifiques aux navigateurs">
<correction vips "Correction d'utilisation de variable non initialisée [CVE-2020-20739]">
</table>


<h2>Mises à jour de sécurité</h2>


<p>
Cette révision ajoute les mises à jour de sécurité suivantes à la version
stable. L'équipe de sécurité a déjà publié une annonce pour chacune de ces
mises à jour :
</p>

<table border=0>
<tr><th>Identifiant</th>  <th>Paquet</th></tr>
<dsa 2020 4766 rails>
<dsa 2020 4767 mediawiki>
<dsa 2020 4768 firefox-esr>
<dsa 2020 4769 xen>
<dsa 2020 4770 thunderbird>
<dsa 2020 4771 spice>
<dsa 2020 4772 httpcomponents-client>
<dsa 2020 4773 yaws>
<dsa 2020 4774 linux-latest>
<dsa 2020 4774 linux-signed-amd64>
<dsa 2020 4774 linux-signed-arm64>
<dsa 2020 4774 linux-signed-i386>
<dsa 2020 4774 linux>
<dsa 2020 4775 python-flask-cors>
<dsa 2020 4776 mariadb-10.3>
<dsa 2020 4777 freetype>
<dsa 2020 4778 firefox-esr>
<dsa 2020 4779 openjdk-11>
<dsa 2020 4780 thunderbird>
<dsa 2020 4781 blueman>
<dsa 2020 4782 openldap>
<dsa 2020 4783 sddm>
<dsa 2020 4784 wordpress>
<dsa 2020 4785 raptor2>
<dsa 2020 4786 libexif>
<dsa 2020 4787 moin>
<dsa 2020 4788 firefox-esr>
<dsa 2020 4789 codemirror-js>
<dsa 2020 4790 thunderbird>
<dsa 2020 4791 pacemaker>
<dsa 2020 4792 openldap>
<dsa 2020 4793 firefox-esr>
<dsa 2020 4794 mupdf>
<dsa 2020 4795 krb5>
<dsa 2020 4796 thunderbird>
<dsa 2020 4798 spip>
<dsa 2020 4799 x11vnc>
<dsa 2020 4800 libproxy>
</table>


<h2>Paquets supprimés</h2>

<p>Les paquets suivants ont été supprimés à cause de circonstances hors de
notre contrôle :</p>

<table border=0>
<tr><th>Paquet</th>               <th>Raison</th></tr>
<correction freshplayerplugin "Plus pris en charge par les navigateurs ; arrêté par l'amont">
<correction nostalgy "Incompatible avec les dernières versions de Thunderbird">
<correction sieve-extension "Incompatible avec les dernières versions de Thunderbird">

</table>

<h2>Installateur Debian</h2>
<p>
L'installateur a été mis à jour pour inclure les correctifs incorporés
dans cette version de stable.
</p>

<h2>URL</h2>

<p>
Liste complète des paquets qui ont été modifiés dans cette version :
</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>
Adresse de l'actuelle distribution stable :
</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/stable/">
</div>

<p>
Mises à jour proposées à la distribution stable :
</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/proposed-updates">
</div>

<p>
Informations sur la distribution stable (notes de publication, <i>errata</i>, etc.) :
</p>

<div class="center">
  <a
  href="$(HOME)/releases/stable/">https://www.debian.org/releases/stable/</a>
</div>

<p>
Annonces et informations de sécurité :
</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>

<h2>À propos de Debian</h2>
<p>
Le projet Debian est une association de développeurs de logiciels libres
qui offrent volontairement leur temps et leurs efforts pour produire le
système d'exploitation complètement libre Debian.
</p>

<h2>Contacts</h2>

<p>
Pour de plus amples informations, veuillez consulter le site Internet de
Debian <a href="$(HOME)/">https://www.debian.org/</a> ou envoyez un courrier
électronique à &lt;press@debian.org&gt; ou contactez l'équipe de
publication de la version stable à &lt;debian-release@lists.debian.org&gt;.
</p>
