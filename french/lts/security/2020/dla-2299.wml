#use wml::debian::translation-check translation="6f150325ad689f53c910f5264462e110d2cbeec5" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Une vulnérabilité d’élévation des privilèges a été découverte dans
<a href="http://net-snmp.sourceforge.net/">Net-SNMP</a>, un ensemble d’outils
pour collecter et organiser l’information à propos des périphériques sur des
réseaux d’ordinateurs.</p>

<p>L’amont signale que :</p>

<ul>
<li>il est toujours possible d’activer cette base d’information pour la gestion
du réseau (MIB) à l’aide de l’option de configuration
<tt>--with-mib-modules</tt> ;</li>

<li>une autre MIB procurant la même fonction, nommément
<tt>ucd-snmp/extensible</tt>, est désactivée par défaut ;</li>

<li>le risque de sécurité de <tt>ucd-snmp/pass</tt> et
<tt>ucd-snmp/pass_persist</tt> est plus faible puisque ces modules introduisent
seulement un risque de sécurité si les scripts invoqués sont exploitables.</li>
</ul>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans
la version 5.7.3+dfsg-1.7+deb9u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets net-snmp.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2299.data"
# $Id: $
