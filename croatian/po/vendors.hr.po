msgid ""
msgstr ""
"Project-Id-Version: Debian webwml organization\n"
"PO-Revision-Date: 2006-08-12 16:57+0200\n"
"Last-Translator: Josip Rodin\n"
"Language-Team: Croatian\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../english/CD/vendors/vendors.CD.def:14
msgid "Vendor"
msgstr "Distributer"

#: ../../english/CD/vendors/vendors.CD.def:15
msgid "Allows Contributions"
msgstr "Omogućava priloge (donacije):"

#: ../../english/CD/vendors/vendors.CD.def:16
#, fuzzy
msgid "CD/DVD/BD/USB"
msgstr "CD/DVD"

#: ../../english/CD/vendors/vendors.CD.def:17
msgid "Architectures"
msgstr "Arhitekture"

#: ../../english/CD/vendors/vendors.CD.def:18
msgid "Ship International"
msgstr "Isporučuje međunarodno"

#: ../../english/CD/vendors/vendors.CD.def:19
msgid "Contact"
msgstr "Kontakt"

#. ###################
#. Vendor home page link, Debian page link
#: ../../english/CD/vendors/vendors.CD.def:44
msgid "Vendor Home"
msgstr "Web distributera:"

#: ../../english/CD/vendors/vendors.CD.def:70
msgid "page"
msgstr "stranica"

#: ../../english/CD/vendors/vendors.CD.def:71
msgid "email"
msgstr "e-mail"

#: ../../english/CD/vendors/vendors.CD.def:114
msgid "within Europe"
msgstr "unutar Europe"

#: ../../english/CD/vendors/vendors.CD.def:118
msgid "To some areas"
msgstr "U neka područja"

#: ../../english/CD/vendors/vendors.CD.def:122
msgid "source"
msgstr "izvorni kod"

#: ../../english/CD/vendors/vendors.CD.def:126
msgid "and"
msgstr "i"

#~ msgid "Official CD"
#~ msgstr "Službeni CD"

#~ msgid "Official DVD"
#~ msgstr "Službeni DVD"

#~ msgid "Development Snapshot"
#~ msgstr "Razvojni snimak"

#~ msgid "Vendor Release"
#~ msgstr "Distributerovo izdanje"

#~ msgid "Multiple Distribution"
#~ msgstr "Višestruka distribucija"

#~ msgid "non-US included"
#~ msgstr "non-US uključen"

#~ msgid "non-free included"
#~ msgstr "non-free uključen"

#~ msgid "contrib included"
#~ msgstr "contrib uključen"

#~ msgid "vendor additions"
#~ msgstr "dodaci tvrtke"

#~ msgid "Custom Release"
#~ msgstr "Vlastito izdanje"

#~ msgid "reseller of $var"
#~ msgstr "preprodaje $var"

#~ msgid "reseller"
#~ msgstr "preprodaje"

#~ msgid "updated weekly"
#~ msgstr "osvježava se tjedno"

#~ msgid "updated twice weekly"
#~ msgstr "osvježava se dvaput tjedno"

#~ msgid "updated monthly"
#~ msgstr "osvježava se mjesečno"

#~ msgid "Architectures:"
#~ msgstr "Arhitekture:"

#~ msgid "DVD Type:"
#~ msgstr "Vrsta DVD-a:"

#~ msgid "CD Type:"
#~ msgstr "Vrsta CD-a:"

#~ msgid "email:"
#~ msgstr "e-mail:"

#~ msgid "Ship International:"
#~ msgstr "Isporučuje međunarodno:"

#~ msgid "Country:"
#~ msgstr "Zemlja:"

#~ msgid "Allows Contribution to Debian:"
#~ msgstr "Omogućava donaciju Debianu:"

#~ msgid "URL for Debian Page:"
#~ msgstr "URL za Debian stranicu:"

#~ msgid "Vendor:"
#~ msgstr "Distributer:"
