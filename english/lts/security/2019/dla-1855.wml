<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>It was discovered that there was an integer overflow vulnerability
in exiv2, a tool to manipulate images containing (eg.) EXIF metadata.</p>

<p>This could have resulted in a denial of service via a specially-crafted 
file.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-13504">CVE-2019-13504</a>

    <p>Out-of-bounds read in Exiv2::MrwImage::readMetadata in mrwimage.cpp in Exiv2 through 0.27.2.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
0.24-4.1+deb8u4.</p>

<p>We recommend that you upgrade your exiv2 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1855.data"
# $Id: $
