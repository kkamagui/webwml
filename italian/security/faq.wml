#use wml::debian::template title="FAQ Debian sulla sicurezza"
#include "$(ENGLISHDIR)/security/faq.inc"
#use wml::debian::translation-check translation="6e1fa2e1aabb151dc8ce2445ee60cc40675fb927" maintainer="skizzhg"

<maketoc>

<toc-add-entry name=buthow>Ho ricevuto un DSA da debian-security-announce,
   come posso aggiornare i pacchetti vulnerabili?</toc-add-entry>

<p>R: Come indicato nel DSA, è opportuno aggiornare i pacchetti afflitti
   dalla vulnerabilità. Per farlo è sufficiente aggiornare (dopo aver
   rinfrescato l'elenco dei pacchetti disponibili con <tt>apt-get update</tt>)
   ogni pacchetto installato nel proprio sistema con <tt>apt-get upgrade</tt>
   oppure aggiornando un pacchetto in particolare con <tt>apt-get install
   <i>pacchetto</i></tt>.</p>

<p>Nella email con l'annuncio è menzionato il pacchetto sorgente in cui
   era presente la vulnerabilità. Di conseguenza si dovrebbe aggiornare
   tutti i pacchetti binari creati da quel pacchetto sorgente. Per
   verificare quali sono i pacchetti binari da aggiornare visitare
   <tt>https://packages.debian.org/src:<i>nome-pacchetto-sorgente</i></tt>
   e fare clic su <i>[show ... binary packages]</i> per la distribuzione
   che si sta aggiornando.</p>

<p>Potrebbe essere necessario riavviare un servizio oppure un processo
   in esecuzione. Il comando <a
   href="https://manpages.debian.org/checkrestart"><tt>checkrestart</tt></a>
   contenuto nel pacchetto <a
   href="https://packages.debian.org/debian-goodies">debian-goodies</a>
   può essere utile per determinare quali.</p>


<toc-add-entry name=signature>La firma degli annunci non è
   correttamente verificata!</toc-add-entry>

<p>R: Molto probabilmente è un problema dal vostro lato. La lista
   <a href="https://lists.debian.org/debian-security-announce/">debian-security-announce</a>
   ha un filtro che accetta solo messaggi con una
   firma corretta proveniente da un iscritto al team della sicurezza.</p>

<p>È probabile che uno dei programmi utilizzati per la posta abbia
   cambiato leggermente il messaggio e quindi la firma. Verificate che il
   vostro programma non tocchi la codifica MIME del messaggio o cambi tab
   e spazi.</p>

<p>Problemi si sono verificati con fetchmail (con l'opzione mimedecode
   abilitata), formail (solo la versione di procmail 3.14.) ed evolution.</p>


<toc-add-entry name=handling>Come è gestita la sicurezza in
   Debian?</toc-add-entry>

<p>R: Una volta che il team riceve una notifica di un incidente,
   uno o più membri prendono in carico la segnalazione e valutano
   l'impatto sulla distribuzione <q>stable</q> di Debian (vale a dire
   cercano di capire se Debian sia o meno vulnerabile).
   Se il nostro sistema è vulnerabile allora lavoriamo ad una soluzione
   che lo risolva. Se non si è attivato da solo allora il
   manutentore del pacchetto viene contattato. Alla fine la soluzione
   viene verificata e creato un nuovo pacchetto che viene poi compilato
   su tutte le architetture stabili e poi caricato sul server. Dopo che
   tutto ciò è stato fatto, viene pubblicato l'annuncio.</p>


<toc-add-entry name=oldversion>Perché perdete tempo con una
   vecchia versione di quel pacchetto?</toc-add-entry>

<p>La più importante regola quando viene preparato un nuovo
   pacchetto che risolve un problema di sicurezza è di fare il minimo
   possibile di cambiamenti. I nostri utenti e gli sviluppatori fanno
   affidamento su un corretto funzionamento di una versione appena essa
   viene rilasciata, ed ogni eventuale cambiamento potrebbe creare problemi
   al sistema di qualcuno. Ciò è vero in particolar modo nel
   caso di librerie: siate sicuri che non cambieremo mai le Application
   Program Interface (API) o le Application Binary Interface (ABI), neanche
   con piccole variazioni.</p>

<p>Questo significa che adottare una nuova versione a monte [NdT: in
   inglese <q>upstream version</q>: la versione <q>originale</q>
   (generalmente mantenuta dagli autori) usata per la creazione del
   pacchetto Debian] non è una buona soluzione, invece andrebbero
   riportati verso la vecchia i cambiamenti significativi. Di solito i
   manutentori della versione a monte sono ben disposti ad aiutare,
   altrimenti il team sicurezza Debian potrebbe farlo al loro posto.</p>

<p>In alcuni casi non è possibile creare una soluzione, per esempio
   quando grandi quantità di codice sorgente dovrebbero essere
   modificate o riscritte. In tal caso potrebbe essere necessario migrare
   verso una nuova versione, ma ciò deve essere preventivamente
   concordato con il team della sicurezza.</p>


<toc-add-entry name=version>La versione del pacchetto indica che io sto
   utilizzando una versione vulnerabile!</toc-add-entry>

<p>R: Invece di aggiornare il programma all'ultima versione preferiamo
   riportare solo le modifiche legate al problema sulla vecchia versione.
   La ragione per la quale facciamo ciò è che vogliamo fare
   in modo che la soluzione abbia il minore impatto possibile sul resto
   della distribuzione.
   Si può verificare se si sta utilizzando una versione sicura
   controllando il changelog del pacchetto o confrontando il numero completo
   di versione con quello indicato nel Debian Security Advisory.</p>


<toc-add-entry name=archismissing>Ho ricevuto un avviso, ma sembra
   mancare il pacchetto per una certa architettura.</toc-add-entry>

<p>R: In genere il team sicurezza rilascia un avviso con i pacchetti
   aggiornati per tutte le architetture supportate da Debian. 
   Tuttavia, alcune di esse sono più lente di altre e può accadere che
   le compilazioni siano per la maggior parte pronte mentre manchino per
   altre; tali architetture rappresentano una piccola parte della nostra
   utenza. A seconda dell'urgenza del problema, si può decidere di
   pubblicare l'annuncio immediatamente. I pacchetti mancanti saranno
   aggiunti all'archivio non appena disponibili, ma non verrà pubblicato
   alcun ulteriore avviso in proposito.
   Naturalmente non rilasceremo mai un avviso dove non siano presenti i
   pacchetti per i386 o amd64.</p>


<toc-add-entry name=unstable>Come è gestita la sicurezza per
   <tt>unstable</tt>?</toc-add-entry>

<p>R: La sicurezza per unstable è principalmente gestita dai curatori dei
   pacchetti, non dal Debian Security Team. Nel caso i curatori siano
   inattivi il team della sicurezza potrebbero inviare delle correzioni
   <em>high-urgency security-only</em>, in ogni caso il supporto per il
   rilascio stabile ha sempre la priorità. Se si vuole un server sicuro
   (e stabile) invitiamo caldamente a utilizzare la versione stable.</p>


<toc-add-entry name=testing>Come è gestita la sicurezza per
   <tt>testing</tt>?</toc-add-entry>

<p>R: La sicurezza per testing beneficia del lavoro fatto dall'intero
   progetto per unstable; c'è comunque un ritardo che al minimo è di due
   giorni e alcune volte le correzioni di sicurezza sono bloccate a causa
   delle transizioni. Il Security Team aiuta a sbloccare le transizioni
   che tengono fermi delle importanti correzioni per la sicurezza,
   tuttavia ciò non è sempre possibile e si possono verificare dei
   ritardi. In particolare nei mesi successivi a un nuovo rilascio
   stabile, quando in unstable arrivano molte nuove versioni, le
   correzioni di sicurezza possono avere ritardi. Se si desidera avere un
   server sicuro (e stabile) invitiamo caldamente ad utilizzare la
   versione stable.</p>


<toc-add-entry name=contrib>Come è gestita la sicurezza per
   <tt>contrib</tt> e <tt>non-free</tt>?</toc-add-entry>

<p>R: La risposta breve è: non lo è. Contrib e non-free non
   sono parti ufficiali della distribuzione Debian e per questo non sono
   supportate dal team sicurezza. Alcuni pacchetti non-free sono distribuiti
   senza sorgenti o senza una licenza che permetta la distribuzione di versioni
   modificate. E in quei casi sono completamente impossibili i fix di
   sicurezza. Se c'è la possibilità di risolvere il problema e il
   manutentore del pacchetto o qualcun altro fornisce un pacchetto
   correttamente aggiornato, allora di solito il team sicurezza lo
   processa e rilascia un advisory.</p>


<toc-add-entry name=sidversionisold>L'avviso dice che la soluzione in
   unstable è nella versione 1.2.3-1, ma unstable ha la 1.2.5-1, cosa
   succede?</toc-add-entry>

<p>R: Cerchiamo di indicare la prima versione in unstable che risolve il
   problema. Qualche volta il maintainer ha nel frattempo caricato nuove
   versioni. Si confronti la versione in unstable con quella da noi
   indicata. Se è la stessa o superiore si dovrebbe essere al sicuro
   dalla vulnerabilità. Per essere sicuri controllare il changelog del
   pacchetto con <tt>apt-get changelog nome-pacchetto</tt> e cercare
   una voce che annuncia la correzione.</p>


<toc-add-entry name=mirror>Perché non ci sono mirror ufficiali di
   security.debian.org?</toc-add-entry>

<p>R: Ci sono. Esistono alcuni mirror ufficiali, realizzati
   mediante alias DNS. Lo scopo di security.debian.org è di
   rendere disponibili gli aggiornamenti nella maniera più
   semplice e rapida possibile.</p>

<p>Incoraggiare l'uso di mirror non ufficiali potrebbe aggiungere un'inutile
   maggiore complessità che potrebbe causare frustrazione se
   i mirror non fossero tenuti aggiornati.</p>


<toc-add-entry name=missing>Ho visto DSA 100 e DSA 102. Dov'è il DSA 101?</toc-add-entry>

<p>R: Alcuni venditori (la maggior parte di GNU/Linux, ma anche di
   derivati BSD) coordinano a volte i <q>Security Advisor</q>
   stabilendo delle date affinché tutti i venditori possano
   rilasciare l'annuncio contemporaneamente. Questo è stato fatto per
   non discriminare i venditori che richiedono più tempo (in altre
   parole quando il venditore necessita di far passare il pacchetto
   attraverso lunghi test di QA o deve supportare diverse architetture o
   distribuzioni binarie). Anche il nostro team prepara gli annunci in
   anticipo. Ogni tanto altri problemi legati alla sicurezza sono stati
   gestiti prima che uscisse l'annuncio ufficiale (e quindi, che fosse
   assegnato il numero), per questo motivo capita che i numeri non siano
   tutti consecutivi.</p>


<toc-add-entry name=contact>Come posso contattare il team sicurezza?</toc-add-entry>

<p>R: Informazioni inerenti alla sicurezza possono essere inviate a
   security@debian.org o a team@security.debian.org, lette ambedue dai
   membri del team sicurezza.
</p>

<p>Chi vuole può crittare l'e-mail con la chiave Debian Security Contact
   (key ID <a
   href="http://pgp.surfnet.nl/pks/lookup?op=vindex&amp;search=0x0D59D2B15144766A14D241C66BAF400B05C3E651">0x0D59D2B15144766A14D241C66BAF400B05C3E651</a>).
   Per le chiavi PGP/GPG personali di membri del team members, si usi il
   keyserver <a href="https://keyring.debian.org/">keyring.debian.org</a>.</p>


<toc-add-entry name=discover>Suppongo di aver trovato un problema di
   sicurezza, cosa dovrei fare?</toc-add-entry>

<p>R: Se si scopre qualche problema di sicurezza, sia in un proprio pacchetto
   che in quello di altre persone, contattare il team sicurezza. Se
   il team sicurezza Debian conferma la vulnerabilità e se esiste
   la possibilità che anche altri venditori siano vulnerabili, il
   team di solito contatta anche gli altri venditori.
   Se la vulnerabilità non è ancora pubblica il team cerca
   di coordinare i <q>security advisory</q> con gli altri venditori,
   per mantenere le maggiori distribuzioni sincronizzate.</p>

<p>Se la vulnerabilità è già pubblicamente nota, assicurarsi di aprire un
   bug report nel Debian BTS, con il tag <q>security</q>.</p>

<p>Se si è un manutentore Debian, <a href="#care">leggere sotto</a>.</p>


<toc-add-entry name=care>Cosa dovrei fare con un problema di sicurezza
   in uno dei miei pacchetti?</toc-add-entry>

<p>R: Se si scopre un problema di sicurezza in un pacchetto, sia proprio
   che di altre persone, contattare il team sicurezza tramite posta
   elettronica all'indirizzo team@security.debian.org. I membri del
   team tengono traccia dei problemi di sicurezza irrisolti, possono
   aiutare i manutentori con problemi di sicurezza o risolverli essi
   stessi, sono responsabili dell'emissione dei <q>security advisory</q>
   e curano security.debian.org.</p>

<p>La <a href="$(DOC)/developers-reference/pkgs.html#bug-security">Developer's
   Reference</a> contiene le istruzioni complete su cosa fare.</p>

<p>È particolarmente importante che non si carichi verso qualsiasi
   distribuzione che non sia <q>unstable</q> senza un accordo
   preventivo con il team sicurezza, perché bypassarlo causerebbe
   confusione e maggior lavoro.</p>


<toc-add-entry name=enofile>Ho provato a scaricare un pacchetto elencato
   in uno dei <q>Security Advisor</q> ma ho avuto un errore <q>file
   not found</q>.</toc-add-entry>

<p>R: Tutte le volte che un <q>bugfix</q> più nuovo
   sostituisce un pacchetto più vecchio su security.debian.org, ci
   sono alte probabilità che il vecchio pacchetto sia stato 
   rimosso quando quello nuovo è stato installato. Da quel momento
   si avrà l'errore <q>file not found</q>. Non vogliamo
   distribuire pacchetti con bug di sicurezza conosciuti più a
   lungo di quanto sia assolutamente necessario.</p>

<p>Bisogna usare i pacchetti elencati negli ultimi <q>security
   advisor</q>, che sono distribuiti tramite la mailing list <a
   href="https://lists.debian.org/debian-security-announce/">debian-security-announce</a>.
   La cosa migliore è semplicemente eseguire <code>apt-get update</code>
   prima aggiornare il pacchetto.</p>


<toc-add-entry name=upload>Ho trovato la soluzione ad un problema, posso
   caricarla su security.debian.org direttamente?</toc-add-entry>

<p>R: No, non è possibile. L'archivio a security.debian.org
   è mantenuto dal team sicurezza, che deve approvare tutti i
   pacchetti. È necessario inviare le <q>patch</q> o il
   codice sorgente modificato al team sicurezza tramite
   l'indirizzo team@security.debian.org. Saranno controllati
   dal team sicurezza ed eventualmente caricati, con o senza
   modifiche.</p>

<p>Se non si è pratici di aggiornamenti per la sicurezza e non si
   è sicuri al 100% che il pacchetto sia integro, usare questo metodo
   e non caricare nella directory incoming. Il team sicurezza ha poche
   possibilità di gestire pacchetti difettosi, specialmente se non
   hanno un numero di versione corretto. Attualmente i pacchetti non possono
   essere rifiutati ed il funzionamento di <abbr title="Build
   Daemon">buildd</abbr> diventerebbe problematico. Per favore si usi la
   posta elettronica e si aiuti non aggiungendo complicazioni al lavoro del
   team sicurezza.</p>

<p>La <a
   href="$(DOC)/developers-reference/pkgs.html#bug-security">Developer's
   Reference</a> contiene le istruzioni complete su cosa fare.</p>


<toc-add-entry name=ppu>Ho trovato la soluzione ad un problema, posso
   caricarlo invece su <q>proposed-updates</q>?</toc-add-entry>

<p>R: Dal punto di vista tecnico, sì. Comunque, non si dovrebbe farlo,
   visto che ciò interferisce pesantemente con il lavoro del team
   sicurezza. I pacchetti sono copiati da security.debian.org nella directory
   <q>proposed-updates</q> automaticamente. Se un pacchetto con un numero
   di versione uguale o superiore è già installato nell'archivio,
   l'aggiornamento di sicurezza sarà rifiutato dal sistema di
   archiviazione. In tal caso, la distribuzione <q>stable</q>
   sarà preparata senza l'aggiornamento di sicurezza di quel
   pacchetto, a meno che i pacchetti <q>sbagliati</q> nella directory
   proposed-updates non siano stati rifiutati. Si contatti invece il team
   sicurezza includendo tutti i dettagli sulla vulnerabilità ed
   allegando i file sorgente (cioè i file diff.gz e dsc) all'e-mail.</p>

<p>La <a
   href="$(DOC)/developers-reference/pkgs.html#bug-security">Developer's
   Reference</a> contiene le istruzioni complete su cosa fare.</p>


<toc-add-entry name=SecurityUploadQueue>Sono sicurissimo che il mio
   pacchetto va bene, posso caricarlo?</toc-add-entry>
   
<p>R: Se si è veramente sicuri che il pacchetto non danneggi
   nulla, che sia la versione sia giusta (cioè maggiore della
   versione in <q>stable</q> e minore della versione in
   <q>testing</q>/<q>unstable</q>), che non sia cambiato il
   funzionamento del pacchetto nonostante sia stato corretto il problema di
   sicurezza, che sia stato compilato per la corretta distribuzione (che
   è <code>oldstable-security</code> o <code>stable-security</code>),
   che il pacchetto contenga il sorgente originale se è nuovo in
   security.debian.org, che la <q>patch</q> sia valida
   per la versione più recente e che essa riguardi solo il
   relativo problema di sicurezza (controllare con <code>interdiff -z</code> i
   file <code>.diff.gz</code>), che la <q>patch</q> sia stata
   controllata almeno tre volte e che <code>debdiff</code> non mostri
   alcun cambiamento, allora è possibile caricare i file nella
   directory incoming
   <code>ftp://ftp.security.upload.debian.org/pub/SecurityUploadQueue</code>
   direttamente a security.debian.org. Si prega di inviare anche un
   avviso con tutti i dettagli e i link a team@security.debian.org.</p>


<toc-add-entry name=help>Come posso aiutare?</toc-add-entry>

<p>R: Controllando bene ogni problema prima di inviarlo a security@debian.org.
   Se si è capaci di fornire delle <q>patch</q> allora questo
   accelererebbe il processo. Non limitarsi ad inoltrare messaggi su come
   verificare la presenza del problema perché noi li riceviamo
   già; cercare invece di aggiungere tutte le informazioni possibili.</p>

<p>Un buon modo per iniziare con il security work è di aiutare
   con il Debian Security Tracker (<a
   href="https://security-tracker.debian.org/tracker/data/report">istruzioni</a>).</p>


<toc-add-entry name=proposed-updates>Qual è lo scopo di proposed-updates?</toc-add-entry>

<p>R: Questa directory contiene i pacchetti che sono candidati ad entrare
   nella prossima distribuzione <q>stable</q> di Debian. Ogni volta
   che un manutentore carica un pacchetto per la distribuzione
   <q>stable</q> allora il pacchetto viene memorizzato nella stessa
   directory. Poiché <q>stable</q> è una versione che
   è ritenuta stabile allora non viene aggiornata automaticamente.
   Il team della sicurezza invia alla versione <q>stable</q> gli
   aggiornamenti dei pacchetti menzionati negli annunci, ma questi
   vengono inseriti nella directory proposed-updates. Ogni tanto il
   manager della distribuzione stabile controlla la lista dei pacchetti
   in proposed-updates e vaglia se un pacchetto sia pronto o meno per
   <q>stable</q>. Il tutto viene poi inserito nella revisione
   successiva di <q>stable</q>, come 2.2r3 o 2.2r4. I pacchetti non
   adatti saranno probabilmente rifiutati e cancellati da proposed-updates.

<p>Si noti che i pacchetti caricati dai manutentori (e non dal team sicurezza)
   nella directory proposed-updates/ non sono supportati dal
   team sicurezza.</p>


<toc-add-entry name=composing>Come è composto il team della sicurezza?</toc-add-entry>

<p>R: Il team sicurezza Debian è composto da
   <a href="../intro/organization#security">diversi membri e segretari</a>.
   È lo stesso team sicurezza che invita le persone ad unirsi ad esso.</p>


<toc-add-entry name=lifespan>Per quanto tempo sono assicurati gli
   aggiornamenti di sicurezza?</toc-add-entry>

<p>R: Il team sicurezza cerca di supportare una distribuzione stable per
   circa un anno dal rilascio della successiva distribuzione stable,
   a meno che un'ulteriore distribuzione stable sia rilasciata
   nell'anno stesso. Non è possibile supportare tre
   distribuzioni; supportarne due contemporaneamente è già
   abbastanza difficile.
</p>


<toc-add-entry name=check>Come si può controllare che i pacchetti
   siano integri?</toc-add-entry>

<p>R: Controllando la firma del file Release mediante la <a
   href="https://ftp-master.debian.org/keys.html">chiave pubblica</a> usata
   per l'archivio. Il file Release contiene le checksums dei file Packages
   e Sources, che a loro volta contengono le checksums dei pacchetti binari
   e sorgenti. Istruzioni dettagliate su come controllare l'integrità dei
   pacchetti possono essere trovate nel <a
   href="$(HOME)/doc/manuals/securing-debian-howto/ch7#s-deb-pack-sign">Debian
   Securing Manual</a>.</p>


<toc-add-entry name=break>Cosa fare se un altro pacchetto non funziona
   dopo un aggiornamento di sicurezza?</toc-add-entry>

<p>R: Prima di tutto si dovrebbe capire perché il pacchetto non
   funziona e come il malfunzionamento è correlato con
   l'aggiornamento di sicurezza, poi si contatti il team sicurezza se il
   malfunzionamento è  serio o lo stable release manager se non lo
   è. Stiamo discutendo circa i pacchetti che non funzionano dopo
   l'aggiornamento di sicurezza di un altro pacchetto. Se non si
   può capire cosa non va ma si ha una correzione, si contatti il
   team sicurezza anche se si potrebbe essere ridiretti allo stable
   release manager.</p>


<toc-add-entry name=cvewhat>Cosa è un identificatore CVE?</toc-add-entry>

<p>R: Il progetto <em>Common Vulnerabilities and Exposures</em> assegna un
   nome univoco, chiamato identificatori CVE, per indicare una specifica
   vulnerabilità in modo da potersi riferire al problema in modo semplice e
   inequivocabile. Ulteriori informazioni possono essere trovate su <a
   href="https://it.wikipedia.org/wiki/Common_Vulnerabilities_and_Exposures">Wikipedia</a>.</p>


<toc-add-entry name=cvedsa>Debian emette un DSA per ogni id
   CVE?</toc-add-entry>

<p>R: Il Debian security team tiene traccia di ogni id CVE rilasciato, lo
   lega ai pacchetti relevanti e valuta il suo impatto nel contesto di
   Debian, il fatto che a qualcosa sia assegnato un id CVE non implica
   necessariamente che il problema sia una seria minaccia per un sistema
   Debian. Queste informazioni sono tracciate nel <a
   href="https://security-tracker.debian.org">Debian Security Tracker</a>
   e per quei problemi che sono considerati seri viene emesso un Debian
   Security Advisory.</p>

<p>I problemi con un basso impatto che non implicano un DSA verranno risolti
   nel rilascio Debian successivo, in un rilascio minore delle distribuzioni
   stable o oldstable oppure sono inseriti in un altro DSA qualora sia emesso
   per una vulnerabilità più seria.</p>


<toc-add-entry name=cveget>Debian può assegnare identificatori
   CVE?</toc-add-entry>

<p>R: Debian è una Numbering Authority di CVE e può assegnare gli
   identificatori ma per policy di CVE solo nel caso di problemi non ancora
   conosciuti. Coloro che sono a conoscenza di una vulnerabilità riguardante
   la sicurezza in un software per Debian e vorrebbe avere un identificatore
   per il problema possono contattare il Debian Security Team. Per i casi in cui
   la vulnerabilità sia già nota si consiglia di seguire la procedura indicata
   nel <a href="https://github.com/RedHatProductSecurity/CVE-HOWTO">CVE
   OpenSource Request HOWTO</a>.</p>


<toc-add-entry name=disclosure-policy>Debian ha una policy sulla divulgazione
   delle vulnerabilità?</toc-add-entry>
<p>R: Debian ha pubblicato una <a href="disclosure-policy">policy sulla
   divulgazione delle vulnerabilità</a> in quanto parte della sua
   partecipazione al programma CVE.</p>


<h1>FAQ Debian sulla sicurezza deprecate</h1>


<toc-add-entry name=localremote>Che significa <q>local 
   (remote)</q>?</toc-add-entry>

<p><b>L'informazione <i>Tipo di problema</i> nelle email DSA mails non è più
   usato da aprile 2014.</b><br/>
   R: Alcuni avvisi riguardano vulnerabilità che non possono essere
   identificate con il classico schema di exploit locale e remoto. Alcune
   vulnerabilità non possono essere sfruttate da remoto, in altre parole non
   corrispondono ad un demone in ascolto su una porta di rete. Nel caso
   possano essere sfruttate da file speciali forniti eventualmente via rete
   anche se il servizio vulnerabile non è connesso permanentemente in rete,
   lo descriveremo come <q>local (remote)</q>.</p>

<p>Tali vulnerabilità sono a metà strada tra le locali e le remote e spesso
   coprono archivi che possono essere forniti attraverso la rete, come
   allegati di posta o da una pagina di download.</p>
