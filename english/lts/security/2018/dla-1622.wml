<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>debian-security-support, the Debian security support coverage checker,
has been updated in jessie. The jessie relevant changes are:</p>

<ul>
  <li>Mark jasperreports as end-of-life in Jessie.</li>
  <li>Mark webkit2gtk as unsupported in all releases. (Closes: #914567)</li>
  <li>Mark jruby in jessie as end-of-life as per DSA-4219-1 (Closes: #901032)</li>
  <li>Mark vlc in jessie as end-of-life as per DSA 4203-1</li>
  <li>mark frontaccounting as unsupported</li>
  <li>Mark redmine as end-of-life for Debian 8 (jessie) (Closes: #897609)</li>
</ul>

<p>For Debian 8 <q>Jessie</q>, the package version is 2018.11.25~deb8u2.</p>

<p>We recommend that you upgrade your debian-security-support packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>

<p>For Debian 6 <q>Squeeze</q>, these issues have been fixed in debian-security-support version 2018.11.25~deb8u2</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1622.data"
# $Id: $
