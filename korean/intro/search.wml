#use wml::debian::template title="데비안 검색 엔진 사용법"
#use wml::debian::translation-check translation="c3b565ac6b9deb00572865c557a9c7094047163a" maintainer="Sebul"

<P> <a href="https://search.debian.org/">https://search.debian.org/</a>에 있는
데비안 검색 엔진은 여러분이 무엇을 하고 싶은가에 따라 여러 방식의 검색을 지원합니다.

<H3>단순 검색</H3>

<P>가장 간단한 방법은 검색 상자 안에 한 낱말을 입력하고 엔터를 누르는
것이다(아니면 <em>Search</em> 버튼을 누릅니다). 그러면 검색 엔진은 그
낱말이 들어있는 모든 페이지를 보여줄 것입니다. 이 방법은 종종 좋은
결과를 보여줍니다.

<P>다음 단계는 하나 이상의 단어를 검색하는 것인데,
입력한 모든 단어가 들어간 웹 사이트 페이지 목록을 보여줍니다.

<H3>Boolean 검색</H3>

<P>단순 검색이 충분하지 않다면
<a href="http://foldoc.org/boolean">Boolean</a>
검색을 사용해 봅니다. <em>AND</em>, <em>OR</em>,
<em>NOT</em>을 사용하거나 이 셋을 조합할 수 있습니다.

<P><B>AND</B>는 두 단어를 모두 포함한 페이지를 보여줍니다. 예를 들어 
 "gcc AND patch" 는
"gcc"와 "patch"가 모두 들어있는 URL을 찾습니다.

<P><B>OR</B>은 두 단어 중 하나라도 들어있는 페이지를
보여줍니다. 예를 들어 
 "gcc OR patch"는 "gcc"나
 "patch"가 들어있는 URL을 찾습니다.

<P><B>NOT</B>은 결과에서 이 단어를 제외시킵니다.
예를 들어 "gcc NOT patch"는 "gcc"를 포함하고 "patch"를 포함하지 않은
URL을 찾습니다. "NOT patch"만으로 검색하는 것은 지원하지 않습니다.
 
<P><B>(</B>...<B>)</B>를 사용하여 하위 표현식을 그룹화 할 수 있습니다.
예를 들어 "(gcc OR make) NOT patch"는
"gcc"또는 "make"는 포함하지만 "patch"는 포함하지 않은 모든 페이지를 찾습니다.