<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>The load_multiboot function in hw/i386/multiboot.c in Quick Emulator
(aka QEMU) allows local guest OS users to execute arbitrary code on
the QEMU host via a mh_load_end_addr value greater than
mh_bss_end_addr, which triggers an out-of-bounds read or write memory
access.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
1.1.2+dfsg-6+deb7u25.</p>

<p>We recommend that you upgrade your qemu packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1351.data"
# $Id: $
