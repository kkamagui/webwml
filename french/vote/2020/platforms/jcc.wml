#use wml::debian::template title="Programme de Jonathan Carter" BARETITLE="true" NOHEADER="true"
#include "$(ENGLISHDIR)/vote/style.inc"
#use wml::debian::translation-check translation="8c043a250a1c33c7397a39c9acedea0b750975d3" maintainer="Jean-Paul Guillonneau"

<DIV class="main">
<TABLE CLASS="title">
<TR><TD>
<BR>
<H1 CLASS="titlemain"><BIG><B>Jonathan Carter</B></BIG><BR>
    <SMALL>Programme de DPL</SMALL><BR>
    <SMALL>15/03/2020</SMALL>
</H1>
<H3>
<A HREF="mailto:jcc@debian.org"><TT>jcc@debian.org</TT></A><BR>
<A HREF="https://jonathancarter.org"><TT>https://jonathancarter.org</TT></A><BR>
<A HREF="https://wiki.debian.org/highvoltage"><TT>https://wiki.debian.org/highvoltage</TT></A>
</H3>
</TD></TR>
</TABLE>

<DIV CLASS="summary">
<B>Objectifs généraux</B> :
<UL CLASS="itemize">
<LI CLASS="li-itemize"> <b>Continuer à faire ce que Debian réalise bien.</b> Excellence technique.
 Promotion du logiciel libre. Empaquetage. Réalisation de nouvelles publications. Mises à jour
 de la publication de Stable.</LI>
<LI CLASS="li-itemize"> <b>Rendre Debian attrayante aux contributeurs.</b> Faire qu’elle soit
 un projet intéressant, enrichissant et accueillant pour les contributeurs actuels et de même
 pour les nouveaux. Apporter une meilleure visibilité à l’excellent travail réalisé.</LI>
<LI CLASS="li-itemize"> <b>Réduire les blocages touchant nos contributeurs.</b> Recueillir de
 fréquents retours sur ce qui empêche des individus de faire ce à quoi ils s’intéressent dans
 Debian, et trouver des moyens de réduire ces freins. Améliorer la collaboration en ligne.</LI>
<LI CLASS="li-itemize"> <b>Améliorer la maintenance du projet.</b> Rendre plus visibles et
 transparentes nos dépenses et nos fonds disponibles. Maintenir un meilleur suivi de nos équipes
 locales. Améliorer les actualisations du DPL.</LI>
</UL>
</DIV>


<H2 CLASS="section">1. Informations personnelles</H2>

<P>Hello, mon nom est Jonathan Carter, connu aussi comme <EM>highvoltage</EM>, avec comme pseudo
 dans Debian <EM>jcc</EM>, et je suis candidat au poste de DPL. </P>

<P>Je suis un hacker de trente-huit ans et un développeur Debian ayant plusieurs domaines d’intérêt
 dans le projet Debian.</P>


<H2 CLASS="section">2. Pourquoi suis-je candidat au poste de DPL</H2>

<P>J’ai expliqué ma motivation pour devenir DPL dans mon
<a href="https://lists.debian.org/debian-vote/2020/03/msg00007.html">courriel de candidature</a>.
En bref, je pense que cette année nous devrions nous concentrer sur la stabilité dans notre
communauté, et prétendre à un environnement où les contributeurs et leurs idées peuvent prospérer.
</P>


<H2 CLASS="section">3. Vision</H2>

<P>Ces idées sont des points de départ. Elles peuvent évoluer (ou même être abandonnées
complètement) selon les débats futurs dans le projet.
</P>


<H3 CLASS="subsection">3.1. Améliorations de la communauté</H3>


<P><B>3.1.1. Lancer un débat public sur notre nomenclature de membres</B></P>

<P>Le terme « développeur Debian » date un peu et ne reflète pas la manière dont le reste du monde
 utilise encore le terme « développeur ». Par exemple, un « développeur Android » ou un « développeur
 iOS » ne sont plus des personnes qui travaillent pour ces projets en amont, mais des personnes qui
 développent pour ces plateformes.
</P>

<P>Le terme conduit aussi à des bizarreries comme appeler nos membres du projet n’ayant aucun droit
de téléversement « Non-uploading Debian Developers » et appeler les responsables de paquet avec
des droits partiels de téléversement qui ne sont pas membres « Debian Maintainers ». Il est aussi
possible pour quelqu’un d’être « Debian Maintainer » et « Non-uploading DD », ce qui est une
manière vraiment déroutante d’exprimer leur statut dans le projet, particulièrement pour les
non connaisseurs.
</P>

<P>J’apprécie que le terme DD veuille dire une grande efficience et je pense que nous pouvons conserver
cela par une meilleure terminologie. Des suggestions telles que DPM pour « Debian Project Member » ont
circulé dans des discussions précédentes. Je pense qu’il est temps d’avoir un débat au sein du projet en bonne
et due forme à propos de cela, de trouver un ensemble de termes et d’acronymes bien adaptés et d’intégrer
les répercussions dans la documentation du projet.
</P>

<P>Selon les conclusions du débat et du ressenti de nos membres, je suis désireux de l’amener vers une
résolution générale.
</P>


<P><B>3.1.2. Promouvoir le mentorat</B></P>

<p>J’ai déjà mentionné que le statut de développeur Debian signifie efficience. J’aimerais nourrir une
culture dans Debian dans laquelle la supervision et le mentorat d’autres personnes deviennent hautement
estimés et que plus de débianeux soient motivés pour y consacrer plus de temps.
</P>

<P>Des milliers de contributeurs de Debian ne sont pas développeurs Debian et attendent
souvent pas mal de temps que leurs paquets soient revus et parrainés. Améliorer le temps de traitement
pour cela aiderait davantage de contributeurs à devenir finalement développeurs Debian, ce qui en
conséquence réduirait la charge de révisions.
</P>

<P>Le BTS a aussi un énorme arriéré de correctifs qui malheureusement deviennent inadaptés à cause du
temps que quelqu’un daigne les examiner. C’est quelque chose que nous devons changer.
</P>

<P>Nous devrions nous encourager les uns les autres à appliquer les correctifs et les parrains à
téléverser et organiser des chasses aux bogues. Ces chasses et les évènements similaires sont vraiment
importantes pour la réussite de Debian et j’entends promouvoir cela.
</P>


<P><B>3.1.3. Améliorer la participation en ligne</B></P>

<P>Lors d’évènements entre individus (tels que les chasses aux bogues), du bon travail est réalisé parce
nous faisons abstraction de nos distractions habituelles et nous disposons d’une grande bande passante pour
accéder aux autres personnes de Debian pouvant être aux alentours. De même, il n’y a aucune raison pour
laquelle nous ne pourrions pas avoir des évènements similaires en ligne. Si cela est structuré
correctement, cela pourrait être productif et amusant. Il existe maintenant de meilleures solutions qui
simplifient le partage de copies d’écran ou de clips vidéo, et que nous devrions envisager en plus des
outils actuels tel IRC. Bien sûr, il est possible d’incorporer des liens dans IRC, mais il faut
utiliser des programmes externes et il n’est pas possible d’avoir des prévisualisations, ce qui rend la
chose plus laborieuse.
</P>

<P>À cause de considérations éthiques et logistiques, de finances ou de responsabilités, le voyage
n’est tout simplement pas une bonne option pour la plupart de nos contributeurs même dans des
circonstances normales. Au moment où j’écris, nous sommes confrontés à une quarantaine et des restrictions
de voyage dans des entreprises et des régions de par le monde, à cause du virus SARS-CoV-2 apportant la
maladie COVID-19. Au moins sur le court terme, cela affectera notre capacité à se regrouper pour faire
notre travail ou pour simplement se déplacer dans un autre pays. Cette crise fait de cette année, une année
spécialement importante pour étudier l’ajout d’outils collaboratifs.
</P>

<P>De bons outils en ligne ne sont pas une nécessité pour se rassembler, mais s’ils sont mis en œuvre
efficacement, ils peuvent être un second choix excellent lorsqu’une rencontre en personne n’est pas
envisageable. Peut-être qu’une Debconf annuelle supplémentaire en ligne, décalée de six mois par rapport à
la conférence habituelle, peut être envisageable. Cela aiderait à rendre Debian plus fédératrice pour ceux
ne pouvant pas voyager.
</P>


<P><B>3.1.3. Équipes locales</B></P>

<P>Quelques essais ont été réalisés au cours des ans pour initier une équipe centrale prenant soin globalement
des équipes locales. Une telle équipe recenserait les équipes locales et garderait leur dossier à jour. Elle
devrait aider aussi à constituer de nouvelles équipes locales de façon qu’elles démarrent d’un bon pied.
Elle pourrait aussi imprimer des autocollants, des dépliants et des affiches qui seraient distribués aux
équipes locales.
</P>

<P>J’ai l’intention de rechercher les personnes qui ont précédemment travaillé sur cela et d’aider à
reconstituer une telle équipe, l’encourageant à se rassembler (sprint) ou à organiser quelques sessions lors
des DebConf.</P>

<P>Nous avons déjà quelques équipes locales très fortes, et celles-ci n’en seront pas perturbées, mais je crois
qu’il pourrait exister plus de groupes locaux forts avec un soupçon d’aide et d’encouragement.
</P>


<P><B>3.1.4. Meilleure intégration</B></P>

<P>J’ai mentionné l’incorporation de nouvelle équipes locales dans le point ci-dessus, mais nous pourrions
l’améliorer aussi pour les individus. Debian est toujours déroutante et semble souvent impressionnante aux
nouveaux contributeurs, et cela ne devrait pas.
</P>

<P>C’est toujours trop difficile pour un nouveau venu d’apprendre comment le projet Debian fonctionne, de
trouver quelque chose d’intéressant à faire et ensuite de joindre les bonnes personnes avec qui travailler.
</P>

<P>Je n’ai aucune solution à proposer mais je pense que nous devrions continuer à débattre de cela lors
d’évènements généraux tels que DebConf et durant toute l’année. Des choses aussi basiques que des brochures
et des affiches expliquant les concepts centraux pourraient aider un tas de gens.
</P>


<P><B>3.1.5. Innovation et expérimentation</B></P>

<P>Je pense que Debian devrait s’engager dans un projet ayant la réputation de permettre
à l’expérimentation et l’innovation de prospérer.
</P>

<P>Si un membre du projet a besoin d’un soutien financier pour concrétiser une innovation, j’ai
l’intention d’être plutôt généreux en approuvant de telles dépenses en suivant nos règles.
</P>

<P>Je crois qu’il devrait être plus facile pour un développeur Debian d’obtenir une instance VPS pour des
expériences sans avoir à payer pour cela. Plusieurs petits obstacles tels que celui-ci existent que nous
devrions atténuer.
</P>


<H3 CLASS="subsection">3.2. Améliorer les rapports</H3>


<P><B>3.2.1. Meilleure compréhension des finances</B>

<P>Dans le passé, des requêtes ont été faites pour une meilleure compréhension de comment Debian dépense ses
fonds et quel montant est disponible à un instant donné.
</P>

<P>C’est un problème que les DPL précédents ont cherché à résoudre. Je ne suis pas sûr jusqu’à quel point nous
pouvons solutionner cela avant la fin du prochain mandat, mais je m’efforcerai de publier des rapports
au moins tous les trimestres sur nos dépenses, ainsi qu’un résumé de ce qui est disponible avec nos
organisations de confiance.
</P>


<P><B>3.2.2. Informations plus fréquentes</B></P>

<P>Cela a été une tendance fantastique des récents DPLs de créer des journaux mensuels de leurs activités. Le problème
avec cela c’est qu’ils peuvent être un peu longs, ce qui peut conduire à des pertes d’informations potentiellement
importantes. Au moment où le rapport mensuel est publié, quelques informations, qui pourraient être plus opportunes,
peuvent être périmées.
</P>

<P>Si je suis élu, je continuerai à publier des journaux mensuels, mais j’envisage de publier des informations plus
courtes entre-temps qui pourront alors être rattachées dans un rapport mensuel plus important.
</P>

<H2>4. Remerciement</H2>

<UL>
<LI>J’ai utilisé <a href="https://www.debian.org/vote/2010/platforms/zack">la mise en page du programme de Zack</a>
comme base pour celle-ci.</LI>
</UL>

<H2> A. Réfutations</H2>



<H3>A.1. Sruthi Chandran </H3>

<P>C’est vraiment formidable d’avoir une autre femme candidate au poste de DPL. Je crois que cela est arrivé
la dernière fois en 2010, et je considère que c’est plutôt déplorable. Aussi, quoiqu’il arrive, je veux
la féliciter pour sa candidature et je suis sûr qu’elle sera une stimulation pour beaucoup d’autres.</P>

<P>Concernant son programme, ses intentions générales sont bonnes mais il manque de détails. Cela rend plus
difficile d’imaginer ce à quoi ressemblera un mandat avec Sruthi. Cela rend plus difficile de trouver des
points qui plaisent ou pas.</P>

<P>Ses idées de donner plus d’attention à des efforts pour la diversité peuvent être intéressantes. Des questions
ont déjà été posées dans des discussions récentes à propos du retour sur investissement pour les dépenses
de sensibilisation et si notre politique de sensibilisation actuelle est optimale. Personnellement, je ne pense
pas qu’un retour sur investissement de 100 % puisse être atteint pour la sensibilisation, c'est-à-dire, que pour
chaque personne que vous touchiez et sur laquelle vous vous investissiez, vous obteniez un contributeur durable,
mais cela ne veut pas dire que cette sensibilisation ne soit pas utile ou que nous devions pas nous en
préoccuper. Toutefois, je serais curieux de voir que Sruthi ferait.</P>

<P>J’aime le travail qu’elle a fait comme DD, et elle est engagée dans un parcours de montagnes russes pour
organiser une DebConf en Inde en 2022, mais je doute que son engagement total dans le projet soit un peu plus
consistant. J’ai cherché dans mon client de courriels ses messages à debian-project, debian-vote et debian-devel
pour avoir une idée sur quels types de sujet elle a participé et sa position sur quelques problèmes, mais il ne
me semble pas qu’elle ait beaucoup participé. Je pense que le simple fait de poser sa candidature au poste de
DPL et que ses plans immédiats pour l’organisation d’une DebConf l’aideront beaucoup en terme de construction
de profil dans le projet Debian, et si elle n’est pas élue, je souhaite qu’elle se représente.</P>


<H3>A.2. Brian Gupta</H3>

<P>Les idées de Brian à propos d’une fondation pour Debian sont très intéressantes, et j’ai mis en avant
quelques discussions intéressantes et sensibilisé sur certains problèmes qui autrement n’auraient pas
été mis en lumière.</P>

<P>Cependant, je pense que cette idée de transformer un vote de DPL en un référendum n’est pas la meilleure
manière de faire. Une évaluation appropriée de nos besoins, une comparaison des options possibles et un
débat complémentaire aboutissant à une résolution générale conduiraient à mon avis à de meilleurs
résultats.</P>

<P>De plus, je ne crois pas qu’il soit possible de mettre en place ce type d’organisation que Brian désire
en un seul mandat de DPL. La réponse à de simples questions telles que « Qui pourvoira en personnel ces
organisations ? », « Aura-t-il un salaire ? », etc., peut être vraiment difficile dans le contexte de
Debian. Elles peuvent toutes avoir de bonnes réponses, mais je pense que cela nous prendra du temps pour
déterminer ce que nous voulons exactement, ce dont nous avons besoin et comment répondre à cela de manière
à ce que le projet s’épanouisse. Éventuellement, il pourrait être possible de démarrer avec un très petit
organisme de confiance (TO) qui couvre la différence entre ce que SPI et les TO actuels peuvent
actuellement nous fournir et ce dont nous avons besoin. Cela mérite d’explorer chaque option possible.</P>

<P>Démarrer une ou plusieurs fondations pourrait avoir des conséquences profondes que nous n’avons pas
envisagées. Que faire si Brian est élu, consacre une année pour faire avancer les choses, puis que des
solutions ne soient pas trouvées et qu’il décide de ne pas se représenter ? Sera-t-il à ceux qui
resteront de démanteler et d’arrêter toute la chose ? Ou serons-nous dans encore un autre chaos de Debian
que nous devrions subir dans les années à venir ?</P>

<P>Il y a certaines choses que nous pouvons accélérer, ou même improviser au fur et à mesure, et il y a
des choses qui doivent être faites dans les règles. Notre structure organisationnelle, notre politique de
soutien et notre formalisation du projet méritent bien qu’on y réfléchisse, et je ne doute pas que cela
continuera à être important dans Debian pendant longtemps, mais je ne pense pas que cela doit être fondé
sur un référendum, ou sur un mandat particulier de DPL.</p>


<H2>A. Journal des modifications</H2>

<P> Les versions de ce programme sont gérées dans un <a href="https://salsa.debian.org/jcc/dpl-platform">dépôt git.</a> </P>

<UL>
<LI><a href="https://salsa.debian.org/jcc/dpl-platform/tags/2.00">2.00</a>: New platform for 2020 DPL elections.</LI>
<LI><a href="https://salsa.debian.org/jcc/dpl-platform/tags/2.00">2.01</a>: Add rebuttals.</LI>
</UL>

<BR>

</DIV> <!-- END MAIN -->


