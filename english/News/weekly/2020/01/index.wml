#use wml::debian::projectnews::header PUBDATE="2020-09-09" SUMMARY="Welcome to the DPN, Debian Communication channels, New DPL, Internal and External news, DebConf20 and events, Reports, and Calls for Help"
#use wml::debian::acronyms

# Status: [Frozen]


## substitute XXX with the number (expressed in letter) of the issue.
## please note that the var issue is not automagically localized, so
## translators need to put it directly in their language!
## example: <intro issue="fourth" />

## Use &#35; to escape # for IRC channels
## Use &#39 for single quotes

<intro issue="first" />

<toc-add-entry name="newdpn">Welcome to the Debian Project News!</toc-add-entry>

<p>We hope that you enjoy this <b>Special Archival Edition</b> of the Debian
Project News which repeats, introduces, and attempts to catch you up on most of
the year's news for 2020 so far.</p>

<p>
For this special edition of the news, we have omitted a few
regular sections in our effort to bring our coverage up to date. Should you wish
to find older or  missing information, please review our
<a href="https://bits.debian.org">Bits from Debian Blog</a> or our
<a href="https://micronews.debian.org">Debian Micronews Service</a> where all
recent and current news items have already been shared through this calendar year.
</p>

<toc-add-entry name="official">Official communication channels for Debian.</toc-add-entry>

<p>
From time to time we get questions about current events, the rules about who may
own websites in the Debian namespace, the progress of development within the
community, or inquiries as to the official channels of communication
for and from Debian.
</p>

<p>
The <a href="https://www.debian.org/News/">News</a> section of the Debian website
carries formal information and announcements many of which are mirrored/shared
from the debian-news and debian-announce mailing lists.
</p>

<p>
Our <a href="https://bits.debian.org/">Bits from Debian Blog</a> carries news
from multiple Debian mailing-lists along with semi-formal information and
announcements on a much faster news release cycle.
</p>

<p>
Our <a href="https://micronews.debian.org/">Micronews Service</a> carries and
covers short, immediate impact, or breaking and current news event items. The
Micronews service also feeds to multiple social media feeds, such as twitter,
identi.ca, and framapiaf.org.
</p>

<p>
Of course, we cannot forget the medium you are reading: the
<a href="https://www.debian.org/News/weekly/">Debian Project News</a> is
the project newsletter, which comes out somewhat randomly (help needed - see below).
</p>

<p>
Please note that all of the listed channels for communication are available via 
the <a href="https://en.wikipedia.org/wiki/HTTPS"><b>https://</b></a> protocol
which you will see in use throughout our website and all official channels.
</p>

<p>
Should you need to reach out to us for questions around an announcement or news item,
please do not hesitate to contact us via email at
<a href="mailto:press@debian.org">press@debian.org</a>.  We would ask you to
please note that the press and publicity teams which <i>provide these
information-only services</i> are not able to provide direct user support; such inquiries
should be forwarded to the
<a href="https://lists.debian.org/debian-user/">debian-user mailing lists</a> or
toward the appropriate <a href="https://wiki.debian.org/Teams">Debian Team</a>.
</p>

<toc-add-entry name="helpspread">The Publicity team call for volunteers and help!</toc-add-entry>

<p>Your Publicity team is asking for help from you our readers, developers, and
interested parties to contribute to the Debian news effort. We implore you to
submit items that may be of interest to our community and also ask for your
assistance with translations of the news into (your!) other languages along with
the needed second or third set of eyes to assist in editing our work before
publishing. If you can share a small amount of your time to aid our team which
strives to keep all of us informed, we need you. Please reach out to us via IRC
on <a href="irc://irc.debian.org/debian-publicity">&#35;debian-publicity</a> on
<a href="https://oftc.net/">OFTC.net</a>, or our
<a href="mailto:debian-publicity@lists.debian.org">public mailing list</a>,
or via email at <a href="mailto:press@debian.org">press@debian.org</a> for
sensitive or private inquiries.</p>

<toc-add-entry name="longlivetheking">We have elected a new DPL.</toc-add-entry>

<p>Jonathan Carter (highvoltage), the newly elected DPL, shares his thoughts,
thanks, and outlines for the future of Debian in his first official
<a href="https://lists.debian.org/debian-devel-announce/2020/04/msg00013.html">blog</a>
post as DPL. New DPL blog posts are also <a href="https://bits.debian.org/dpl/">posted</a>
on the official Debian blog.
</p>

<toc-display/>

<toc-add-entry name="security">Security Advisories</toc-add-entry>

<p>Debian's Security Team releases current advisories on a daily basis
(<a href="$(HOME)/security/2020/">Security Advisories 2020</a>). Please
read them carefully and subscribe to the <a href="https://lists.debian.org/debian-security-announce/">security mailing
list</a>.</p>

<p>The Debian website now also <a href="https://www.debian.org/lts/security/">archives</a>
the security advisories issued by the Debian Long Term Support team and posted to the
<a href="https://lists.debian.org/debian-lts-announce/">debian-lts-announce mailing list</a>.
</p>

<toc-add-entry name="internal">Internal News</toc-add-entry>

<p><b>News for Debian 10 <q>buster</q>: 10.5 released</b></p>

<p>In July 2019, we welcomed
<a href="https://www.debian.org/News/2019/20190706">the release of Debian 10
(codename <q>buster</q>)</a>. Since then, 
the Debian project announced the
<a href="https://www.debian.org/News/2019/20190907">first</a>,
<a href="https://www.debian.org/News/2019/20191116">second</a>,
<a href="https://www.debian.org/News/2020/20200208">third</a>,
<a href="https://www.debian.org/News/2020/20200509">fourth</a>,
and <a href="https://www.debian.org/News/2020/20200801">fifth</a>
point release updates of its stable distribution.
Debian 10.5 was published on 1 August 2020.</p>

<p><b>Updated Debian 9: 9.13 released</b></p>

<p>The Debian project announced the
<a href="https://www.debian.org/News/2019/2019090702">tenth</a>,
<a href="https://www.debian.org/News/2019/20190908">eleventh</a>,
<a href="https://www.debian.org/News/2020/2020020802">twelfth</a>,
and <a href="https://www.debian.org/News/2020/20200718">thirteenth</a>
point (and final) release updates of its old stable distribution Debian 9
(codename <q>stretch</q>).
Debian 9.13 was published on 18 July 2020.</p>

<p>Security support for the <q>oldstable</q> Debian 9 release has been discontinued
as of 6 July 2020. However, <q>stretch</q> benefits from Long Term Support (LTS)
<a href="https://wiki.debian.org/LTS/Stretch">until the end of June 2022</a>.
The LTS is limited to i386, amd64, armel, armhf and arm64. 
</p>

<p><b>Debian 8 LTS reaching end-of-life</b></p>

<p>On 30 June 2020, Debian LTS support for Debian 8 <q>Jessie</q> reached
its end-of-life five years after its initial release on 26 April 2015.</p>

<p><b>News on Debian 11 <q>bullseye</q></b></p>

<p>Paul Gevers from the Release Team <a href="https://lists.debian.org/debian-devel-announce/2020/03/msg00002.html">shared</a>
the upcoming policy for our next release, the tentative freeze dates, along with
other updates and changes. For additional information regarding how these changes
and policies came to be, please refer to an earlier <a
href="https://lists.debian.org/debian-devel-announce/2019/07/msg00002.html">Bits from the Release Team</a>.</p>

<p>Aurelien Jarno proposed the
<a href="https://lists.debian.org/debian-devel-announce/2019/08/msg00003.html">removal of the mips architecture</a>
for the next release, because the porting effort has become increasingly difficult.
This removal does not affect <q>stretch</q> or <q>buster</q>.
</p>

<p>In view of Python 2's removal for the <q>bullseye</q> release, the debian-python
team provides news about the <a
href="https://lists.debian.org/debian-devel-announce/2019/11/msg00000.html">progress and next steps</a>
of the process. More details are available on the <a
href="https://wiki.debian.org/Python/2Removal">dedicated wiki page</a>.</p>

<p>The Debian Installer Bullseye Alpha 1 release was
<a href="https://www.debian.org/devel/debian-installer/News/2019/20191205.html">published</a>
on 5 December 2019.</p>

<p>Cyril Brulebois shared news on the <a
href="https://lists.debian.org/debian-devel-announce/2020/03/msg00005.html">Debian Installer Bullseye Alpha 2</a>
release, published on 16 March 2020, which features many improvements and changes to
clock-setup, grub-installer, preseed, and systemd among other enhancements. Tests and
reports to find bugs and further improve the installer are welcome. Installer images
and everything else are
<a href="https://www.debian.org/devel/debian-installer">available for download</a>.</p>

<p><b>A new localization package for the manpages</b></p>

<p>Historically, man pages for a given package have often had translations 
provided in various other packages using different methods, teams, and formats. 
Currently, most of them are no longer maintained. Most of the critical manpages
were previously translated to keep the man pages up-to-date.</p>

<p>Debian will release a new 
<a href="https://lists.debian.org/debian-l10n-french/2020/02/msg00105.html">manpages-l10n</a>.
package which will replace Perkamon, manpages-de, manpages-fr, manpages-fr-extra, 
manpages-pl, manpages-pl-dev, manpages-pt, and manpages-pt-dev. Additionally, 
this new package brings manpage translations for the Dutch and Romanian languages. 

The result will be that French, German, Dutch, Polish, Romanian,
and Portuguese speakers will now have possibly translated manpages contained in 
a single package. From those translations speakers can help with or report bugs against 
pages. We welcome others to join this effort, but please keep in mind areas 
where packages are no longer maintained, those packages have a higher priority 
to convert into PO files and update.</p>

<p><b>DKIM Keys</b></p>

<p>Adam D. Barratt shared information on a recent enhancement to the Debian
infrastructure with an enhancement for DDs to utilize
<a href="https://lists.debian.org/debian-devel-announce/2020/04/msg00004.html">DKIM keys for mail
authentication</a>.</p>

<p><b>Salsa Authentication Changes</b></p>

<p>Enrico Zini announced <a href="https://lists.debian.org/debian-devel-announce/2020/04/msg00007.html">Salsa logins are enabled on nm.debian.org</a>.
This change improves ease of use for Applicants, Maintainers, and new Developers.
Following this change, Bastian Blank from the Salsa administrators team announced a
<a href="https://lists.debian.org/debian-devel-announce/2020/04/msg00010.html">Salsa update: removal of -guest accounts</a>
towards using Salsa as an authentication provider.</p>

<p>Sean Whitton released <a href="https://www.debian.org/doc/debian-policy/">Debian Policy 4.5.0.2</a>
with updates to generated usernames, service unit script use, and update-rc.d
updates.</p>


<p><b>Bits from the Technical Committee</b></p>

<p>The Technical Committee while preparing their DebConf20 talk intitled
<a href="https://debconf20.debconf.org/talks/16-meet-the-technical-committee/">Meet the Technical Committee</a> published their
<a href="https://lists.debian.org/debian-devel-announce/2020/08/msg00005.html">annual report</a>
and a <a href="https://salsa.debian.org/debian/tech-ctte/-/blob/master/talks/rethinking-the-tc.md">proposal document</a>
to enhance the TC's work and processes.
</p>

<p><b>New Debian Academy Team</b></p>

<p>
A new initiative to define and run an official Debian e-learning platform with
Debian specific courses. Do you want to help make it happen? Visit the 
<a href="https://wiki.debian.org/DebianAcademy">wiki page</a>
and join the team.
</p>

<toc-add-entry name="external">External News</toc-add-entry>

<p><b>Debian France has renewed its board</b></p>

<p>Debian France is a non-profit organisation forming part of the Trusted Organisations.
They are the main trusted organisation in Europe. They have just renewed their
board. There were few changes, except the president who is now Jean-Philippe MENGUAL.</p>

<p>Keep in mind that this non-profit organisation has on its board only Debian
developers, as President, Secretary and Treasurer. The other administrators are
not all Debian Developers but contribute to Debian on booths or other events.
<a href="https://france.debian.net/posts/2020/renouvellement_instances/">See the news</a>.
</p>


<toc-add-entry name="MiniDebs">MiniDebConfs, MiniDebCamps, and DebConf20</toc-add-entry>

<p><b>MiniDebConfs and MiniDebCamps</b></p>
<p>
At the start of 2020, there were seven <a href="https://wiki.debian.org/MiniDebConf">MiniDebConfs</a>
scheduled all around the world, reflecting the vitality of our community.
Organized by Debian project members, MiniDebConfs are open to everyone and
provide an opportunity for developers, contributors and other interested people
to meet in person. But due to the constraints related to the coronavirus
(COVID-19) pandemic, most if not all of these conferences have been cancelled or
postponed to 2021: 
<a href="https://maceio2020.debian.net">Maceió (Brazil)</a>,
<a href="https://wiki.debian.org/DebianEvents/gb/2020/MiniDebConfAberdeen">Aberdeen (Scotland)</a>,
<a href="https://wiki.debian.org/DebianEvents/fr/2020/Bordeaux">Bordeaux (France)</a>,
<a href="https://wiki.debian.org/DebianLatinoamerica/2020/MiniDebConfLatAm">El Salvador</a>, and
<a href="https://lists.debian.org/debian-devel-announce/2020/02/msg00006.html">Regensburg (Germany)</a>.
Some have been rescheduled online: the first one,
<a href="https://wiki.debian.org/DebianEvents/internet/2020/MiniDebConfOnline">MiniDeconfOnline #1</a>,
preceded by a DebCamp, was held from 28 to 31 May 2020. The 
<a href="https://wiki.debian.org/DebianEvents/internet/2020/MiniDebConfOnline2">MiniDebConf Online #2 "Gaming Edition"</a>,
devoted to gaming on Debian (and Linux in general) will take place from 19
to 22 November 2020 - the first two days being a Debcamp. Visit the
<a href="https://wiki.debian.org/DebianEvents/internet/2020/MiniDebConfOnline2">wiki page of the event</a>
for details.</p>


<p><b>DebConf20 held Online, DebConf21 to be held in Haifa, Israel</b></p>

<p>
<a href="https://www.debian.org/News/2020/20200830">DebConf20</a> has been
held online for the first time, due to the coronavirus (COVID-19) disease
pandemic.</p>

<p>The Debian community has adapted to this change with the continuing of idea
sharing, Birds of a Feather (BoF) sessions and discussions and all of the
things  we have developed as traditions over the years. All of the sessions were
streamed, with a variety of ways of participating: via IRC messaging,
online collaborative text documents, and video conferencing meeting rooms.
</p> 

<p>With more than 850 attendees from 80 different countries and a total of over
100 event talks, discussion sessions, Birds of a Feather (BoF) gatherings and
other activities, <a href="https://debconf20.debconf.org">DebConf20</a> was a
large success. The DebConf20 schedule included two tracks in languages
other than English: the Spanish language MiniConf and the Malayalam language MiniConf.</p>

<p>Most of the talks and sessions are available through the <a
href="https://meetings-archive.debian.net/pub/debian-meetings/2020/DebConf20/">Debian meetings archive website</a>
and the <a href="https://debconf20.debconf.org/">DebConf20 website</a> will 
remain active for archival purposes and will continue to offer links to the
presentations and videos of talks and events.
</p>

<p>
Next year, <a href="https://wiki.debian.org/DebConf/21">DebConf21</a> is planned
to be held in Haifa, Israel, in August or September.
</p>

<toc-add-entry name="debday">Debian Day</toc-add-entry>

<p>27 years strong! Happy Anniversary! Happy <a href="https://wiki.debian.org/DebianDay/2020">#DebianDay!</a>
Due to the worldwide Covid-19 pandemic, fewer local events were organised for DebianDay
and some of them held online in <a href="https://wiki.debian.org/Brasil/Eventos/DebianDayBrasil2020">Brazil</a>
(15 to 16 August). Nevertheless, developers and contributors had the opportunity
to meet in La Paz (Bolivia), České Budějovice (Czech Republic), and Haifa
(Israel) during DebConf20.
</p>

<toc-add-entry name="otherevents">Other events</toc-add-entry>

<p>
The Debian Brasil community organized from May 3rd to Jun 6th, 2020, an online
event called
<a href="https://debianbrasil.gitlab.io/FiqueEmCasaUseDebian/">#FiqueEmCasaUseDebian (#StayHomeUseDebian)</a>.
During 27 nights, the DDs Daniel Lenharo and Paulo Santana (phls) hosted guests
who shared their knowledge about Debian. You can read a
<a href="http://softwarelivre.org/debianbrasil/blog/fiqueemcasausedebian-it-was-35-days-with-talks-translations-and-packaging">report</a>.
</p>

<toc-add-entry name="reports">Reports</toc-add-entry>
## It's easier to link to the monthly reports for the LTS section and the RB links rather than
# summarize each report or number of packages. Feel free to input this information if you need to fill
# the issue out
#

<p><b>LTS Freexian Monthly Reports</b></p>

<p>Freexian issues <a href="https://raphaelhertzog.com/tag/Freexian+LTS/">monthly reports</a>
about the work of paid contributors to Debian Long Term Support.
</p>

<p><b>Reproducible Builds status update</b></p>

<p>Follow the <a
href="https://reproducible-builds.org/blog/">Reproducible
Builds blog</a> to get the weekly reports on their work in the <q>buster</q> cycle.
</p>


<toc-add-entry name="help">Help needed</toc-add-entry>
#Make bold -fix
<p><b>Teams needing help</b></p>
## Teams needing help
## $Link to the email from $Team requesting help

<p>Debian Developers Reference is now maintained as ReStructuredText. Translations
and translation updates are welcome - please see the announcement in the
<a href="https://lists.debian.org/debian-devel-announce/2019/08/msg00003.html">Misc. Developer News</a> and
the <a href="https://lists.debian.org/debian-devel/2020/02/msg00500.html">developers-reference translations</a>
thread for more information.</p>

<p><b>Call for bullseye artwork proposals</b></p>

<p>Jonathan Carter made the <a
href="https://lists.debian.org/debian-devel-announce/2020/08/msg00002.html">official call
for proposals for bullseye artwork</a>.
For the most up-to-date details, please refer to the <a
href="https://wiki.debian.org/DebianDesktop/Artwork/Bullseye">wiki</a>. 
At the same time, we would like to thank Alex Makas for doing the <a
href="https://wiki.debian.org/DebianArt/Themes/futurePrototype">futurePrototype
theme for buster</a>. If you would like, or know of someone who would like, to
create a desktop look and feel, be sure to send in your artwork. Submission
deadline is 10 October 2020.</p>

<p><b>Packages needing help:</b></p>

## link= link to the mail report from wnpp@debian.org to debian-devel ML
## orphaned= number of packages orphaned according to $link
## rfa= number of packages up for adoption according to $link

<wnpp link="https://lists.debian.org/debian-devel/2020/09/msg00058.html"
        orphaned="1191"
        rfa="213" />

<p><b>Newcomer bugs</b></p>

## check https://bugs.debian.org/cgi-bin/pkgreport.cgi?tag=newcomer and add outstanding + forwarded + pending upload
<p>
Debian has a <q>newcomer</q> bug tag, used to indicate bugs which are suitable for new
contributors to use as an entry point to working on specific packages.

There are currently <a href="https://bugs.debian.org/cgi-bin/pkgreport.cgi?tag=newcomer">195</a>
bugs available tagged <q>newcomer</q>.
</p>

<toc-add-entry name="code">Code, coders, and contributors</toc-add-entry>
<p><b>New Package Maintainers since 1 July 2019</b></p>
##Run the ./new-package-maintainers script locally
##./new-package-maintainers YYYMMDD
<p>
Please welcome: Marie Alexandre, Saira Hussain, laokz, Arthur Diniz, Daniel
Pimentel, Ricardo Fantin da Costa, Ivan Noleto, Joao Seckler, Andre Marcelo
Alvarenga, Felipe Amorim, Pedro Vaz de Mello de Medeiros, Marco Villegas,
Thiago Gomes Verissimo, Teemu Toivola, Giuliano Augusto Faulin Belinassi,
Miguel Figueiredo,  Stephan Lachnit, Nicola Di Lieto, Ximin Luo, Paul Grosu,
Thomas Ward, Ganael Laplanche, Aaron M. Ucko, Rodrigo Carvalho, Lukas
Puehringer, Markus Teich, Alexander Ponyatykh, Rob Savoury, Joaquin de Andres,
Georges Basile Stavracas Neto, Jian-Ding Chen (timchen119), Juan Picca,
Katerina, Matthew Fernandez, Shane McDonald, Eric Desrochers, Remi Duraffort,
Sakirnth Nagarasa, Ambady Anand S., Abhijith Sheheer, Helen Koike, Sven Hartge,
Priyanka Saggu, Sebastian Holtermann, Jamie Bliss, David Hart, James Tocknell,
Julien Schueller, Matt Hsiao, Jafar Al-Gharaibeh, Matthias Blümel, Dustin
Kirkland, Gao Xiang , Alberto Leiva Popper, Benjamin Hof, Antonio Russo, Jérôme
Lebleu, Ramon Fried, Evangelos Rigas, Adam Cecile, Martin Habovstiak, Lucas
Kanashiro, Alessandro Grassi, Estebanb, James Price, Cyril Richard, John Scott,
David Bürgin, Beowulf, Igor Petruk, Thomas Dettbarn, Vifly, Lajos Veres,
Andrzej Urbaniak, Phil Wyett, Christian Barcenas, Johannes Schilling, Josh
Steadmon, Sven Hesse, Gert Wollny, suman rajan, kokoye2007, Kei Okada, Jonathan
Tan, David Rodriguez, David Krauser, Norbert Schlia, Pranav Ballaney, Steve
Meliza, Fabian Grünbichler, Sao I Kuan, Will Thompson, Abraham Raji, Andre
Moreira Magalhaes, Lorenzo Puliti, Dmitry Baryshkov, Leon Marz, Ryan Pavlik,
William Desportes, Michael Elterman, Simon, Schmeisser, Fabrice Bauzac, Pierre
Gruet, Mattia Biondi, Taowa Munene-Tardif, Sepi Gair, Piper McCorkle, Alois
Micard, xiao sheng wen, Roman Ondráček, Abhinav Krishna C K, Konstantin Demin,
Pablo Mestre Drake, Harley Swick, Robin Gustafsson, Hamid Nassiby, Étienne
Mollier, Karthik, Ben Fiedler, Jair Reis, Jordi Sayol, Emily Shaffer, Fabio dos
Santos Mendes, Bruno Naibert de Campos, Junior Figueredo, Marcelo Vinicius
Campos Amedi, Tiago Henrique Vercosa de Lima, Deivite Huender Ribeiro Cardoso,
Guilherme de Paula Xavier Segundo, Celio Roberto Pereira, Tiago Rocha, Leandro
Ramos, Filipi Souza, Leonardo Santiago Sidon da Rocha, Asael da Silva Vaz,
Regis Fernandes Gontijo, Carlos Henrique Lima Melara, Alex Rosch de Faria, Luis
Paulo Linares, Jose Nicodemos Vitoriano de Oliveira, Marcio Demetrio Bacci,
Fabio Augusto De Muzio Tobich, Natan Mourao, Wilkens Lenon, Paulo Farias,
Leonardo Rodrigues Pereira, Elimar Henrique da Silva, Delci Silva Junior, Paulo
Henrique Hebling Correa, Gleisson Jesuino Joaquim Cardoso, Joao Paulo Lima de
Oliveira, Jesus Ali Rios, Jaitony de Sousa, Leonardo Santos, Aristo Chen,
Olivier Humbert, Roberto De Oliveira, Martyn Welch, Arun Kumar Pariyar, Vasyl
Gello, Antoine Latter, Ken VanDine, Francisco M Neto, Dhavan Vaidya, Raphael
Medaer, Evangelos Ribeiro Tzaras, Richard Hansen, Joachim Langenbach, Jason
Hedden, Boian Bonev, Kay Thriemer, Sławomir Wójcik, Lukas Märdian, Kevin Duan,
zhao feng, Leandro Cunha, Cocoa, J0J0 T, Johannes Tiefenbacher, Iain Parris,
Guobang Bi, Lasse Flygenring-Harrsen, Emanuel Krivoy, Qianqian Fang, Rafael
Onetta Araujo, Shruti Sridhar, Alvin Chen, Brian Murray, Nick Gasson.
</p>

<p><b>New Debian Maintainers</b></p>
##Run the ./dd-dm-from-keyring.pl script locally for new DM and DD
##./dd-dm-from-keyring.pl YYYMMDD
<p>
Please welcome: Hans van Kranenburg, Scarlett Moore, Nikos Tsipinakis, Joan
Lledó, Baptiste Beauplat, Jianfeng Li, Denis Danilov, Joachim Falk, Thomas
Perret, William Grzybowski, Lars Tangvald, Alberto Molina Coballes, Emmanuel
Arias, Hsieh-Tseng Shen, Jamie Strandboge, Sven Geuer, Håvard Flaget Aasen,
Marco Trevisan, Dennis Braun, Stephane Neveu, Seunghun Han, Alexander Johan
Georg Kjäll, Friedrich Beckmann, Diego M. Rodriguez, Nilesh Patra, Hiroshi
Yokota, Shayan Doust, Chirayu Desai, Arnaud Ferraris, Francisco Vilmar Cardoso
Ruviaro, Patrick Franz, François Mazen, Kartik Kulkarni, Fritz Reichwald, Nick
Black, Octavio Alvarez.
</p>

<p><b>New Debian Developers</b></p>
<p>
Please welcome: Keng-Yu Lin, Judit Foglszinger, Teus Benschop, Nick Morrott,
Ondřej Kobližek, Clément Hermann, Gordon Ball, Louis-Philippe Véronneau, Olek
Wojnar, Sven Eckelmann, Utkarsh Gupta, Robert Haist, Gard Spreemann, Jonathan
Bustillos, Scott Talbert, Paride Legovini, Ana Custura, Felix Lechner, Richard
Laager, Thiago Andrade Marques, Vincent Prat, Michael Robin Crusoe, Jordan
Justen, Anuradha Weeraman, Bernelle Verster, Gabriel F. T. Gomes, Kurt
Kremitzki, Nicolas Mora, Birger Schacht and Sudip Mukherjee.
</p>

<p><b>Contributors</b></p>

## Visit the link below and pull the information manually.

<p>
1375 people and 9 teams are currently listed on the
<a href="https://contributors.debian.org/">Debian Contributors</a> page for 2020.
</p>


<p><b>New and noteworthy packages</b></p>

<p>
A sample of the many packages <a href="https://packages.debian.org/unstable/main/newpkg">
added to the unstable Debian archive</a> in the past few weeks:</p>

<ul>
<li><a href="https://packages.debian.org/unstable/main/bpftool">bpftool - Inspection and simple manipulation of BPF programs and maps</a></li>
<li><a href="https://packages.debian.org/unstable/main/elpa-magit-forge">elpa-magit-forge - Work with Git forges from the comfort of Magit</a></li>
<li><a href="https://packages.debian.org/unstable/main/libasmjit0">libasmjit0 - Complete x86/x64 JIT and AOT Assembler for C++</a></li>
<li><a href="https://packages.debian.org/sid/libraritan-rpc-perl">libraritan-rpc-perl - Perl module for the Raritan JSON-RPC interface</a></li>
<li><a href="https://packages.debian.org/unstable/main/python3-rows">python3-rows - library to tabular data, no matter the format</a></li>
</ul>


<p><b>Once upon a time in Debian:</b></p>

## Items pulled from the Timeline https://timeline.debian.net
## Jump to any random year/ same month/ same week.
## Provide link and link description.
## This may work better with a script at some point, but for now let us see
## what the ease of work is.

## Format - YYYY-MM-DD text

<ul>

<li>2007-09-02 <a href="https://lists.debian.org/debian-devel-announce/2007/09/msg00001.html">packages.debian.org updated</a></li>

<li>2006-09-04 <a href="https://lists.debian.org/debian-devel-announce/2006/09/msg00002.html">cdrkit (cdrtools fork) uploaded</a></li>

<li>2009-09-04 <a href="https://lists.debian.org/debian-devel-announce/2009/09/msg00002.html">grub package now based on GRUB2</a></li>

<li>1997-09-06 <a href="https://lists.debian.org/debian-devel-announce/1997/09/msg00000.html">BTS severity tracking implemented</a></li>

<li>2008-09-08 <a href="https://lists.debian.org/debian-devel-announce/2008/09/msg00001.html">m68k architecture moving to debian-ports.org</a></li>

</ul>


<toc-add-entry name="continuedpn">Want to continue reading DPN?</toc-add-entry>
<continue-dpn />

<p><a href="https://lists.debian.org/debian-news/">Subscribe or Unsubscribe</a> from the Debian News mailing list</p>

#use wml::debian::projectnews::footer editor="The Publicity Team with contributions from Laura Arjona Reina, Jean-Pierre Giraud, Paulo Henrique de Lima Santana, Jean-Philippe Mengual, Donald Norwood"
# No need for the word 'and' for the last contributor, it is added at build time.
# Please add the contributors to the /dpn/CREDITS file
# Translators may also add a translator="foo, bar, baz" to the previous line
