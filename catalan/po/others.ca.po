# others webwml Catalan template.
# Copyright (C) 2002, 2004, 2005, 2007 Free Software Foundation, Inc.
# Jordi Mallach <jordi@debian.org>, 2002.
# Guillem Jover <guillem@debian.org>, 2004-2008, 2011, 2018.
#
msgid ""
msgstr ""
"Project-Id-Version: webwml\n"
"PO-Revision-Date: 2018-07-15 02:11+0200\n"
"Last-Translator: Guillem Jover <guillem@debian.org>\n"
"Language-Team: Catalan <debian-l10n-catalan@lists.debian.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../english/devel/debian-installer/ports-status.defs:10
msgid "Working"
msgstr "Funciona"

#: ../../english/devel/debian-installer/ports-status.defs:20
msgid "sarge"
msgstr "sarge"

#: ../../english/devel/debian-installer/ports-status.defs:30
msgid "sarge (broken)"
msgstr "sarge (trencada)"

#: ../../english/devel/debian-installer/ports-status.defs:40
msgid "Booting"
msgstr "Arranca"

#: ../../english/devel/debian-installer/ports-status.defs:50
msgid "Building"
msgstr "Construït"

#: ../../english/devel/debian-installer/ports-status.defs:56
msgid "Not yet"
msgstr "Encara no"

#: ../../english/devel/debian-installer/ports-status.defs:59
msgid "No kernel"
msgstr "No te nucli"

#: ../../english/devel/debian-installer/ports-status.defs:62
msgid "No images"
msgstr "No te imatges"

#: ../../english/devel/debian-installer/ports-status.defs:65
msgid "<void id=\"d-i\" />Unknown"
msgstr "<void id=\"d-i\" />Desconegut"

#: ../../english/devel/debian-installer/ports-status.defs:68
msgid "Unavailable"
msgstr "No està disponible"

#: ../../english/devel/join/nm-steps.inc:7
msgid "New Members Corner"
msgstr "Racó dels nous membres"

#: ../../english/devel/join/nm-steps.inc:10
msgid "Step 1"
msgstr "Primera passa"

#: ../../english/devel/join/nm-steps.inc:11
msgid "Step 2"
msgstr "Segona passa"

#: ../../english/devel/join/nm-steps.inc:12
msgid "Step 3"
msgstr "Tercera passa"

#: ../../english/devel/join/nm-steps.inc:13
msgid "Step 4"
msgstr "Quarta passa"

#: ../../english/devel/join/nm-steps.inc:14
msgid "Step 5"
msgstr "Cinquena passa"

#: ../../english/devel/join/nm-steps.inc:15
msgid "Step 6"
msgstr "Sisena passa"

#: ../../english/devel/join/nm-steps.inc:16
msgid "Step 7"
msgstr "Setena passa"

#: ../../english/devel/join/nm-steps.inc:19
msgid "Applicants' checklist"
msgstr "Llista de tasques del sol·licitant"

#: ../../english/devel/website/tc.data:11
msgid ""
"See <a href=\"m4_HOME/intl/french/\">https://www.debian.org/intl/french/</a> "
"(only available in French) for more information."
msgstr ""
"Vegeu <a href=\"m4_HOME/intl/french/\">https://www.debian.org/intl/french/</"
"a> (només disponible en francès) per a més informació."

#: ../../english/devel/website/tc.data:12
#: ../../english/devel/website/tc.data:14
#: ../../english/devel/website/tc.data:15
#: ../../english/devel/website/tc.data:16
#: ../../english/events/merchandise.def:145
msgid "More information"
msgstr "Més informació"

#: ../../english/devel/website/tc.data:13
msgid ""
"See <a href=\"m4_HOME/intl/spanish/\">https://www.debian.org/intl/spanish/</"
"a> (only available in Spanish) for more information."
msgstr ""
"Vegeu <a href=\"m4_HOME/intl/spanish/\">https://www.debian.org/intl/spanish/"
"</a> (només disponible en castellà) per a més informació."

#: ../../english/distrib/pre-installed.defs:18
msgid "Phone"
msgstr "Telèfon"

#: ../../english/distrib/pre-installed.defs:19
msgid "Fax"
msgstr "Fax"

#: ../../english/distrib/pre-installed.defs:21
msgid "Address"
msgstr "Adreça"

#: ../../english/events/merchandise.def:13
msgid "Products"
msgstr "Productes"

#: ../../english/events/merchandise.def:16
msgid "T-shirts"
msgstr "samarretes"

#: ../../english/events/merchandise.def:19
msgid "hats"
msgstr "gorres"

#: ../../english/events/merchandise.def:22
msgid "stickers"
msgstr "adhesius"

#: ../../english/events/merchandise.def:25
msgid "mugs"
msgstr "tasses"

#: ../../english/events/merchandise.def:28
msgid "other clothing"
msgstr "d'altra roba"

#: ../../english/events/merchandise.def:31
msgid "polo shirts"
msgstr "samarreta polo"

#: ../../english/events/merchandise.def:34
msgid "frisbees"
msgstr "discs voladors"

#: ../../english/events/merchandise.def:37
msgid "mouse pads"
msgstr "coixinet del ratolí"

#: ../../english/events/merchandise.def:40
msgid "badges"
msgstr "insígnies"

#: ../../english/events/merchandise.def:43
msgid "basketball goals"
msgstr "cistelles de bàsquet"

#: ../../english/events/merchandise.def:47
msgid "earrings"
msgstr "arracades"

#: ../../english/events/merchandise.def:50
msgid "suitcases"
msgstr "maletins"

#: ../../english/events/merchandise.def:53
msgid "umbrellas"
msgstr "paraigües"

#: ../../english/events/merchandise.def:56
msgid "pillowcases"
msgstr "fundes de coixí"

#: ../../english/events/merchandise.def:59
msgid "keychains"
msgstr "clauers"

#: ../../english/events/merchandise.def:62
msgid "Swiss army knives"
msgstr ""

#: ../../english/events/merchandise.def:65
msgid "USB-Sticks"
msgstr "memòries USB"

#: ../../english/events/merchandise.def:80
msgid "lanyards"
msgstr "cordons"

#: ../../english/events/merchandise.def:83
msgid "others"
msgstr "d'altres"

#: ../../english/events/merchandise.def:90
#, fuzzy
msgid "Available languages:"
msgstr "Llenguatge:"

#: ../../english/events/merchandise.def:107
msgid "International delivery:"
msgstr ""

#: ../../english/events/merchandise.def:118
msgid "within Europe"
msgstr ""

#: ../../english/events/merchandise.def:122
msgid "Original country:"
msgstr ""

#: ../../english/events/merchandise.def:187
msgid "Donates money to Debian"
msgstr ""

#: ../../english/events/merchandise.def:192
msgid "Money is used to organize local free software events"
msgstr ""

#: ../../english/logos/index.data:6
msgid "With&nbsp;``Debian''"
msgstr "Amb «Debian»"

#: ../../english/logos/index.data:9
msgid "Without&nbsp;``Debian''"
msgstr "Sense «Debian»"

#: ../../english/logos/index.data:12
msgid "Encapsulated PostScript"
msgstr "PostScript encapsulat"

#: ../../english/logos/index.data:18
msgid "[Powered by Debian]"
msgstr "[Funciona amb Debian]"

#: ../../english/logos/index.data:21
msgid "[Powered by Debian GNU/Linux]"
msgstr "[Funciona amb Debian GNU/Linux]"

#: ../../english/logos/index.data:24
msgid "[Debian powered]"
msgstr "[Funciona amb Debian]"

#: ../../english/logos/index.data:27
msgid "[Debian] (mini button)"
msgstr "[Debian] (botó petit)"

#: ../../english/mirror/submit.inc:7
msgid "same as the above"
msgstr "igual que l'anterior"

#: ../../english/women/profiles/profiles.def:24
msgid "How long have you been using Debian?"
msgstr ""

#: ../../english/women/profiles/profiles.def:27
msgid "Are you a Debian Developer?"
msgstr ""

#: ../../english/women/profiles/profiles.def:30
msgid "What areas of Debian are you involved in?"
msgstr ""

#: ../../english/women/profiles/profiles.def:33
msgid "What got you interested in working with Debian?"
msgstr ""

#: ../../english/women/profiles/profiles.def:36
msgid ""
"Do you have any tips for women interested in getting more involved with "
"Debian?"
msgstr ""

#: ../../english/women/profiles/profiles.def:39
msgid ""
"Are you involved with any other women in technology group? Which one(s)?"
msgstr ""

#: ../../english/women/profiles/profiles.def:42
msgid "A bit more about you..."
msgstr ""

#~ msgid "Wanted:"
#~ msgstr "Sol·licitat:"

#~ msgid "Who:"
#~ msgstr "Qui:"

#~ msgid "Architecture:"
#~ msgstr "Arquitectura:"

#~ msgid "Specifications:"
#~ msgstr "Especificacions:"

#~ msgid "Where:"
#~ msgstr "A on:"

#~ msgid "Name:"
#~ msgstr "Nom:"

#~ msgid "Company:"
#~ msgstr "Companyia:"

#~ msgid "URL:"
#~ msgstr "URL:"

#~ msgid "or"
#~ msgstr "o"

#~ msgid "Email:"
#~ msgstr "Correu:"

#~ msgid "Rates:"
#~ msgstr "Tarifa:"

#~ msgid "Willing to Relocate"
#~ msgstr "Disponible sense límit geogràfic"

#~ msgid ""
#~ "<total_consultant> Debian consultants listed in <total_country> countries "
#~ "worldwide."
#~ msgstr ""
#~ "<total_consultant> consultors de Debian llistats en <total_country> "
#~ "païssos per tot el món."

#~ msgid "Mailing List Subscription"
#~ msgstr "Subscripció a les llistes de correu"

#~ msgid ""
#~ "See the <a href=\"./#subunsub\">mailing lists</a> page for information on "
#~ "how to subscribe using e-mail. An <a href=\"unsubscribe\">unsubscription "
#~ "web form</a> is also available, for unsubscribing from mailing lists. "
#~ msgstr ""
#~ "Mireu la pàgina de les <a href=\"./#subunsub\">llistes de correu</a> per "
#~ "a informació de com us podeu subscriure usant el correu.  També hi ha "
#~ "disponible un <a href=\"unsubscribe\">formulari web per a donar-vos de "
#~ "baixa</a> d'aquestes llistes de correu. "

#~ msgid ""
#~ "Note that most Debian mailing lists are public forums. Any mails sent to "
#~ "them will be published in public mailing list archives and indexed by "
#~ "search engines. You should only subscribe to Debian mailing lists using "
#~ "an e-mail address that you do not mind being made public."
#~ msgstr ""
#~ "Tingueu en compte que la major part de les llistes de correu de Debian, "
#~ "són forums publics. Qualsevol correu que s'hi envíi es publicarà als "
#~ "arxius publics de les llistes de correu i s'indexarà pels motors de "
#~ "cerques. Us haurieu de suscriure a les llistes de correu de Debian "
#~ "emprant una adreça de correu que no us importi es façi publica."

#~ msgid "Please select which lists you want to subscribe to:"
#~ msgstr "Si us plau, seleccioneu a quines llistes us desitgeu donar d'alta:"

#~ msgid "No description given"
#~ msgstr "Descripció no disponible"

#~ msgid "Moderated:"
#~ msgstr "Moderada:"

#~ msgid "Posting messages allowed only to subscribers."
#~ msgstr "Enviament de missatges només permés a subscriptors."

#~ msgid ""
#~ "Only messages signed by a Debian developer will be accepted by this list."
#~ msgstr ""
#~ "Aquesta llista de correu només acceptarà missatges signats per un "
#~ "desenvolupador de Debian."

#~ msgid "Subscription:"
#~ msgstr "Subscripció:"

#~ msgid "is a read-only, digestified version."
#~ msgstr "és de sols lectura, versió resumida."

#~ msgid "Your E-Mail address:"
#~ msgstr "La vostra direcció de correu:"

#~ msgid "Subscribe"
#~ msgstr "Subscriu-m'hi"

#~ msgid "Clear"
#~ msgstr "Netejar"

#~ msgid ""
#~ "Please respect the <a href=\"./#ads\">Debian mailing list advertising "
#~ "policy</a>."
#~ msgstr ""
#~ "Si us plau respecteu la <a href=\"./#ads\">política d'anuncis en les "
#~ "llistes de correu Debian</a>."

#~ msgid "Mailing List Unsubscription"
#~ msgstr "Desubscripció de les llistes de correu"

#~ msgid ""
#~ "See the <a href=\"./#subunsub\">mailing lists</a> page for information on "
#~ "how to unsubscribe using e-mail. An <a href=\"subscribe\">subscription "
#~ "web form</a> is also available, for subscribing to mailing lists. "
#~ msgstr ""
#~ "Mireu la pàgina de les <a href=\"./#subunsub\">llistes de correu</a> per "
#~ "a informació de com us podeu dessubscriure usant el correu.  També hi ha "
#~ "disponible un <a href=\"subscribe\">formulari web per a donar-vos d'alta</"
#~ "a> d'aquestes llistes de correu. "

#~ msgid "Please select which lists you want to unsubscribe from:"
#~ msgstr ""
#~ "Si us plau, seleccioneu a quines llistes us desitgeu donar-vos de baixa:"

#~ msgid "Unsubscribe"
#~ msgstr "Dóna'm de baixa"

#~ msgid "open"
#~ msgstr "oberta"

#~ msgid "closed"
#~ msgstr "tancada"

#~ msgid "Topics:"
#~ msgstr "Temes:"

#~ msgid "Location:"
#~ msgstr "Localització:"

#~ msgid "Previous Talks:"
#~ msgstr "Conferencies prèvies:"

#~ msgid "p<get-var page />"
#~ msgstr "p<get-var page />"

#~ msgid "URL"
#~ msgstr "URL"

#~ msgid "Version"
#~ msgstr "Versió"

#~ msgid "Status"
#~ msgstr "Estat"

#~ msgid "Package"
#~ msgstr "Paquet"

#~ msgid "ALL"
#~ msgstr "Totes"

#~ msgid "Unknown"
#~ msgstr "Desconegut"

#~ msgid "??"
#~ msgstr "??"

#~ msgid "BAD?"
#~ msgstr "Malament?"

#~ msgid "OK?"
#~ msgstr "Bé?"

#~ msgid "BAD"
#~ msgstr "Mal"

#~ msgid "OK"
#~ msgstr "Bé"

#~ msgid "Old banner ads"
#~ msgstr "Pancartes antigues"

#~ msgid "Download"
#~ msgstr "Descàrrega"
