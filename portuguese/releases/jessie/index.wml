#use wml::debian::template title="Informações de lançamento do Debian &ldquo;jessie&rdquo;"
#include "$(ENGLISHDIR)/releases/info"
#include "$(ENGLISHDIR)/releases/jessie/release.data"
#include "$(ENGLISHDIR)/releases/arches.data"
#use wml::debian::translation-check translation="b30f61ea84c8132971f2d931fae4d548e309d02c"

<p>O Debian <current_release_jessie> foi
lançado em <a href="$(HOME)/News/<current_release_newsurl_jessie/>"><current_release_date_jessie></a>.
<ifneq "8.0" "<current_release>"
  "O Debian 8.0 foi incialmente lançado em <:=spokendate('2015-04-26'):>."
/>
O lançamento incluiu muitas
mudanças importantes, descritas no
nosso <a href="$(HOME)/News/2015/20150426">comunicado à imprensa</a> e
nas <a href="releasenotes">notas de lançamento</a>.</p>

<p><strong>Debian 8 foi substituído pelo
<a href="../stretch/">Debian 9 (<q>stretch</q>)</a>.
As atualizações regulares de suporte de segurança foram descontinuadas
a partir de <:=spokendate('2018-06-17'):>.
</strong></p>

<p><strong>O Jessie também se beneficia do suporte de longo prazo (<q>Long Term Support (LTS)</q>) até o
final de junho de 2020. O LTS está limitado a i386, amd64, armel e armhf.
Para mais informações, consulte a <a
href="https://wiki.debian.org/LTS">seção LTS da Wiki do Debian</a>.
</strong></p>

<p>Para obter e instalar o Debian, veja
a página <a href="debian-installer/">informações de instalação</a> e o
<a href="installmanual">guia de instalação</a>. Para atualizar a partir de
uma versão mais antiga do Debian, veja as instruções nas
<a href="releasenotes">notas de lançamento</a>.</p>

<p>Arquiteturas suportadas durante o período de suporte do LTS:</p>

<ul>
<:
foreach $arch (@archeslts) {
	print "<li><a href=\"$(HOME)/ports/$arch/\">$arches{$arch}</a>\n";
}
:>
</ul>

<p>No lançamento inicial do Jessie, essas arquiteturas eram suportadas:</p>

<ul>
<:
foreach $arch (@arches) {
	print "<li><a href=\"$(HOME)/ports/$arch/\">$arches{$arch}</a>\n";
}
:>
</ul>

<p>Ao contrário do que desejamos, pode haver alguns problemas existentes
na versão, mesmo que ela seja declarada <em>estável (stable)</em>. Nós fizemos
<a href="errata">uma lista dos principais problemas conhecidos</a>, e você
sempre pode nos <a href="reportingbugs">relatar outros problemas</a>.</p>

<p>Por último mas não menos importante, nós temos uma lista de <a href="credits">
pessoas que merecem o crédito</a> por fazer este lançamento acontecer.</p>

<if-stable-release release="squeeze">
<p>Nenhuma informação disponível ainda.</p>
</if-stable-release>

<if-stable-release release="wheezy">

<p>O codinome para a próxima versão principal do Debian após o <a
href="../wheezy/">wheezy</a> é <q>jessie</q>.</p>

<p>Esta versão começou como uma cópia da wheezy, e está atualmente em um
estado chamado <q><a href="$(DOC)/manuals/debian-faq/ftparchives#testing">
testing</a></q>. Isso significa que as coisas não deveriam quebrar
de maneira tão ruim quanto nas versões instável (unstable) ou
experimental, porque os pacotes só são autorizados a entrar nesta
versão depois que um certo período de tempo passou, e quando eles
não têm nenhum bug crítico ao lançamento relatado contra os mesmos.</p>

<p>Por favor, note que as atualizações de segurança para a versão
teste (testing) ainda <strong>não</strong> são gerenciadas pelo time de
segurança. Por isso, a <q>testing</q> <strong>não</strong> recebe atualizações
de segurança em tempo hábil.
# For more information please see the
# <a href="https://lists.debian.org/debian-testing-security-announce/2008/12/msg00019.html">announcement</a>
# of the Testing Security Team.
Encorajamos você a mudar suas entradas do sources.list de testing para
squeeze por enquanto, se você precisa do suporte de segurança. Veja também
o texto na <a href="$(HOME)/security/faq#testing">FAQ da equipe de Segurança</a>
para a versão teste (testing).</p>

<p>Pode haver um <a href="releasenotes">rascunho das notas de lançamento disponível</a>.
Por favor, também <a href="https://bugs.debian.org/release-notes">
verifique as adições propostas para as notas de lançamento</a>.</p>

<p>Para imagens de instalação e documentação sobre como instalar o
<q>testing</q>, veja <a href="$(HOME)/devel/debian-installer/">a página do
Instalador do Debian</a>.</p>

<p>Para encontrar mais sobre como a versão teste (testing) funciona,
verifique <a href="$(HOME)/devel/testing">as informações dos(as) desenvolvedores(as)
sobre ela</a>.</p>

<p>As pessoas frequentemente perguntam se há um único <q>medidor de progresso</q>
para o lançamento. Infelizmente não há um, mas nós podemos nos referir a vários
locais que descrevem coisas que precisar ser resolvidas para que o lançamento
aconteça:</p>

<ul>
  <li><a href="https://release.debian.org/">Página genérica de estado do lançamento</a></li>
  <li><a href="https://bugs.debian.org/release-critical/">Bugs críticos ao lançamento</a></li>
  <li><a href="https://udd.debian.org/bugs.cgi?base=only&amp;rc=1">Bugs no sistema básico</a></li>
  <li><a href="https://udd.debian.org/bugs.cgi?standard=only&amp;rc=1">Bugs nos pacotes standard e task</a></li>
</ul>

<p>Além disso, relatórios de estado geral são enviados pelos gerentes
de lançamento para a <a href="https://lists.debian.org/debian-devel-announce/">\
lista de discussão debian-devel-announce</a>.</p>

</if-stable-release>
